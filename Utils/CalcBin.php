<?php

namespace Utils;

final class CalcBin {

    /**
     * для вычесления длинны в битах из целого числа 
     * @param int $int максимальная длинны числа 
     * @return int длинная в битах
     */
    public static function count2int($int) {
        return strlen(decbin(pow(10, $int) - 1));
    }

    /**
     * для вычесления длинны в битах из целого числа 
     * @param int $int число 
     * @return int длинная в битах
     */
    public static function int($int) {
        return strlen(decbin($int - 1));
    }

    /**
     * для вычесления длинны в битах из длинны символов
     * @param int $count число 
     * @return int длинная в битах
     */
    public static function char($count) {
        return $count * 8;
    }

}
