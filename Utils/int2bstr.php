<?php

namespace Utils;

class int2bstr {

    /**
     *
     * @var int
     */
    public $int;

    /**
     *
     * @var string
     */
    public $bstring;

    /**
     *
     * @var int
     */
    public $bit;

    /**
     *
     * @var string
     */
    public $bmask;

    /**
     *
     * @var boolean
     */
    public $lock = false;

    /**
     *
     * @var callable
     */
    public $cb;
    /**
     *
     * @var string bstring
     */
    public $bin;
    /**
     * 
     * @param int|string $int
     * @param int|null $bit
     * @param callable $cb
     */
    public function __construct($int = null, $bit = null, $cb = null) {
        $this->int = $int;
        $this->bit = $bit;
        $this->cb = $cb;
        if ($int !== null) {
            $this->bstring();
        }
    }

    /**
     * 
     * @param callable $cb
     * @return this|self|static
     */
    public function cb($cb) {
        $this->cb = $cb;
        return $this;
    }

    public function __invoke() {
        $this->lock = true;
        $this->bstring();
    }

    /**
     * 
     * @param int $int
     * @param int $bit
     */
    public function run($int, $bit = null) {
        $this->int = $int;
        $this->bit = $bit;
        $this->lock = true;
        $this->bstring();
    }

    public function __set($name, $value) {
        switch ($name) {
            case 'bit':
                if ($value !== null && $value !== 0) {
                    $this->bit = $value;
                    $this->bmask = str_repeat('0', $value);
                } else {
                    $this->bit = 0;
                }
                break;
        }
    }

    public function bstring() {
        $this->lock = true;
        $this->bstring = null;
        $this->bstring = decbin($this->int);
        setTimeout(function($event) {
            if ($this->bstring === null) {
                $event->timeout();
                return false;
            }
            if ($this->bit !== 0) {
                $this->bmask();
            } else {
                call_user_func($this->cb, $this->bstring);
            }
            $event->finish();
        }, 1e2);
    }

    /**
     * 
     * @return boolean
     */
    public function locked() {
        return $this->lock;
    }

    public function bmask() {
        $this->bin = null;
        $this->bin = substr($this->bmask, 0, $this->bit - strlen($this->bstring)) . $this->bstring;
        setTimeout(function($event) {
            if ($this->bin === null) {
                $event->timeout();
                return false;
            }
            call_user_func($this->cb, $this->bin);
            $event->finish();
        }, 1e2);
    }

}
