<?php

namespace Utils;

class bin2bstr {

    /**
     *
     * @var string
     */
    public $bstring;

    /**
     *
     * @var callable
     */
    public $cb;

    /**
     *
     * @var string
     */
    public $bin;

    /**
     *
     * @var string
     */
    public $hex;

    public function __construct($bin = null, $cb = null) {
        if ($bin !== null) {
            $this->bin = $bin;
        }
        if ($cb !== null) {
            $this->cb = $cb;
        }
        if ($this->bin !== null && $this->cb !== null) {
            $this->hex();
        }
    }

    public function __invoke($bin = null, $cb = null) {
        if ($bin !== null) {
            $this->bin = $bin;
        }
        if ($cb !== null) {
            $this->cb = $cb;
        }
        $this->hex();
    }

    public function hex() {
        $this->hex = null;
        $this->hex = base_convert($this->bin, 2, 16);
        setTimeout(function($event) {
            if ($this->hex === null) {
                $event->timout();
                return false;
            }
            $this->bstring();
            $event->finish();
        }, 1e2);
    }

    public function bstring() {
        $this->bstring = null;
        $this->bstring = pack('H*', $this->hex);
        setTimeout(function($event) {
            if ($this->bstring === null) {
                $event->timout();
                return false;
            }
            call_user_func($this->cb, $this->bstring);
            $event->finish();
        }, 1e2);
    }

}
