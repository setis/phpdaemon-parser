<?php

namespace Utils;

class hex2bstr {

    /**
     *
     * @var string
     */
    public $bstring;

    /**
     *
     * @var callable
     */
    public $cb;

    /**
     *
     * @var string
     */
    public $hex;

    public function __construct($hex = null, $cb = null) {
        if ($hex !== null) {
            $this->hex = $hex;
        }
        if ($cb !== null) {
            $this->cb = $cb;
        }
        if($this->hex !== null && $this->cb !== null){
            $this->bstring();
        }
    }

    public function __invoke($hex = null, $cb = null) {
        if ($hex !== null) {
            $this->hex = $hex;
        }
        if ($cb !== null) {
            $this->cb = $cb;
        }
        $this->bstring();
    }
    public function bstring() {
        $this->bstring = null;
        $this->bstring = pack('H*', $this->hex);
        setTimeout(function($event) {
            if ($this->bstring === null) {
                $event->timout();
                return false;
            }
            call_user_func($this->cb, $this->bstring);
            $event->finish();
        }, 1e2);
    }

}
