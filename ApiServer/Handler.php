<?php

namespace PHPDaemon\Applications\ApiServer;

use \Exception,
    \ReflectionMethod,
    Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent,
    Symfony\Component\EventDispatcher\Event,
    PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug;
use PHPDaemon\Applications\BootStrap\Events,
    PHPDaemon\Applications\ApiServer\Consts\Method,
    PHPDaemon\Applications\ApiServer\Consts\Config,
    PHPDaemon\Applications\ApiServer\Consts\Mode;

class Handler {

    use \Traits\getInstance;
    /**
     *
     * @var Response
     */
    public $Response;

    /**
     *
     * @var Handler\Event
     */
    public $Event;

    /**
     *
     * @var Handler\Call
     */
    public $Call;

    /**
     *
     * @var Mode
     */
    public $mode;

    /**
     *
     * @var boolean
     */
    public $Error = false;

    /**
     *
     * @var boolean
     */
    public $Exception = false;

    /**
     *
     * @var boolean
     */
    public $debug = true;

    public function __construct($cfg = null) {
        $this->Response = new Response($this);
        if ($cfg) {
            $this->configure($cfg);
        }
    }

    public function clear() {
        $this->Response->clear();
        switch ($this->mode) {
            case Mode::Event:
                $this->Event->clear();
                break;
            case Mode::Call:
                $this->Call->clear();
                break;
            case Mode::Сombined:
                $this->Event->clear();
                $this->Call->clear();
                break;
        }
    }

    public function onError() {
        set_error_handler([$this->Response, 'errorHandler']);
        return $this;
    }

    public function onException() {
        set_exception_handler([$this->Response, 'exceptionHandler']);
        return $this;
    }

    public function configure(array $cfg) {
        $setting = $cfg[Config::Run];
        if (isset($setting['Error']) && $setting['Error'] === true) {
            $this->onError();
        }
        if (isset($setting['Exception']) && $setting['Exception'] === true) {
            $this->onException();
        }
        if (isset($setting['protocol'])) {
            $this->protocol = $setting['protocol'];
        }
        $this->mode = $setting['mode'];
        switch ($setting['mode']) {
            case Mode::Event:
                $this->Event = new Handler\Event($cfg[Config::Event]);
                $this->Event->Response = &$this->Response;
                break;
            case Mode::Call:
                $this->Call = new Handler\Call($cfg[Config::Call]);
                $this->Call->Response = &$this->Response;
                break;
            case Mode::Сombined:
                $this->Event = new Handler\Event($cfg[Config::Event]);
                $this->Event->Response = &$this->Response;
                $this->Call = new Handler\Call($cfg[Config::Call]);
                $this->Call->Response = &$this->Response;
                break;
        }
        if (isset($setting['protocol'])) {
            $this->Response->protocol = $setting['protocol'];
        }
        if (isset($setting['debug'])) {
            $this->debug = $setting['debug'];
        }
        $this->Response->protocols = $cfg[Config::Protocol];
    }

    public function handler($input = null) {
        $id = 'W#' . Daemon::$process->getPid();
        if (Mode::Event === $this->mode) {
            $this->Event->input($input, function() {
                $this->Event->depends();
                $this->Event->handler();
            });
        } elseif (Mode::Call === $this->mode) {
            Daemon::log($id . ' ServerApi->Call');
            $this->Call->input($input, function() {
                $this->Call->depends();
                $this->Call->handler();
            });
        } elseif (Mode::Сombined === $this->mode) {
            Daemon::log($id . ' ServerApi->Сombined');
            try {
                $this->Event->input($input, function() {
                    $this->Event->depends();
                });
            } catch (Exception $e) {
                $this->Call->input($input, function()use($id) {
                    $this->Call->depends();
                    $this->Call->handler(null, function($e)use($id) {
                        Daemon::log($id . ' ' . __METHOD__ . ':' . __LINE__ . ' ' . $e->getMessage() . ' ' . $this->event);
                        throw new Exception('not executed');
                    });
                });
                return;
            }
            $this->Event->handler(null, function($e)use($id) {
                Daemon::log($id . ' ' . __METHOD__ . ':' . __LINE__ . ' ' . $e->getMessage() . ' -> ' . $this->event);
                $this->Call->input($input, function()use($id) {
                    $this->Call->depends();
                    $this->Call->handler(null, function($e)use($id) {
                        Daemon::log($id . ' ' . __METHOD__ . ':' . __LINE__ . ' ' . $e->getMessage() . ' ' . $this->event);
                        throw new Exception('not executed');
                    });
                });
            });
        } else {
            Daemon::log($id . ' ServerApi->not found method');
        }
    }

}
