<?php

namespace PHPDaemon\Applications\ApiServer;

use Exception,
    PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    \BootStrap\Env;

class Response {

    /**
     *
     * @var Array
     */
    public $headers = [];

    /**
     *
     * @var string
     */
    public $body;

    /**
     *
     * @var Handler
     */
    public $Handler;
    public $response;
    public $lock = 0;

    /**
     *
     * @var string
     */
    public $protocol = 'json';

    /**
     *
     * @var array
     */
    public $protocols = [];

    /**
     *
     * @var \PHPDaemon\HTTPRequest\Generic
     */
    public $Request;

    /**
     *
     * @var \Closure|callable
     */
    public $callback;

    public function __construct(&$Handler = null) {
        if ($Handler !== null) {
            $this->Handler = $Handler;
        }
        $this->Request = &Env::$Request;
        Env::$Response = $this;
    }

    public function config(array $cfg) {
        if (isset($cfg['protocol'])) {
            $this->protocol = $cfg['protocol'];
        }
        if (isset($cfg['protocols'])) {
            $this->protocols = $cfg['protocols'];
        }
    }

    public function clear() {
        $this->headers = [];
        $this->body = null;
    }

    public function response($body = null, $headers = null, $replace_body = false, $replace_header = false, $encode = true) {
        if ($replace_body === true || $body !== null) {
            $this->body = $body;
        }
        if ($replace_header === true || $headers !== null) {
            $this->headers = $headers;
        }
        if (count($this->headers) > 0) {
            $this->headers($this->headers);
        }
        $this->headers($this->protocol($this->protocol));
        if (!is_object($this->Request)) {
            \console\console(__METHOD__, $this->headers, $this->body);
            throw new Exception('Response not found out');
        }
        $this->Request->out(($encode) ? call_user_func([$this, $this->protocol], $this->body) : $this->body);
        $this->Request->finish();
    }

    public function protocol($protocol = null) {
        if ($protocol === null) {
            $protocol = $this->protocol;
        }
        if (!isset($this->protocols[$protocol])) {
            throw new Exception('not found protocol: ' . $protocol);
        }
        return $this->protocols[$protocol];
    }

    public function headers(array $headers = null) {
        if ($headers === null) {
            return false;
        }
        foreach ($headers as $header) {
            $this->Request->header($header, true, false);
        }
        return true;
    }

    public function defaultProtocolHeaders($protocol = null) {
        $this->headers($this->protocol($protocol));
    }

    public function json($data) {
        if (is_array($data) || is_object($data)) {
            $result = json_encode($data);
        } else {
            $result = json_encode(['result' => $data]);
        }

        if ($result === false) {
            if (json_last_error() === 6) {
                return json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR);
            }
            return json_encode([
                'result' => false,
                'error' => [
                    'exception' => 'json_encode',
                    'code' => json_last_error(),
                    'msg' => json_last_error_msg()
                ]
            ]);
        }
        return $result;
    }

    public function jsonp($result) {
        $callback = $this->Handler->callback;
        if ($callback === null) {
            $callback = $this->Handler->method(Consts::Get)['callback'];
            return $callback . '(' . $this->json($result) . ');';
        } else {
            return call_user_func($callback, $this->json($result));
        }
    }

    public function errorHandler($errno, $errstr, $errfile, $errline) {
        $this->response([
            'result' => false,
            'error' => [
                'code' => $errno,
                'msg' => $errstr,
                'file' => $errfile,
                'line' => $errline
            ]
                ], null,
//            ['HTTP/1.1 404 Not Found'], 
                true, true);
    }

    public function exceptionHandler(\Exception $e) {
        console(__METHOD__, $e);
        if ($e === null) {
            response(false);
            return false;
        }

//        $this->headers[] = 'HTTP/1.1 404 Not Found';
        $this->body = [
            'result' => false,
            'error' => [
                'exception' => get_class($e),
                'code' => $e->getCode(),
                'msg' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
            ]
        ];
        if ($this->Handler->debug === true) {
            setTimeout(function($event)use($e) {
                $trace = $e->getTrace();
                $this->body['error']['trace'] = $trace;
                if ($trace !== null) {
                    $this->response();
                    $event->finish();
                }
            }, 100);
        } else {
            $this->response();
        }
    }

}
