<?php

namespace PHPDaemon\Applications\ApiServer\Handler;

use PHPDaemon\Applications\ApiServer\Exception;

class Call extends Base {

    /**
     *
     * @var Array
     */
    public $replace;

    /**
     *
     * @var Array
     */
    public $args_to_array;

    /**
     *
     * @var string
     */
    public $class;

    /**
     *
     * @var string
     */
    public $func;

    /**
     *
     * @var int|string|array
     */
    public $args;

    /**
     *
     * @var object
     */
    public $obj;

    /**
     *
     * @var boolean
     */
    public $dynamic = false;

    /**
     *
     * @var boolean
     */
    public $return = false;

    public static function isStatic($class, $func) {
        $reflectMethod = new \ReflectionMethod($class, $func);
        return $reflectMethod->isStatic();
    }

    public $rules = [
        'dependence',
        'dependenceList',
        'proccess',
        'dynamic',
        'replace',
        'args_to_array',
        'return'
    ];

    public function clear() {
        $this->reset();
        $this->class = null;
        $this->func = null;
        $this->args = null;
        $this->obj = null;
    }

    public function handler($onSuccess = null, $onError = null) {
        if (is_string($onSuccess)) {
            $onSuccess = events()->dispatch_template($onSuccess, Events::EVENT_ON);
        }
        if (is_string($onError)) {
            $onError = events()->dispatch_template($onError, Events::EVENT_EXCEPTION, $this);
        }
        $this->class = str_replace('.', '\\', $this->class);
        $flag = true;
        if ($this->replace) {
            $function = $this->class . '.' . $this->func;
            if (isset($this->replace['function']) && isset($this->replace['function'][$function])) {
                list($this->class, $this->func) = explode('.', $this->replace['function'][$function]);
                $flag = false;
            }
            if (isset($this->replace['class']) && isset($this->replace['class'][$this->class])) {
                $this->class = $this->replace['class'][$this->class];
                $flag = false;
            }
        }
        $class = $this->class;
        if ($this->args_to_array && isset($this->args_to_array[$class]) && in_array($this->func, $this->args_to_array[$class])) {
            $args = true;
        } else {
            $args = false;
        }
        if (self::isStatic($class, $this->func)) {
            if ($args === true) {
                if ($this->return === true) {
                    $this->Response->response(call_user_func_array([$this->class, $this->func], $this->args));
                } else {
                    call_user_func_array([$this->class, $this->func], $this->args);
                }
//                $this->eventDispatcher->dispatch('response', new GenericEvent($this, [call_user_func_array([$this->class, $this->func], $this->args), $callback]));
            } else {
                if ($this->return === true) {
                    $this->Response->response(call_user_func([$this->class, $this->func], $this->args));
                } else {
                    call_user_func([$this->class, $this->func], $this->args);
                }
//                $this->eventDispatcher->dispatch('response', new GenericEvent($this, [call_user_func([&$this->class, $this->func], $this->args), $callback]));
            }
        } else {
            $this->obj = new $class();
            if (!is_object($this->obj)) {
                if (is_callable($onError)) {
                    call_user_func($onError, Exception('not object is class:' . $this->class));
                    return false;
                } else {
                    throw new Exception('not object is class:' . $this->class);
                }
            }
            if (!$this->dynamic && !method_exists($this->obj, $this->func)) {
                if (is_callable($onError)) {
                    call_user_func($onError, Exception('not found method or dynamic enable ' . $this->class . '::' . $this->func));
                    return false;
                } else {
                    throw new Exception('not found method or dynamic enable ' . $this->class . '::' . $this->func);
                }
            }
            if ($args === true) {
                if ($this->return === true) {
                    $this->Response->response(call_user_func_array([&$this->obj, $this->func], $this->args));
                } else {
                    call_user_func_array([&$this->obj, $this->func], $this->args);
                }
//                $this->eventDispatcher->dispatch('response', new GenericEvent($this, [call_user_func_array([&$this->obj, $this->func], $this->args), $callback]));
            } else {
                if ($this->return === true) {
                    $this->Response->response(call_user_func([&$this->obj, $this->func], $this->args));
                } else {
                    call_user_func([&$this->obj, $this->func], $this->args);
                }
//                $this->eventDispatcher->dispatch('response', new GenericEvent($this, [call_user_func([&$this->obj, $this->func], $this->args), $callback]));
            }
            if (is_callable($onSuccess)) {
                call_user_func($onSuccess);
                return true;
            }
        }
    }

}
