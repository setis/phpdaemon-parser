<?php

namespace PHPDaemon\Applications\ApiServer\Handler;

use PHPDaemon\Applications\ApiServer\Consts\Method,
    PHPDaemon\Applications\ApiServer\Exception;

abstract class Base {

    /**
     *
     * @var \PHPDaemon\HTTPRequest\Generic
     */
    public $Request;

    /**
     *
     * @var \PHPDaemon\Applications\ApiServer\Response
     */
    public $Response;

    /**
     *
     * @var Array
     */
    public $input = [];

    /**
     *
     * @var array
     */
    public $vars;

    /**
     *
     * @var array
     */
    public $proccess;

    /**
     *
     * @var array
     */
    public $rules;

    public function __construct($cfg = null) {
        if ($cfg) {
            $this->configure($cfg);
        }
        $this->Request = &\BootStrap\Env::$Request;
    }

    public function configure(array $cfg) {
        foreach ($this->rules as $name) {
            if (!empty($cfg[$name])) {
                $this->{$name} = $cfg[$name];
            }
        }
        foreach ($this->proccess as $value) {
            $this->input[] = $value['name'];
        }
    }

    public function reset() {
        foreach ($this->input as $var) {
            $this->{$var} = null;
        }
        return $this;
    }

    /**
     * @param string get|post|request
     * @return array
     */
    public function &method($method = null) {
//        console(__METHOD__, $method === Method::Get, gettype($method) === gettype(Method::Get));
        if (is_string($method)) {
            $method = trim(strtolower($method));
        } else {
            $method = (string) $method;
        }

        switch ($method) {
            case Method::Vars:
            case 'vars':
                return $this->vars;
            case 'get':
            case Method::Get:
                return $this->Request->attrs->get;
            case 'post':
            case Method::Post:
                return $this->Request->attrs->post;
            case 'request':
            case Method::Request;
                return $this->Request->attrs->request;
        }
    }

    /**
     * 
     * @return $this|self|static
     * @throws Exception
     */
    public function depends() {
        foreach ($this->dependenceList as $depend) {
            if (empty($this->{$depend})) {
                throw new Exception('not input ' . $depend);
                \console('depend not input ' . $depend . ' ', $this->{$depend}, $this->proccess[$depend]);
            }
//            else{
//                \console('depend input ' . $depend . ' ' ,$this->{$depend},$this->proccess[$depend]);
//            }
        }
        return $this;
    }

    public function input(array $input = null, $event = null) {
        $this->reset();
        if ($input === null) {
            foreach ($this->proccess as $var => $array) {
                $input = $this->method($array['method']);
                $name = $array['name'];
                if (isset($input[$name])) {
                    $this->{$var} = $input[$name];
                }
//                \console('input ' . $var . ' ' ,$this->{$var},$array);
            }
        } else {
            foreach ($this->input as $var) {
                if (isset($input[$var])) {
                    $this->{$var} = $input[$array['name']];
                }
            }
        }
        if (is_string($event)) {
            $this->eventDispatcher->dispatch($event, new GenericEvent($this));
        } else
        if (is_callable($event)) {
            call_user_func($event, $this);
        }
    }

    public function run($array = null) {
        $this->input($array, function() {
            $this->handler();
        });
    }

}
