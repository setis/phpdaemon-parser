<?php

namespace PHPDaemon\Applications\ApiServer\Handler;

use PHPDaemon\Applications\ApiServer\Exception,
    Symfony\Component\EventDispatcher\GenericEvent,
    PHPDaemon\Core\Daemon;

class Event extends Base {

    /**
     *
     * @var string
     */
    public $event;

    /**
     *
     * @var array
     */
    public $params;

    /**
     *
     * @var array|callable
     */
    public $listeners = [];
    public $rules = [
        'dependence',
        'dependenceList',
        'proccess'
    ];

    public function configure(array $cfg) {
        parent::configure($cfg);
        if (isset($cfg['group'])) {
            $this->listeners = \BootStrap\Events::getInstance()->getGroup($cfg['group']);
        }
    }

    public function clear() {
        $this->event = null;
        $this->params = null;
    }

    public function handler($onSuccess = null, $onError = null) {
        Daemon::log('W#' . Daemon::$process->getPid() . " ServerApi->Event:\t{$this->event}");
        if (is_string($onSuccess)) {
            $onSuccess = events()->dispatch_template($onSuccess, Events::EVENT_ON);
        }
        if (is_string($onError)) {
            $onError = events()->dispatch_template($onError, Events::EVENT_EXCEPTION, $this);
        }
        if ($this->event === null) {
            if (is_callable($onError)) {
                call_user_func($onError, new Exception('not input event'));
                return false;
            } else {
                throw new Exception('not input event');
            }
        }
        if (!in_array($this->event, $this->listeners)) {
            if (is_callable($onError)) {
                call_user_func($onError, new Exception('not event register'));
                return false;
            } else {
                throw new Exception('not event register');
            }
        }

        \BootStrap\Events::$EventDispatcher->dispatch($this->event, new GenericEvent($this, ($this->params === null) ? [] : $this->params));
        if (is_callable($onSuccess)) {
            call_user_func($onSuccess);
            return true;
        }
    }

}
