<?php

namespace PHPDaemon\Applications\ApiServer\Consts;

interface Config {

    const
            Event = 0,
            Call = 1,
            Run = 2,
            Protocol = 3

    ;
}
