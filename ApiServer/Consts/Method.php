<?php

namespace PHPDaemon\Applications\ApiServer\Consts;

interface Method {

    const
            Get = 0,
            Post = 1,
            Request = 2,
            Vars = 3;

}