<?php

namespace PHPDaemon\Applications\ApiServer;

use PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    PHPDaemon\Core\AppInstance;

class Server extends AppInstance {

    /**
     *
     * @var \PHPDaemon\Applications\ApiServer\Handler
     */
    public $api;
    public $setting;

    /**
     * Constructor.
     * @return void
     */
    protected function init() {
        $this->setting = \BootStrap\AppConfig::load($this, $this->name);
    }

    /**
     * Setting default config options
     * Overriden from AppInstance::getConfigDefaults
     * Uncomment and return array with your default options
     * @return array|false
     */
    protected function getConfigDefaults() {
        return [
            'enable' => 1
        ];
    }

    /**
     * Called when the worker is ready to go.
     * @return void
     */
    public function onReady() {
        if ($this->isEnabled()) {
            $this->api = new Handler($this->setting);
        }
    }

    /**
     * Creates Request.
     * @param object Request.
     * @param object Upstream application instance.
     * @return object Request.
     */
    public function beginRequest($req, $upstream) {
        return new Request($this, $upstream, $req);
    }

}

?>
