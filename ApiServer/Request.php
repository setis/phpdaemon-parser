<?php

namespace PHPDaemon\Applications\ApiServer;

use PHPDaemon\HTTPRequest\Generic;

class Request extends Generic {

    /**
     *
     * @var Server
     */
    public $appInstance;

    /**
     *
     * @var int
     */
    public $timeout;
    public $i = 0;

    public function init() {
        $this->timeout = $this->appInstance->setting[Consts\Config::Run]['timeout'];
        \BootStrap\Env::$Request = $this;
        $this->appInstance->api->Response->Request = $this;
        \BootStrap\Env::$Response = $this->appInstance->api->Response;
        $this->appInstance->api->Response->defaultProtocolHeaders();
        \console\console(__METHOD__, $this->i++);
//        $this->appInstance->api->handler();
//        $this->sleep(1, true);
    }


    protected function run() {
        \console\console(__METHOD__, $this->i++);
        $this->appInstance->api->handler();
        $this->sleep($this->timeout);
    }

    public function onFinish() {
        $this->appInstance->api->clear();
    }

}
