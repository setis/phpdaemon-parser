<?php

namespace Grabber\Functions;

/**
 * 
 * @param \GearmanJob $job
 */
function job(\GearmanJob $job) {
    $data = json_decode($job->workload(), true);
    $data['job'] = $job;
    \Grabber\Kernel::getInstance()->handler($data);
}

/**
 * 
 * @param \GearmanJob $job
 */
function dispatch(\GearmanJob $job) {
    $data = json_decode($job->workload(), true);
//    \console\msg(__FUNCTION__,$data);
    $data['job'] = $job;
    $name = $data['event'];
    unset($data['event']);
    $event = new \Symfony\Component\EventDispatcher\GenericEvent(null, $data);
    $dispatcher = \Grabber\Kernel::getInstance()->EventDispatcher;
    if (is_array($name)) {
        foreach ($name as $n) {
            $dispatcher->dispatch($n, $event);
        }
    } elseif (is_string($name)) {
        $dispatcher->dispatch($name, $event);
    } else {
        $job->sendException("not events:" . json_encode($name));
    }
}

function jobCall(\GearmanJob $job) {
    list($callback, $param_arr) = json_decode($job->workload(), true);
//    \console\console(__FUNCTION__, $callback, $param_arr);
    if ($callback === null) {
        throw new \Exception();
    }
    if (!is_callable($callback)) {
        throw new \Exception('not callbale ' . json_encode([$callback, $param_arr]));
    }
    if ($param_arr === null || (is_array($param_arr) && count($param_arr) === 0)) {
//        \console\console(__FUNCTION__, $callback);
        call_user_func($callback);
    } else {
//        (new \ReflectionMethod($callback[0],$callback[1]))->isStatic()
        call_user_func_array($callback, $param_arr);
    }
}

/**
 * 
 * @param array|string $callback
 * @param array|null $param_arr
 * @return \GearmanTask
 * @throws \Exception
 */
function queueCall($callback, array $param_arr = null) {
//    console(__FUNCTION__, $callback);
//    if(!is_array($callback)){
//        throw new \Exception('not callback');
//    }
    if (empty($callback)) {
        \console\msg(__FUNCTION__, debug_backtrace()[0]);
    }
    $debug = debug_backtrace()[0];
    unset($debug['args']);
    $params = json_encode([$callback, $param_arr, $debug]);
    if ($params === false) {
        $e = new \Exception('not encode json');
        \console\msg('error-' . __FUNCTION__, dumpException($e));
        throw $e;
    }
    return \PHPDaemon\Applications\Gearman\Client::getInstance('')->queue('call', $params, \PHPDaemon\Applications\Gearman\Type::Async_Background);
}

/**
 * 
 * @param array $data
 * @return \GearmanTask
 */
function queue(array $data) {
    return \PHPDaemon\Applications\Gearman\Client::getInstance('')->queue('grabber', json_encode($data), \PHPDaemon\Applications\Gearman\Type::Async_Background);
}

/**
 * 
 * @param array $data
 * @return \GearmanTask
 */
function event(array $data) {
    return \PHPDaemon\Applications\Gearman\Client::getInstance('')->queue('dispatch', json_encode($data), \PHPDaemon\Applications\Gearman\Type::Async_Background);
}

/**
 * 
 * @param \Symfony\Component\EventDispatcher\GenericEvent $event
 * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
 * @param boolean $stop
 * @return \Closure
 */
function error(\Symfony\Component\EventDispatcher\GenericEvent $event, \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher, $stop = false) {
    return function($e)use($dispatcher, $event, $stop) {
        $event['exception'] = $e;
        if ($stop) {
            $event->stopPropagation();
        }
        $dispatcher->dispatch('error', $event);
    };
}

function dbError(&$model, $data, $error, $success = null, $duplicate = null) {
    return function($e, $query)use(&$model, $data, $error, $success, $duplicate) {
        switch ($e->getCode()) {
            case 1146:
                $model->install(function()use($model, $error, $data, $success, $duplicate) {
                    $model->insert($data, $success, $error);
                }, function($e)use($model, $data, $error, $success) {
                    switch ($e->getCode()) {
                        case 1062:
                            if ($duplicate !== null) {
                                call_user_func($duplicate);
                            }
                            return;
                        case 1050:
                            $model->insert($data, $success, $error);
                            break;
                        default:
                            $error($e);
                            break;
                    }
                });
                break;
            case 1062:
                if ($duplicate !== null) {
                    call_user_func($duplicate);
                }
                return;
            default:
                $error($e, $query);
                break;
        }
    };
}

/**
 * 
 * @param \Symfony\Component\EventDispatcher\GenericEvent $event
 * @param \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher
 * @param array|string $action
 * @param \Closure $callback
 */
function success(\Symfony\Component\EventDispatcher\GenericEvent $event, \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher, $action = null, \Closure $callback = null) {
    return function($value)use($dispatcher, $event, $action, $callback) {
        if ($action !== null) {
            if (is_string($action)) {
                $dispatcher->dispatch($action, ($callback !== null) ? $callback($event, $value) : $event);
            } elseif (is_array($action)) {
                foreach ($action as $a) {
                    $dispatcher->dispatch($a, ($callback !== null) ? $callback($event, $value) : $event);
                }
            }
        }
    };
}
