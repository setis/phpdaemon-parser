<?php

if (isset($func['event']) && $func['event'] === true) {

    /**
     * 
     * @return \BootStrap\Events
     */
    function events() {
        return BootStrap\Events::getInstance();
    }

}

if (isset($func['session']) && $func['session'] === true) {

    /**
     * 
     * @return \Session\Redis
     */
    function session($mode = false) {
        return ($mode === true) ? \Session\Redis::getInstance()->run() : \Session\Redis::getInstance();
    }

}
if (isset($func['user']) && $func['user'] === true) {

    /**
     * 
     * @return \Auth\User
     */
    function user($mode = false) {
        return ($mode === true) ? \Auth\User::getInstance()->run() : \Auth\User::getInstance();
    }

}
if (isset($func['stack']) && $func['stack'] === true) {

    /**
     * 
     * @return  \PHPDaemon\Applications\BootStrap\Stack
     */
    function stack($mode = false) {
        return \PHPDaemon\Applications\BootStrap\Stack::getInstanceNew($mode);
    }

}

function error_401($callback = null) {
    \PHPDaemon\Applications\Api\Response::getInstance()->phpdaemon->header('HTTP/1.0 401 Unauthorized');
    if ($callback) {
        call_user_func($callback);
    }
}

function auth_is($onSuccess, $onError) {
    session(true);
    $user = user(true);
    $user->callback(function($self)use($onSuccess, $onError) {
        $callback = ($self->session->session['uid'] > 0) ? $onSuccess : $onError;
        $self->session->write(function()use($callback) {
            call_user_func($callback);
        });
    });
}

function error_auth() {
    response([
        'result' => false,
        'error' => [
            'code' => 401,
            'msg' => 'not auth'
        ]
            ], [
        'HTTP/1.0 401 Unauthorized'
            ], true, true);
}
