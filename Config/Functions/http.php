<?php

namespace http;

use PHPDaemon\Core\Daemon,
    \BootStrap\Env;

function response($body = null, $headers = null, $encode = true) {
    Env::$Response->response($body, $headers, false, false, $encode);
}

function exception(\Exception $e) {
    Env::$Response->exceptionHandler($e);
}

function wakeup() {
    Env::$Request->wakeup();
}

function sleep($time = 0, $set = false) {
    Env::$Request->sleep($time, $set);
}

function out($s, $flush = true) {
    Env::$Request->out($s, $flush);
}

function finish($status = 0, $zombie = false) {
    Env::$Request->finish($status, $zombie);
}
