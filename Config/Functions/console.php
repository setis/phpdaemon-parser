<?php

namespace console;

function info() {
    return \BootStrap\Console::getInstance()->info(func_get_args());
}

function msg() {
    return call_user_func_array([\BootStrap\Console::getInstance(), 'msg'], func_get_args());
}

function log($name, $content) {
    return call_user_func([\BootStrap\Console::getInstance(), 'log'], $name, $content);
}

function console() {
    return \BootStrap\Console::console(func_get_args());
}
