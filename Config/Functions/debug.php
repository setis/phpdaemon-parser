<?php

namespace debug;

function exception(\Exception $e) {
    return [
        'exception' => get_class($e),
        'code' => $e->getCode(),
        'msg' => $e->getMessage(),
        'line' => $e->getLine(),
        'file' => $e->getFile(),
        'trace' => $e->getTrace()
    ];
}
