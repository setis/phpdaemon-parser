<?php

use \BootStrap\Env,
    PHPDaemon\Applications\ApiServer\Consts\Config,
    PHPDaemon\Applications\ApiServer\Consts\Method,
    PHPDaemon\Applications\ApiServer\Consts\Mode;

return [
    Config::Event => [
        'dependence' => true,
        'dependenceList' => [
            'event'
        ],
        'proccess' => [
            'event' => [
                'name' => 'action',
                'method' => Method::Get
            ],
            'params' => [
                'name' => 'in',
                'method' => Method::Get
            ]
        ],
        'group' => [
            'manga'
        ]
    ],
    Config::Run => [
        'Error' => false,
        'Exception' => false,
        'mode' => Mode::Event,
        'debug' => true,
        'protocol' => 'json',
        'timeout' => 45,
        'callback' => function($json) {
    return $_GET['callback'] . '(' . $json . ')';
}
    ],
    Config::Protocol => [
        'json' => [
            'Access-Control-Allow-Origin: *',
            'Access-Control-Allow-Headers: Content-Type,*',
//            'Access-Control-Allow-Headers: X-Requested-With, X-Prototype-Version, Content-Disposition, Cache-Control, Content-Type,Pragma',
            'Access-Control-Allow-Methods: GET,PUT,POST,DELETE,OPTIONS',
            'Access-Control-Max-Age: 1000',
            'Access-Control-Allow-Credentials: true',
            'Content-Type: application/json; charset=utf-8'
        ],
        'jsonp' => [
            'Access-Control-Allow-Origin: *',
            'Access-Control-Allow-Methods: GET',
            'Access-Control-Max-Age: 1000',
            'Content-Type: text/javascript; charset=utf-8'
        ]
    ]
];

