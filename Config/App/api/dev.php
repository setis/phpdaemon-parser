<?php

use \BootStrap\Env,
    PHPDaemon\Applications\ApiServer\Consts\Config,
    PHPDaemon\Applications\ApiServer\Consts\Method,
    PHPDaemon\Applications\ApiServer\Consts\Mode;

return [
    Config::Call => [
        'dependence' => true,
        'dependenceList' => [
            'class',
            'func'
        ],
        'proccess' => [
            'class' => [
                'name' => 'mod',
                'method' => Method::Get
            ],
            'func' => [
                'name' => 'action',
                'method' => Method::Get
            ],
            'args' => [
                'name' => 'in',
                'method' => Method::Request
            ]
        ],
        'dynamic' => false,
        'return' => false,
        'replace' => [
            'class' => [
//                    'ApiClient' => '\PHPDaemon\Applications\ApiClient',
                'test' => 'Test\Api',
                'readmanga' => 'Test\Readmanga',
                'index' => 'Test\Index',
//                'storage' => 'Controller_Mediastorage_Upload',
//                'test' => 'test\test'
            ],
            'function' => [
                'upload.handler' => 'Controller_upload.handler'
            ]
        ],
        'args_to_array' => [
            'Order\Item' => [
                'view111',
            ],
        ],
    ],
    Config::Run => [
        'Error' => false,
        'Exception' => false,
        'event.groups' => ['api'],
        'event.listners' => [],
        'mode' => Mode::Call,
        'debug' => true,
        'protocol' => 'json',
        'timeout'=>30,
        'callback' => function($json) {
    return $_GET['callback'] . '(' . $json . ')';
}
    ],
    Config::Protocol => [
        'json' => [
            'Access-Control-Allow-Origin: http://localhost:8383',
            'Access-Control-Allow-Headers: Content-Type,*',
//                        'Access-Control-Allow-Headers: X-Requested-With, X-Prototype-Version, Content-Disposition, Cache-Control, Content-Type,Pragma',
            'Access-Control-Allow-Methods: GET,PUT,POST,DELETE,OPTIONS',
            'Access-Control-Max-Age: 1000',
            'Access-Control-Allow-Credentials: true',
            'Content-Type: application/json; charset=utf-8'
        ],
        'jsonp' => [
            'Access-Control-Allow-Origin: *',
            'Access-Control-Allow-Methods: GET',
            'Access-Control-Max-Age: 1000',
            'Content-Type: text/javascript; charset=utf-8'
        ]
    ]
];

