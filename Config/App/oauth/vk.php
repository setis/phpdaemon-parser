<?php

return [
    'enable' => 1,
    'service' => [
        'key' => 4704340,
        'secret' => 'ZOh3cH4riG5JtNSGJmQG',
        'callback' => 'http://studio-devel.com:8080/oauth/vk/',
        'name' => 'vk',
        'scopes' => [
            'offline',
//            'nohttps',
            'stats',
            'notify',
            'friends',
            'photos',
            'docs',
            'notes',
            'status',
            'groups'
        ]
    ]
];
