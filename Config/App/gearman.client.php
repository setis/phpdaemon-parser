<?php

return [
    'server' => [
        'server' => 'localhost:4730',
        'options' => GEARMAN_CLIENT_NON_BLOCKING | GEARMAN_CLIENT_FREE_TASKS | GEARMAN_CLIENT_UNBUFFERED_RESULT,
//        'timeout' => 15,
    ],
    'timeout' => 4e4
//            'handlers' => [
//                'Complete' => [$this, 'Complete'],
//                'Created' => [$this, 'Created'],
//                'Data' => [$this, 'Data'],
//                'Status' => [$this, 'Status'],
//                'Fail' => [$this, 'Fail']
//            ]
];
