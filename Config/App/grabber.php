<?php

return [
    'service' => [
        'vk'
    ],
    'access' => [
        'vk:59076975:4704340'=>[
            'name' => 'vk',
            'access' => '1e1b8ad517dae6615aab9a04a18ecaffade9e2b106b2c362242d7f11786c44f9452bf79d89508da49b1d0',
            'group_id' => 59076975,
            'secret' => '50026e1f3d477735ac',
            'app_id' => 4704340,
            'scopes' => [
                'offline',
                'nohttps',
                'stats',
                'notify',
                'friends',
                'photos',
                'docs',
                'notes',
                'status',
                'groups'
            ]
        ]
    ],
    'source'=>[
      'readmanga'  
    ],
    'config' => [
        'readmanga' => [
            'compare.file'=>[
                'link'=>['path'],
                'file'=>['size','md5']
            ],
            'site' => 'http://readmanga.me',
            'resource'=>[
                'vk:59076975:4704340'
            ]
        ]
    ]
];
