<?php

use PHPDaemon\Applications\Sql\Builder,
    PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    PHPDaemon\Applications\Api\Response,
    Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent,
    Captcha\Recaptcha,
    Auth\Login;
use PHPDaemon\Applications\BootStrap\Events;

return [
    'response.result' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
response($event->getArgument(0), [], true, true);
},
    'response.exception' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
exception($event->getArgument(0));
},
    'response.success' => function($event, $name, EventDispatcher $dispatcher) {
response(true);
},
    'response.error' => function($event, $name, EventDispatcher $dispatcher) {
response(false);
},
    'response.exception' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
exception($event->getArgument(0));
},
    'auth.check' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
session(true);
$user = user(true);
$user->callback(function() {
    session()->write(function() {
        response((session()->session['uid'] > 0) ? true : false);
    });
});
},
    'auth.login' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
session(true);
$user = user(true);
$user->callback(function($self)use($event) {
    Login::getInstance()->login($event->getArguments(), function($result)use($self) {
        $self->onHandler($result[0], function() {
            session()->write(function() {
                response(true);
            });
        });
    }, function($e) {
        exception($e);
    }
    );
});
},
    'auth.logout' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
$session = session(true);
$session->callback(function()use($session) {
    $session->destroy();
    response(true);
});
},
    'auth.isLogin' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
$session = session(true);
$session->callback(function() use($event) {
    Login::getInstance()->isLogin($event->getArgument(0), function($result) {
        response((bool) count($result));
    }, function($e) {
        exception($e);
    }
    );
});
},
    'auth.create' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
console($name, $event->getArguments(), Daemon::$process->getPid());
Login::getInstance()->create($event->getArguments(), function() {
    response(true);
}, function($e) {
    response($e);
}
);
},
    'order.status.list' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
response(\Order\Checkout::$statusList);
},
    'order.status.delivery' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
response(\Order\Checkout::$deliveryList);
},
    'order.checkout.add' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $user = user();
    $checkout = \Order\Checkout::getInstance();
    $checkout->table($user->tid);
    $checkout->user = $user->uid;
    $form = \Order\Form::getInstance();
    $array = $event->getArguments();
    $array['code'] = $user->tid;
    $array['uid'] = $user->uid;
    $form->submit($array, function($result)use($checkout, $array) {
        $array['submit'] = $result;
        $checkout->change($array, function() {
            response(true);
        });
    }, 'response.exception');
}, function() {
    error_auth();
});
},
    'order.checkout.change' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $checkout = \Order\Checkout::getInstance();
    $user = user();
    $checkout->table($user->tid);
    $checkout->user = $user->uid;
    $checkout->change($event->getArguments(), 'response.success', 'response.exception');
}, function() {
    error_auth();
});
},
    'order.checkout.remove' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $checkout = \Order\Checkout::getInstance();
    $checkout->table(user()->tid);
    $checkout->remove($event->getArgument(0), 'response.success', 'response.exception');
}, function() {
    error_auth();
});
},
    'order.checkout.count' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $checkout = \Order\Checkout::getInstance();
    $checkout->table(user()->tid);
    $checkout->count(null, 'response.result', 'response.exception');
}, function() {
    error_auth();
});
},
    'order.checkout.view' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $checkout = \Order\Checkout::getInstance();
    $checkout->table(user()->tid);
    $checkout->view($event->getArguments(), 'response.result', 'response.exception');
}, function() {
    error_auth();
});
},
    'item.add' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $item = \Order\Item::getInstance();
    $user = user();
    $item->table($user->tid);
    $item->user = $user->uid;
    $item->add($event->getArguments(), 'response.result', 'response.exception');
}, function() {
    error_auth();
});
},
    'item.change' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $item = \Order\Item::getInstance();
    $user = user();
    console('item.change', $user->tid);
    $item->table($user->tid);
    $item->user = $user->uid;
    $item->change($event->getArguments(), 'response.success', 'response.exception');
}, function() {
    error_auth();
});
},
    'item.count' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $item = \Order\Item::getInstance();
    $item->table(user()->tid);
    $item->count(null, 'response.result', 'response.exception');
}, function() {
    error_auth();
});
},
    'item.remove' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $item = \Order\Item::getInstance();
    $item->table(user()->tid);
    $item->remove($event->getArgument(0), 'response.success', 'response.exception');
}, function() {
    error_auth();
});
},
    'item.view' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $item = \Order\Item::getInstance();
    $item->table(user()->tid);
    $item->view($event->getArguments(), 'response.result', 'response.exception');
}, function() {
    error_auth();
});
},
    'print.handler' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $obj = \PostalApi\RussianBarcode\Barcode::getInstance();
    $user = user();
    $obj->user = $user->uid;
    $obj->table($user->tid);
    $checkout = \Order\Checkout::getInstance();
    $checkout->table($user->tid);
    $checkout->user = $user->uid;
    PostalApi\RussianPostCalc::getInstance()->handler($event->getArguments(), 'response.result', 'response.exception');
}, function() {
    error_auth();
});
},
    'session.storage' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
session(true);
$user = user(true);
$user->callback(function($self)use($event) {
    response($self->session->session);
});
},
    'login.create' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
Login::getInstance()->sql_create(1, function() {
    response(true);
}, function($e) {
    exception($e);
});
},
    'barcode' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $obj = \PostalApi\RussianBarcode\Barcode::getInstance();
    $user = user();
    $obj->user = $user->uid;
    $obj->table($user->tid)->wrapper($event->getArgument(0), 'response.result', 'response.exception');
}, function() {
    error_auth();
});
},
    'barcode.test' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $obj = \PostalApi\RussianBarcode\Barcode::getInstance();
    $user = user();
    $obj->user = $user->uid;
    $checkout = \Order\Checkout::getInstance();
    $checkout->table($user->tid);
    $checkout->user = $user->uid;
    $obj->table($user->tid)->is_barcode($event->getArgument('data'),function($result){
        PostalApi\RussianPostCalc::getInstance()->handler($event->getArguments(), 'response.result', 'response.exception');
    });
}, function() {
    error_auth();
});
},
    'barcode.test1' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
auth_is(function()use($event) {
    $obj = \PostalApi\RussianBarcode\Barcode::getInstance();
    $user = user();
    $obj->user = $user->uid;
    $obj->table($user->tid)->test2(function($result){
        response($result);
    },function($e){
        exception($e);
    });
}, function() {
    error_auth();
});
},
];
