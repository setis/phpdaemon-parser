<?php

use Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent,
    GuzzleHttp\Event\CompleteEvent,
    GuzzleHttp\Event\ErrorEvent,
    Grabber\Consts\Http,
    Grabber\Kernel,
    Grabber\Parse\readmanga,
    Grabber\Index,
    PHPDaemon\Core\Daemon;

return [
    'readmanga.action.list' => function($event, $name, EventDispatcher $dispatcher) {
        $dispatcher->dispatch('grabber.queue', new GenericEvent(null, [
            'http.cfg' => [
                'method' => 'get',
                'url' => '/list?type=&sortType=rate&max=100000000',
                'events' => [
                    Http::Complete => [
                        'readmanga.list',
                        'grabber.complete'
                    ],
                    Http::Error => [
                        'grabber.error',
                    ]
                ],
                'options' => [
                    'base_url' => 'http://readmanga.me',
                    'future' => true,
                    'defaults' => [
                        'debug' => false
                    ]
                ]
            ],
            'params' => [
                'lang' => 'ru',
                'source' => 'readmanga'
            ]
        ]));
    },
            'readmanga.list' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        $data['http.cfg'] = $event['http.cfg'];
        $data['http.cfg']['events'][Http::Complete] = [
            'readmanga.content',
//            'grabber.db.short',
//            'grabber.db.long',
//            'grabber.date.year',
//            'grabber.relations',
//            'grabber.status',
            'readmanga.image',
//            'grabber.info',
            'grabber.complete',
        ];
        $data['params'] = $event['params'];
        /* @var $e \GuzzleHttp\Event\CompleteEvent */
        $e = $event->getSubject();
        $arr = readmanga::lists($e->getResponse()->getBody()->getContents());
        if (empty($arr)) {
            $data['exception'] = new \Exception('not found readmanga::lists');
            $dispatcher->dispatch('grabber.error', new GenericEvent(null, $data));
            $event->stopPropagation();
            return false;
        }
        foreach ($arr as $value) {
            $data['http.cfg']['url'] = $value['link'];
            $dispatcher->dispatch('grabber.queue', new GenericEvent(null, $data));
        }
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    },
            'readmanga.content' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        /* @var $e \GuzzleHttp\Event\CompleteEvent */
        $e = $event['http.event'];
        $result = readmanga::content($e->getResponse()->getBody()->getContents());
        $event['storage'] = $result;
        $params = $event['params'];
        $names = Index::dataPack([
                    'ru' => $result['name.ru'],
                    'en' => $result['name.eng']
        ]);
        $authors = Index::data($result['author'], 'en', 'name');
        $description = Index::dataString($result['description'], 'ru');
        $genres = Index::data($result['genre'], 'ru', 'name');
        $iNames = Index::index($names);
        $params['readmanga.image'] = [
            $iNames['en'],
            $result['cover'],
        ];
        $event['download.prefix']='conver';
        $params['grabber.db.short'] = [
            'name' => $names,
            'author' => $authors,
            'genre' => $genres
        ];
        $params['grabber.db.long'] = [
            'description' => $description
        ];


        $params['grabber.relations'] = [
            'name_relations_author' => Index::relations($iNames, Index::index($authors, 'en')),
            'name_relations_description' => Index::relations($iNames, Index::index($description)),
            'name_relations_genre' => Index::relations($iNames, Index::index($genres)),
            'name_relations_alias' => Index::relationsByUniq($iNames, $iNames)
        ];
        if (!empty($result['related.manga'])) {
            $params['grabber.relations']['name_relations_related'] = Index::relations($iNames, Index::uniq($result['related.manga'], 'en', 'name'));
        }
        if (!empty($result['like'])) {
            $params['grabber.relations']['name_relations_like'] = Index::relations($iNames, Index::uniq($result['like'], 'en', 'name'));
        }
        $params['grabber.date.year'] = ['en', $iNames['en'], $result['year']];
        $params['grabber.status'] = ['en', $iNames['en'], ($result['status'] == "завершен") ? 1 : 0];
        $params['link'] = &$result['chapters'];
        $event['params'] = $params;
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'readmanga.test' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        Grabber\Events\readmanga::content($event['http.event']);
    }, 'readmanga.image' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        \Grabber\Functions\event([
            'name' => 'grabber.image',
            'http.cfg' => [
                'method' => 'get',
                'options' => [
                    'base_url' => 'http://readmanga.me',
                    'future' => true,
                    'defaults' => [
                        'debug' => false
                    ]
                ]
            ],
            'download' => $event['params']['readmanga.image'],
            'download.prefix' => $event['download.prefix'],
            'download.events' => [
                Http::Complete => [
                    'grabber.image.property',
                ],
                Http::Error => [
                    'grabber.error',
                ]
            ],
            'source' => 'readmanga',
//            'job.count'=>(count($event['params']['readmanga.image'])*2)+2,
//            'job.current'=>1,
            'job.on'=>false
        ]);
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'readmanga.resource.link' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        console('event: ' . $name, $event['params']['link']);
        $model = \Grabber\Model\resource\link::getInstance();
        $model->setPrefix('readmanga');
        foreach ($event['params']['link'] as $param) {
            $link = &$param['link'];
            $hash = md5($link);
            $size = strlen($link);
            $id = $size . ':' . $hash;
            $ids[] = $id;
            $datas[$id] = [
                'link' => $link,
                'md5' => $hash,
                'size' => $size,
                'id' => $id
            ];
        }
        $error = \Grabber\Functions\error($event, $dispatcher);
        $sql = $model->Builder()->select('*')->where()->in('id', $ids)->back()->build();
        $success = function($result)use($model, $datas, $error) {
            foreach ($result as $row) {
                unset($result[$row['id']]);
            }
            console(__FILE__ . ':' . __LINE__, $result);
            foreach ($datas as $data) {
                $model->insert($data, null, $error);
            }
        };
        $try = function() use($model, $sql, $success, $error) {
            $model->Fetch($sql, $success, $error);
        };
        $model->Fetch($sql, $success, function($e)use($try, $model, $error) {
            if ($e->getCode() === 1146) {
                $model->install($try, $error);
            } else {
                call_user_func($error, $e);
            }
        });
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'vk.access' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        $event['vk'] = new \ApiClient\Config([
            'name' => 'vk',
            'access' => '1e1b8ad517dae6615aab9a04a18ecaffade9e2b106b2c362242d7f11786c44f9452bf79d89508da49b1d0',
            'group_id' => 59076975,
            'secret' => '50026e1f3d477735ac',
            'app_id' => 4704340,
            'scopes' => [
                'offline',
                'nohttps',
                'stats',
                'notify',
                'friends',
                'photos',
                'docs',
                'notes',
                'status',
                'groups'
            ]
        ]);
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'vk.createAlbum' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        $params = $event['params'];
        /* @var $config  \ApiClient\Config */
        $config = &$event['vk'];
        \ApiClient\Kernel::getInstance()->api($config, 'photos.createAlbum', [
            'title' => $title,
            'group_id' => $config->group_id,
            'description' => $description,
            'privacy' => 0,
            'comment_privacy' => 0,
            'upload_by_admins_only' => 1,
            'comments_disabled' => 0,
            'access_token' => $config->access
                ], [], function($data)use($access_id, $service) {
            Grabber\Model\album_relations_access::getInstance()->insert([
                'service' => $service,
                'album' => $data['id'],
                'access' => $access_id,
                'id' => $service . ':' . $data['id'] . ':' . $access_id
            ]);
        });

        $event['params'] = $params;
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }
        ];
        