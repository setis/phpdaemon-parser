<?php

use PHPDaemon\Applications\Sql\Builder,
    PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    PHPDaemon\Applications\Api\Response,
    Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent,
    PHPDaemon\Applications\BootStrap\Events;

return [
    'lang.add' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        \console\console(__METHOD__, $event->getArguments());
        Manga\Model\lang::getInstance()->insert($event->getArguments(), function() {
                    response(true);
                }, function(\Exception $e) {
                    response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                });
            },
                    'lang.edit' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                \console\console(__METHOD__, $event->getArguments());
                Manga\Model\lang::getInstance()->update($event->getArguments(), function() {
                    response(true);
                }, function(\Exception $e) {
                    response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                });
            },
                    'lang.view' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                Manga\Model\lang::getInstance()->select(function($data) {
                    response($data);
                }, function(\Exception $e) {
                    response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                }, $event['offset'], $event['limit'], '*', $event['sort'], $event['order']);
            },
                    'lang.view.all' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                $model = Manga\Model\lang::getInstance();
                $model->Fetch($model->Builder()->select('*')->build(), function($data) {
                    response($data);
                }, function(\Exception $e) {
                    response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                });
            },
                    'lang.search' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                $model = Manga\Model\lang::getInstance();
                $s = $event['search'];
                $sql = $model->Builder()->select('*')->where()->like('code', "%$s%")->newOr()->like('lang', "%$s%")->back()->build();
                \console\console($name, $sql);
                $model->Fetch($sql, function($data) {
                    response($data);
                }, function(\Exception $e) {
                    response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                });
            },
                    'lang.remove' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                Manga\Model\lang::getInstance()->delete($event['id'], function() {
                    response(true);
                }, function(\Exception $e) {
                    response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                });
            },
                    'lang.remove.all' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                $model = Manga\Model\lang::getInstance();
                $model->Query($model->Builder()->delete()->where()->in('code', $event->getArguments())->back()->build(), function() {
                    response(true);
                }, function(\Exception $e) {
                    response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                });
            },
                    'translator.add' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                if (Daemon::$req->attrs->server['REQUEST_METHOD'] === 'OPTIONS') {
                    http\response(true);
                    return;
                }elseif (!isset(Daemon::$req->attrs->post['in'])) {
                    http\response(['error' => 'not found field:in data','in'=>Daemon::$req->attrs]);
                    return;
                }
                $in = json_decode(Daemon::$req->attrs->post['in'], true);
                if ($in === null) {
                    http\response(['error' => 'json_decode ' . json_last_error_msg()]);
                }
                $time = mvc\model::timestamp();
                $in['time'] = $time;
                \console\console($name.__LINE__);
                $result = \Manga\Action\Upload::getInstance()->handler('translator');
                Manga\Model\translator::getInstance()->insert($in, function($id)use(&$result) {
                    $img = \Manga\Model\img::getInstance()->setPrefix('translator');
                    $doc = [
                        'id' => $id,
                        'time' => time()
                    ];
                    $storage = \Manga\Model\storage::getInstance()->setPrefix('translator');
                    foreach ($result as $path => $file) {
                        $s = pathinfo($path);
                        $spath = $s['dirname'] . '/s_' . $s['basename'];
                        $mpath = $s['dirname'] . '/m_' . $s['basename'];
                        $storage->insert([
                            'hash' => [],
                            'type' => 'fs',
                            'cfg' => [
                                'path' => $spath
                            ],
                            'view' => '/image/translator/s_' . $s['basename'],
                            'size' => null
                        ]);
                        $doc['img'][$file['name']]['75x75'] = [
                            'alt' => null,
                            'url' => [
                                'fs' => ['/image/translator/s_' . $s['basename']]
                            ]
                        ];
                        $storage->insert([
                            'hash' => [],
                            'type' => 'fs',
                            'cfg' => [
                                'path' => $mpath
                            ],
                            'view' => '/image/translator/m_' . $s['basename'],
                            'size' => null
                        ]);
                        $doc['img'][$file['name']]['200x200'] = [
                            'alt' => null,
                            'url' => [
                                'fs' => ['/image/translator/m_' . $s['basename']]
                            ]
                        ];
                        $storage->insert([
                            'hash' => [],
                            'type' => 'fs',
                            'cfg' => [
                                'path' => $path
                            ],
                            'view' => '/image/translator/' . $s['basename'],
                            'size' => null
                        ]);
                        $doc['img'][$file['name']]['original'] = [
                            'alt' => null,
                            'url' => [
                                'fs' => ['/image/translator/' . $s['basename']]
                            ]
                        ];
                        Grabber\Functions\queueCall('\Manga\Action\Task::small', [$path, $spath]);
                        Grabber\Functions\queueCall('\Manga\Action\Task::medium', [$path, $mpath]);
                    }
                    $img->insert($doc);
                    http\response(true);
                }, function(\Exception $e) {
                    http\response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                });
            },
                    'translator.view' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                BootStrap\Benchmark::start($name);
                Manga\Model\translator::getInstance()->select(function($data) {
                    $in = [];
                    $result = [];
                    foreach ($data as $v) {
                        $in[] = $v['id'];
                        $result[$v['id']] = $v;
                    }
                    \Manga\Model\img::getInstance()->setPrefix('translator')->Collection()->findAll(function($b)use(&$result) {
                        foreach ($b as $v) {
                            $result[$v['id']]['fid'] = (string) $v['_id'];
                            $result[$v['id']]['img'] = $v['img'];
                        }
                        BootStrap\Benchmark::stop('translator.view', true);
                        http\response(array_values($result));
                    }, ['id' => ['$in' => $in]]);
                }, function(\Exception $e) {
                    BootStrap\Benchmark::stop('translator.view1', true);
                    http\response([
                        'error' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]);
                }, $event['offset'], $event['limit'], '*', $event['sort'], $event['order']);
            },
                    'translator.img.remove' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                $collection = \Manga\Model\img::getInstance()->setPrefix('translator')->Collection();
                $storage = \Manga\Model\storage::getInstance()->setPrefix('translator');
                $func = function($storage, $type, $path) {
                    $wrapper = clone $storage;
                    console\console(['type' => $type, 'view' => (is_array($path)) ? ['$in' => $path] : $path]);
                    $wrapper->findOne(function($result)use($wrapper) {
                        console\console($result);
                        switch ($result['type']) {
                            case 'fs':
                                if (file_exists($result['cfg']['path'])) {
                                    unlink($result['cfg']['path']);
                                }
                                $wrapper->remove(['_id' => $result['_id']]);
                                break;
                        }
                    }, ['type' => $type, 'view' => (is_array($path)) ? ['$in' => $path] : $path]);
                };
                $collection->findOne(function($b)use($event, $collection, $func, $storage) {
                    console\console($b['img'][$event['name']]);
                    if (isset($b['img'][$event['name']])) {
                        foreach ($b['img'][$event['name']] as &$v) {
                            foreach ($v['url'] as $type => $path) {
                                $func(clone $storage, $type, $path);
                            }
                        }
                        unset($b['img'][$event['name']]);
                        $collection->update(['_id' => $b['_id']], ['$set' => ['img' => $b['img']]]);
                    }
                    http\response(true);
                }, ['id' => $event['id'], 'img' => $event['name'], '_id' => new MongoId($event['fid'])]);
            },
                    'translator.img.add' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
                $collection = \Manga\Model\img::getInstance()->setPrefix('translator')->Collection();
                $storage = \Manga\Model\storage::getInstance()->setPrefix('translator');
                $collection->findOne(function($b)use($event, &$storage, &$collection) {
                    if (isset($b['img'][$event['name']])) {
                        $arr = $b['img'][$event['name']];
                        foreach ($arr as $v) {
                            foreach ($v['url'] as $type => $path) {
                                $storage->findOne(function($result)use(&$storage) {
                                    switch ($result['type']) {
                                        case 'fs':
                                            if (file_exists($result['cfg']['path'])) {
                                                unlink($result['cfg']['path']);
                                            }
                                            $storage->remove(['_id' => $result['_id']]);
                                            break;
                                    }
                                }, ['type' => $type, 'view' => $path]);
                            }
                        }
                        unset($b['img'][$event['name']]);
                        $collection->update(['_id' => $b['_id']], ['$set' => ['img' => $b['img']]]);
                    }
                    http\response(true);
                }, ['id' => $event['id'], 'img' => $event['name'], '_id' => new MongoId($event['fid'])]);
            },
                ];
                