<?php

use Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent,
    GuzzleHttp\Event\CompleteEvent,
    GuzzleHttp\Event\ErrorEvent,
    Grabber\Consts\Http,
    Grabber\Kernel,
    Grabber\Parse\readmanga,
    PHPDaemon\Core\Daemon;

return [
    'grabber.complete' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        if ($event->isPropagationStopped()) {
            return false;
        }
//        Daemon::log('--------Grabber Complete-------->' . "\n\t" .
////                'id: ' . $event->getArgument('id') . "\n\t" .
//                'url: ' . $event->getArgument('url') . "\n\t" .
////                'events: ' . implode(',', $event->getArgument('event.listeners')) . "\n\t" .
////                'params: ' . json_encode($event->getArgument('params')) . "\n\t" .
//                'status: ' . "$numerator/$count\n"
//        );
    }, 'grabber.start' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        if (isset($event['job'])) {
            $count = (isset($event['job.count'])) ? $event['job.count'] : 1;
            $current = (isset($event['job.current'])) ? $event['job.current'] : 0;
            $event['job']->sendStatus($current, $count);
        }
    }, 'grabber.finish' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        if (isset($event['job'])) {
            $count = (isset($event['job.count'])) ? $event['job.count'] : 1;
            $event['job']->sendStatus($count, $count);
        }
        console("event: $name url: {$event['url']}");
        $event = null;
    }, 'grabber.error' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $result = $event->getArguments();
        if (isset($result['exception'])) {
            $e = $result['exception'];
            $result['exception'] = [
                'exception' => get_class($e),
                'code' => $e->getCode(),
                'msg' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'trace' => $e->getTrace()
            ];
        }
        info($name, $result);
        console($name, $result['exception']['msg'], $result);
//        unset($result['storage']);
        unset($result['html']);
        if (isset($event['job.count'])) {
            $count = $event['job.count'];
            /* @var $job \GearmanJob */
            $job = $event['job'];
            $job->sendStatus($count, $count);
        }
    }, 'grabber.queue' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        try {
            \Grabber\Functions\queue($event->getArguments());
        } catch (\Exception $e) {
            console\msg("error-$name", $event->getArguments(), dumpException($e));
            throw $e;
        }
    }, 'grabber.relations' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = \Grabber\Model\register::getInstance();
        foreach ($event[$name] as $table => $array) {
            $model->setTable($table);
            foreach ($array as $data) {
                $model->insert($data, null, \Grabber\Functions\dbError(clone $model, $data, $error));
            }
        }
        $event['job']->sendStatus(++$event['job.current'], $event['job.count']);
    }, 'grabber.db.long' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        $model = \Grabber\Model\long::getInstance();
        $error = \Grabber\Functions\error($event, $dispatcher);
        foreach ($event[$name] as $table => $array) {
            $model->setTable($table);
            foreach ($array as $data) {
                $model->insert($data, null, \Grabber\Functions\dbError($model, $data, $error));
            }
        }
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'grabber.db.short' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        $model = \Grabber\Model\short::getInstance();
        $error = \Grabber\Functions\error($event, $dispatcher);
        foreach ($event[$name] as $table => $array) {
            $model->setTable($table);
            foreach ($array as $data) {
                $model->insert($data, null, \Grabber\Functions\dbError($model, $data, $error));
            }
        }
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'grabber.date.year' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        console($name,$event[$name]);
        list($lang, $uid, $years) = $event[$name];
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = clone \Grabber\Model\year::getInstance();
        if (empty($years)) {
            console($name, $years);
        } else {
            foreach ($years as $year) {
                if (strlen($year) > 4) {
                    info($name, $years, $event['params'], $event['url']);
                }
                $data = [
                    'id' => $year . ':' . $uid,
                    'lang' => $lang,
                    'year' => $year,
                    'name' => $uid
                ];
                $model->insert($data, null, \Grabber\Functions\dbError($model, $data, $error));
            }
        }
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'grabber.download.fs' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        $cfg = $event[$name];
        /* @var $e \GuzzleHttp\Event\CompleteEvent */
        $e = $event['http.event'];
        $content = $e->getResponse()->getBody()->getContents();
        $url = $event['url'];
        if (empty($cfg['dir'])) {
            $cfg['dir'] = '/tmp';
        }
        if (empty($cfg['prefix'])) {
            $cfg['prefix'] = 'grabber_';
        }
        $write = function (\PHPDaemon\FS\File $file)use($content, &$event, $url) {
            $event['save'] = $file->path;
            $file->write($content);
        };
        switch ($cfg['file']) {
            case 'tmpname':
                \PHPDaemon\FS\FileSystem::tempnam($cfg['dir'], $cfg['prefix'], $write);
                break;
            case 'original':
            default:
                \PHPDaemon\FS\FileSystem::open($cfg['dir'] . '/' . pathinfo($url, PATHINFO_BASENAME), 'a+', $write);
                break;
        }
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'grabber.status' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        list($lang, $uid, $status) = $event[$name];
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = clone \Grabber\Model\status::getInstance();
        $data = [
            'id' => "$status:$uid",
            'lang' => $lang,
            'status' => $status,
            'name' => $uid,
        ];
        $model->insert($data, null, \Grabber\Functions\dbError($model, $data, $error));
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    }, 'grabber.image' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
//          \console\msg($name, get_class($event), $event->getArguments());
        if ($event->isPropagationStopped()) {
            return false;
        }
        if (!isset($event['download']) && count($event['download']) !== 0) {
            \console\console('error-' . $name, $event->getArguments());
            $event->stopPropagation();
            return false;
        }

        $links = $event['download'];
        $iname = $event['download.name'];
        $source = $event['source'];
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = \Grabber\Model\image\source::getInstance();
        $model->setPrefix($event['download.prefix']);
        foreach ($links as &$link) {
            \console\console($name . ':' . __LINE__, $link);
            $link = (string) $link;
            $hash = md5((string) $link);
            $size = strlen($link);
            $id = "$source:$size:$hash";
            $uniq = "$id|$iname";
            $datas[$id] = [
                'id' => $id,
                'uniq' => $uniq,
                'md5' => $hash,
                'size' => $size,
                'name' => $iname,
                'link' => $link,
                'source' => $source
            ];
        }
//        \console\msg($name, $datas);
        $sql = "SELECT * FROM {$model->table} WHERE id not in(" . mvc\model::in(array_keys($datas)) . ");";
        $error = \Grabber\Functions\error($event, $dispatcher);
        $success = function($result)use($datas, $dispatcher, $event) {
            $arr = [];
            if (count($result) !== 0) {
                foreach ($result as $row) {
                    $arr[$row['id']] = $row['link'];
                }
            } else {
                foreach ($datas as $id => $data) {
                    $arr[$id] = $data['link'];
                }
            }
            $cfg = $event['http.cfg'];
            $cfg['url'] = $arr;
            $cfg['events'] = (isset($event['download.events'])) ? $event['download.events'] : [
                Http::Complete => [
                    'grabber.image.property',
                ],
                Http::Error => [
                    'grabber.error',
                ]
            ];
            $event['grabber.image.source'] = $datas;
            $event['http.cfg'] = $cfg;
            if (count($arr) !== 0) {
                $dispatcher->dispatch('grabber.image.source', $event);
                $dispatcher->dispatch('grabber.image.download', $event);
            } else {
                $dispatcher->dispatch('grabber.finish', $event);
            }
        };
        $model->Fetch($sql, $success, function($e)use($sql, $success, $model, $error) {
            if ($e->getCode() === 1146) {
                $model->install(function() use($model, $sql, $success, $error) {
                    $model->Fetch($sql, $success, $error);
                }, $error);
            } else {
                call_user_func($error, $e);
            }
        });
    }, 'grabber.image.source' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = \Grabber\Model\image\source::getInstance();
        $model->setPrefix($event['download.prefix']);
//        console($name, $model->table, $event['download']);
        foreach ($event[$name] as $data) {
            $model->insert($data, null, \Grabber\Functions\dbError($model, $data, $error));
        }
    }, 'grabber.image.download' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        \Grabber\Kernel::getInstance()->handler($event);
    }, 'grabber.image.property' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
//        console($name, $event['url']);
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = \Grabber\Model\image\property::getInstance();
        $model->setPrefix($event['download.prefix']);
//        $model->install(null, function() {
//            
//        });
        /* @var $CompleteEvent CompleteEvent */
        $CompleteEvent = $event['http.event'];
        $upload = $event['download'];
        $request = $CompleteEvent->getRequest();
        $url = $request->getPath();
        $id = array_search($url, $upload);
        if ($id === false) {
            $url = $request->getUrl();
            $id = array_search($url, $upload);
        }

        $body = $CompleteEvent->getResponse()->getBody();
        $image = $body->getContents();
        $md5 = md5($image);
        $size = $body->getSize();
        list($width, $height, $type) = getimagesizefromstring($image);
        $uniq = "$width:$height:$size:$type:$md5";
        $arr = [$id, $uniq];

        $data = [
            'id' => $uniq,
            'width' => $width,
            'height' => $height,
            'size' => $size,
            'type' => $type,
            'md5' => $md5,
            'name' => pathinfo($event['url'], PATHINFO_FILENAME)
        ];
        $success = function()use($dispatcher, &$event, $name, $arr) {
            $event['download.id'] = $arr;
//            \console\msg($name, ($event['download.id'] === $arr), $arr);
            $dispatcher->dispatch('grabber.image.register', $event);
        };
        $event['http.event'] = null;
        $event['http.cfg'] = null;
        $model->insert($data, $success, \Grabber\Functions\dbError($model, $data, $error, $success, $success));
    }, 'grabber.image.register' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
//        \console\msg($name, $event['download.id'], $event['download.name']);
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = \Grabber\Model\image\register::getInstance();
        $model->setPrefix($event['download.prefix']);
//        $model->install(null, function() {
//            
//        });
        $iname = $event['download.name'];
        list($source, $property) = $event['download.id'];
        $data = [
            'id' => "$source|$property|$iname",
            'source' => $source,
            'property' => $property,
            'name' => $iname
        ];
//        \console\msg($name, $data);
        $success = function()use($dispatcher, $event) {
            $dispatcher->dispatch('grabber.finish', $event);
        };
        $model->insert($data, $success, \Grabber\Functions\dbError($model, $data, $error, $success, $success));
    }, 'grabber.versionist' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        if (!isset($event[$name])) {
            $event->stopPropagation();
            $event['exception'] = new \Exception($name . ' not found params');
            $dispatcher->dispatch('grabber.exception', $event);
            return false;
        }
        $params = $event[$name];
        $error = \Grabber\Functions\error($event, $dispatcher);
        $model = \Grabber\Model\versionist\id::getInstance();
        foreach($params as $v){
            $model->insert($data, $success, \Grabber\Functions\dbError($model, $data, $error, $success, $success));
        }
        
        $event['job']->sendStatus(++$event['job.current'], $event['job.count']);
    }, 'grabber.info' => function(GenericEvent $event, $name, EventDispatcher $dispatcher) {
        $event['event.action'] = $name;
        if ($event->isPropagationStopped()) {
            return false;
        }
        $array = $event->getArguments();
        foreach (['job', 'job.current', 'job.count', 'http.event', 'http.cfg', 'url'] as $key) {
            if (isset($array[$key])) {
                unset($array[$key]);
            }
        }
        \console\msg("info-{$event['name']}", $event['url'], $array);
        $n = $event['job.current'] + 1;
        $event['job.current'] = $n;
        $event['job']->sendStatus($n, $event['job.count']);
    },
        ];
        