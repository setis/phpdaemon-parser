<?php

use \BootStrap\Env,
    PHPDaemon\Applications\Api\Consts\Config,
    PHPDaemon\Applications\Api\Consts\Method,
    PHPDaemon\Applications\Api\Consts\Mode;

/* @var $bootstrap \PHPDaemon\Applications\BootStrap\Kernel */
Env::$BootStrap = $bootstrap;
Env::$events = Env::$config . 'Events/';
Env::$functions = Env::$config . 'Functions/';
Env::$gearman = Env::$config . 'Gearman/';
Env::$appConfig = Env::$config . 'App/';
$bootstrap->load([
    'autoload' => [
        'autoload' => dirname(__DIR__) . '/vendor/autoload.php',
        'include' => dirname(__DIR__)
    ],
    'recaptcha' => [
        '*' => [
            'public_key' => '6LdMc_gSAAAAAOX5LTecZqnhAnJNUgcPcjhfa3hW',
            'private_key' => '6LdMc_gSAAAAAP6i3-P26v_4vg0k7Dnvk_PvcGgb',
            'ssl' => false
        ]
    ],
    'app' => [
        'cache' => true,
        'apps' => [
            'PHPDaemon\Applications\ApiServer\Server' => 'api',
            'PHPDaemon\Applications\Gearman\Client' => 'gearman.client',
            'PHPDaemon\Applications\Gearman\Worker' => 'gearman.worker',
            'PHPDaemon\Applications\OAuth\External' => 'oauth',
            'PHPDaemon\Applications\Grabber\Kernel' => 'grabber',
        ]
    ],
    'functions' => [
        [
            'functions' => [
                'event' => true,
                'response' => true,
                'session' => true,
                'user' => true,
                'stack' => true,
                'exception' => true,
                'console' => true,
            ],
            'path' => Env::$functions . 'generals.php'
        ],
        [
            'path' => Env::$functions . 'http.php'
        ],
        [
            'path' => Env::$functions . 'debug.php'
        ],
        [
            'path' => Env::$functions . 'console.php'
        ],
        [
            'path' => Env::$functions . 'grabber.php'
        ]
    ],
    'gearman' => [
        'functions' => [
            'grabber' => [
//                'path' => Env::$gearman . 'grabber.php',
                'call' => '\Grabber\Functions\job',
//                'timeout'=>-1
            ],
            'dispatch' => [
//                'path' => Env::$gearman . 'grabber.php',
                'call' => '\Grabber\Functions\dispatch',
//                'timeout'=>-1
            ],
            'call' => [
//                'path' => Env::$gearman . 'grabber.php',
                'call' => '\Grabber\Functions\jobCall',
//                'timeout'=>-1
            ]
        ],
        'jobs' => [
            'grabber' => true,
            'dispatch' => true,
            'call' => true,
        ]
    ],
    'events' => [
        'api' => Env::$events . 'api.php',
        'manga' => Env::$events . 'manga.php',
        'readmanga' => Env::$events . 'readmanga.php',
        'grabber' => Env::$events . 'grabber.php',
    ],
    'poll' => [
        'redis' => [
            'redis',
            'tcp://127.0.0.1:6379'
        ],
        'grabber' => [
            'mysql',
            [
                'server' => 'tcp://root:123123@127.0.0.1/grabber',
                'name' => 'manga',
                'protologging' => 1,
                'privileged' => 1,
                'enable' => 1
            ]
        ],
        'manga' => [
            'mysql',
            [
                'server' => 'tcp://root:123123@127.0.0.1/manga',
                'name' => 'manga',
                'protologging' => 1,
                'privileged' => 1,
                'enable' => 1
            ]
        ]
    ],
]);
$bootstrap->on([
    'autoload',
    'functions',
    'events',
    'app',
    'pollGuzzleHttp',
    'poll',
    'console',
    'gearman',
]);
Env::$cfg['Grabber'] = 'manga';
BootStrap\Console::getInstance()->logger['sql'] = dirname(__DIR__).'/logs.sql';
use Grabber\Index\Storage;
//
//Storage::$length = [
//    //2^4 = 16 int(2)
//    Storage::type_lang => 4,
//    //2^7 = 128 int(3)
//    Storage::type_source => 7,
//    //2^7 = 128 int(3)
//    Storage::type_resource => 7,
//    //2^1 = 2 bit(1)
//    Storage::type_status => 1,
//    //2^11 = 2048 int(4)
//    Storage::type_year => 11,
//    // 2^10 = 1024 int(4)
//    Storage::type_chapter => 10,
//    // 2^9 = 512 int(3)
//    Storage::type_page => 9,
//    //2^5 = 32 int(2)
//    Storage::type_image => 5,
//    //2^8 = 256 int(3)
//    Storage::type_size_name => 8,
//    //2^11 = 2048 int(4)
//    Storage::type_size_url => 11,
//    //2^12 = 4096 int(4)
//    Storage::type_image_width => 12,
//    //2^12 = 4096 int(4)
//    Storage::type_image_height => 12,
//];
//Storage::$values = [
//    //2^4 = 16 int(2)
//    Storage::type_lang => 16,
//    //2^7 = 128 int(3)
//    Storage::type_source => 128,
//    //2^7 = 128 int(3)
//    Storage::type_resource => 128,
//    //2^1 = 2 bit(1)
//    Storage::type_status => 2,
//    //2^11 = 2048 int(4)
//    Storage::type_year => 2048,
//    // 2^10 = 1024 int(4)
//    Storage::type_chapter => 1024,
//    // 2^9 = 512 int(3)
//    Storage::type_page => 512,
//    //2^5 = 32 int(2)
//    Storage::type_image => 32,
//    //2^8 = 256 int(3)
//    Storage::type_size_name => 256,
//    //2^11 = 2048 int(4)
//    Storage::type_size_url => 2048,
//    //2^12 = 4096 int(4)
//    Storage::type_image_width => 4096,
//    //2^12 = 4096 int(4)
//    Storage::type_image_height => 4096,
//];
//

Storage::load([
    'readmanga'
        ], Storage::type_source);
Storage::load([
    'readmanga',
    'vk'
        ], Storage::type_resource);

Storage::load([
    'en',
    'ru',
        ], Storage::type_lang);
//Storage::load([
//    0,
//    1,
//        ], Storage::type_status);
//Storage::load([
//    0,
//    1500,
//        ], Storage::type_chapter);
//Storage::load([
//    0,
//    300,
//        ], Storage::type_page);
//Storage::load([
//    0,
//    16,
//        ], Storage::type_image);
//Storage::load([
//    0,
//    255,
//        ], Storage::type_size_name);
\Grabber\Index\Data\Short::initilize();
\Grabber\Index\Data\Long::initilize();
\Grabber\Index\Data\Year::initilize();
\Grabber\Index\Data\Status::initilize();
\Grabber\Index\Data\Chapter::initilize();
\Grabber\Index\Data\Image\property::initilize();
\Grabber\Index\Data\Image\chapter::initilize();
\Grabber\Index\Data\Url::initilize();
