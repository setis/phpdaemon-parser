<?php

namespace BootStrap;

use Exception,
    PHPDaemon\Applications\Sql\Builder,
    PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    PHPDaemon\Applications\Api\Response,
    Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent;

class Events {

    use \Traits\getInstance;

    /**
     *
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    public static $EventDispatcher;

    /**
     * 
     * @var array
     */
    public $loaded;

    /**
     * 
     * @var array
     */
    public $not_loaded;

    /**
     *
     * @var array|string
     */
    public $events = [];

    public function __construct(array $list = null) {
        self::$EventDispatcher = new EventDispatcher();
        foreach ($this->initEvent() as $eventName => &$listener) {
            self::$EventDispatcher->addListener($eventName, $listener);
        }
        if ($list) {
            $this->loadList($list);
        }
    }

    public function loadList(array $list) {
        foreach ($list as $group => $file) {
            $this->load($file, $group);
        }
        return $this;
    }

    public function load($file, $group) {
        $path = realpath($file);
        $id = 'W#' . Daemon::$process->getPid();
        if (file_exists($path) && is_readable($path)) {
            $events = include $path;
//            $listeners = array_merge($this->eventDispatcher->getListeners(), $events);
            $this->loaded[$group] = $path;
            Daemon::log($id . ' EVENTS: loaded events ' . "$group(" . count($events) . ") -> $path");
            $this->addGroup($group, array_keys($events));
            foreach ($events as $eventName => &$listener) {
                if (self::$EventDispatcher->hasListeners($eventName)) {
                    Daemon::log($id . ' EVENTS: duplicate  event->' . "$eventName $group -> $path");
                }
                self::$EventDispatcher->addListener($eventName, $listener);
            }
            return true;
        } else {
            $this->not_loaded[$group] = [
                'path' => $path,
                'file' => $file,
                'read' => is_readable($path)
            ];
            Daemon::log($id . ' EVENTS: not load events ' . "$group($file) -> $path read->" . var_export(is_readable($path), true));
            return false;
        }
    }

    public function getGroup($group) {
        if (is_string($group) && isset($this->events[$group])) {
            return $this->events[$group];
        } elseif (is_array($group)) {
            $good = $bad = $result = [];
            foreach ($group as $subgroup) {
                if (isset($this->events[$subgroup])) {
                    foreach($this->events[$subgroup] as $event){
                        $result[] = $event;
                    }
                    $good[] = $subgroup;
                } else {
                    $bad[] = $subgroup;
                }
            }
            $uniq = array_unique($result);
            if(count($uniq)!==  count($result)){
                Daemon::log('Event::getGroup: not uniq events');
            }
            if(count($bad)>0){
                Daemon::log('Event::getGroup: not found -> '.  implode(',', $bad));
            }
            if(count($good)>0){
                Daemon::log('Event::getGroup: found -> '.  implode(',', $good));
            }
            return $uniq;
        }
        return [];
    }

    public function setGroup($group, $events) {
        $this->events[$group] = $events;
    }

    public function addGroup($group, $events) {
        if (!isset($this->events[$group])) {
            $this->events[$group] = $events;
        } else {
            $this->events[$group] = array_merge($this->events[$group], $events);
        }
    }

    public function isGroup($group) {
        return (isset($this->events[$group]));
    }

    public function initEvent() {
        return [];
    }

    public function addListeners(array &$listeners) {
        foreach ($listeners as $event => &$listener) {
            self::$EventDispatcher->addListener($event, $listener);
        }
        return $this;
    }

    public function dispatch($name, $self, array $arguments = []) {
        self::$EventDispatcher->dispatch($name, new GenericEvent($self, $arguments));
    }

    const
            EVENT_SQL_INSERT_SUCCESS = 0,
            EVENT_SQL_INSERT_ERROR = 1,
            EVENT_SQL_REMOVE_SUCCESS = 2,
            EVENT_SQL_REMOVE_ERROR = 3,
            EVENT_SQL_UPDATE_SUCCESS = 4,
            EVENT_SQL_UPDATE_ERROR = 5,
            EVENT_SQL_SELECT_SUCCESS = 6,
            EVENT_SQL_SELECT_ERROR = 7,
            EVENT_SQL_ERROR = 8,
            EVENT_SQL_SUCCESS = 9,
            EVENT_EXCEPTION = 10,
            EVENT_ON = 11;

    public function dispatch_template($name, $template, $self = null) {
        switch ($template) {
            case self::EVENT_SQL_INSERT_SUCCESS:
                return function($query)use($self, $name) {
                    self::$EventDispatcher->dispatch($name, new GenericEvent($self, [$query->insertId]));
                };
            case self::EVENT_SQL_INSERT_ERROR:
            case self::EVENT_SQL_REMOVE_ERROR:
            case self::EVENT_SQL_UPDATE_ERROR:
            case self::EVENT_SQL_SELECT_ERROR:
            case self::EVENT_SQL_ERROR:
                return function($error, $query = null)use($self, $name) {
                    self::$EventDispatcher->dispatch($name, new GenericEvent($self, [$error, $query]));
                };
            case self::EVENT_SQL_REMOVE_SUCCESS:
            case self::EVENT_SQL_UPDATE_SUCCESS:
            case self::EVENT_SQL_SUCCESS:
            case self::EVENT_ON:
                return function()use($name) {
                    self::$EventDispatcher->dispatch($name);
                };
            case self::EVENT_SQL_SELECT_SUCCESS:
                return function($result, $query = null)use($name, $self) {
                    self::$EventDispatcher->dispatch($name, new GenericEvent($self, [$result, $query]));
                };
            case self::EVENT_EXCEPTION:
                return function($exception)use($name, $self) {
                    self::$EventDispatcher->dispatch($name, new GenericEvent($exception, [$self]));
                };
        }
    }

    public function event_template($template, $callback) {
        switch ($template) {
            case self::EVENT_SQL_INSERT_SUCCESS:
            case self::EVENT_SQL_SELECT_SUCCESS:
            case self::EVENT_SQL_SUCCESS:
                return function(GenericEvent $event, $name, EventDispatcher $dispatcher)use($callback) {
                    call_user_func($callback, $event->getArgument(0));
                };
            case self::EVENT_SQL_INSERT_ERROR:
            case self::EVENT_SQL_REMOVE_ERROR:
            case self::EVENT_SQL_UPDATE_ERROR:
            case self::EVENT_SQL_SELECT_ERROR:
            case self::EVENT_SQL_ERROR:
                return function(GenericEvent $event, $name, EventDispatcher $dispatcher)use($callback) {
                    call_user_func($callback, $event->getArguments());
                };
            case self::EVENT_SQL_REMOVE_SUCCESS:
            case self::EVENT_SQL_UPDATE_SUCCESS:
                return function(GenericEvent $event, $name, EventDispatcher $dispatcher)use($callback) {
                    call_user_func($callback);
                };
        }
    }

}
