<?php

namespace BootStrap;

use \PHPDaemon\Clients\Redis\Pool as Redis,
    \PHPDaemon\Clients\MySQL\Pool as MySQL

;

class Poll {

    use \Traits\getInstance;

    /**
     *
     * @var \SplObjectStorage
     */
    public $Poll;

    /**
     *
     * @var array
     */
    public $cfg;

    public function __construct() {
        $this->Poll = new \SplObjectStorage();
        $this->cfg = [];
    }

    /**
     * 
     * @param array $config
     * @return $this|self|static
     */
    public function configure(array $config) {
        foreach ($config as $name => $cfg) {
            $this->cfg[$name] = $cfg;
        }
        return $this;
    }

    /**
     * 
     * @param string $type
     * @param string $connect
     * @return \PHPDaemon\Clients\Redis\Pool|\PHPDaemon\Clients\MySQL\Pool
     */
    public function connect($name, $type, $connect = '') {
        $cfg = $type . '://' . $name;
        foreach ($this->Poll as $resource => $url) {
            if ($url === $cfg) {
                return $resource;
            }
        }
        switch ($type) {
            case 'redis':
                $resource = Redis::getInstance($connect);
                break;
            case 'mysql':
                $resource = MySQL::getInstance($connect);
                break;
        }
        $this->Poll->attach($resource, $cfg);
        return $resource;
    }

    public function connectByName($name) {
        if (isset($this->cfg[$name])) {
            list($type, $connect) = $this->cfg[$name];
            return $this->connect($name, $type, $connect);
        } 
        return false;
    }

}
