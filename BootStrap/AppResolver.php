<?php

/**
 * Default application resolver
 *
 * @package Core
 * @author  Zorin Vasily <maintainer@daemon.io>
 */
use PHPDaemon\Core\Daemon;
use PHPDaemon\Core\ClassFinder;

class AppResolver extends PHPDaemon\Core\AppResolver {

    public $list = [
        'api' => '\PHPDaemon\Applications\ApiServer\Server',
        'oauth' => '\PHPDaemon\Applications\OAuth\External',
    ];
   
    public $appName = [];

    public function __construct() {
        $root = dirname(__DIR__) . '/';
        $this->appName = [
            'Gearman\Client' => $root . 'Gearman/Client.php',
            'Gearman\Worker' => $root . 'Gearman/Worker.php',
        ];
    }

    public function appName($class) {
        return (isset($this->appName[$class])) ? $this->appName[$class] : false;
    }

    /**
     * Routes incoming request to related application. Method is for overloading.
     * @param object Request.
     * @param object AppInstance of Upstream.
     * @return string Application's name.
     */
    public function getRequestRoute($req, $upstream) {
        $uri = $req->attrs->server['DOCUMENT_URI'];
        $log = 'W#' . Daemon::$process->getPid() . ' Request:' . $uri;
        $route = Route\Kernel::getInstance()->run($log,$uri);
        if($route !== false){
            return $route;
        }
        $arr = explode('/', $uri);
        $key = $arr[1];
        if (!empty($key) && in_array($key, array_keys($this->list))) {
            $val = '';
            if ($key === 'oauth') {
                $val = ':' . $arr[2];
            }
            Daemon::log($log . ' Controller:' . $this->list[$key] . $val);
            return $this->list[$key] . $val;
        }
        if (preg_match('~^/(WebSocketOverCOMET|Example.*)/?~', $req->attrs->server['DOCUMENT_URI'], $m)) {
            Daemon::log($log . ' Controller:' . $m[1]);
            return $m[1];
        }
        $upstream->badRequest($req);
        Daemon::log($log . ' bad');
        return false;
//        throw new \Exception();
        /* Example
          $host = basename($req->attrs->server['HTTP_HOST']);

          if (is_dir('/home/web/domains/' . basename($host))) {
          preg_match('~^/(.*)$~', $req->attrs->server['DOCUMENT_URI'], $m);
          $req->attrs->server['FR_PATH'] = '/home/web/domains/'.$host.'/'.$m[1];
          $req->attrs->server['FR_AUTOINDEX'] = TRUE;
          return 'FileReader';
          } */
    }

    /**
     * Resolve full name of application by its class and name
     * @param  string $appName  Application name
     * @param  string $instance Application class
     * @return string
     */
    public function getAppFullname($appName, $instance = '') {
        return $appName . ($instance !== '' ? ':' . $instance : '');
    }

    /**
     * Gets instance of application
     * @param  string  $appName  Application name
     * @param  string  $instance Instance name
     * @param  boolean $spawn    If true, we spawn an instance if absent
     * @param  boolean $preload  If true, worker is at the preload stage
     * @return object $instance AppInstance
     */
    public function getInstance($appName, $instance = '', $spawn = true, $preload = false) {
        $class = ClassFinder::find($appName, 'Applications');
        if (class_exists($appName)) {
            $class = $appName;
        } elseif (!class_exists($class)) {
            Daemon::$process->log(__METHOD__ . ': unable to find application class ' . json_encode($class) . '\'');
            return false;
        }
        if (!is_subclass_of($class, '\\PHPDaemon\\Core\\AppInstance')) {
            Daemon::$process->log(__METHOD__ . ': class ' . json_encode($class) . '\' found, but skipped as long as it is not subclass of \\PHPDaemon\\Core\\AppInstance');
            return false;
        }
        if (isset(Daemon::$appInstances[$class][$instance])) {
            return Daemon::$appInstances[$class][$instance];
        }
        if (!$spawn) {
            return false;
        }
        $fullname = $this->getAppFullname($appName, $instance);
        $fullnameClass = $this->getAppFullname($class, $instance);
        if ($fullname !== $fullnameClass && isset(Daemon::$config->{$fullname})) {
            Daemon::$config->renameSection($fullname, $fullnameClass);
        }
        if (!$preload) {
            /** @noinspection PhpUndefinedVariableInspection */
            if (!$class::$runOnDemand) {
                return false;
            }
            if (isset(Daemon::$config->{$fullnameClass}->limitinstances)) {
                return false;
            }
        }

        return new $class($instance);
    }

}

return new AppResolver();
