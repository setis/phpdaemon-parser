<?php

namespace BootStrap;

use PHPDaemon\Core\Daemon;

class Benchmark {

    use \Traits\getInstance;

    /**
     *
     * @var array
     */
    public $times = [];

    public static function start($label) {
        self::getInstance()->times[$label] = [
            'start' => [
                microtime(true),
                memory_get_usage(true)
            ]
        ];
    }

    public static function stop($label, $log = false) {
        self::getInstance()->times[$label]['stop'] = [
                microtime(true),
                memory_get_usage(true)
        ];
        if ($log) {
            $total = self::getInstance()->total($label);
            Daemon::log(__CLASS__.': '.$label."\ttotal time: $total[0]\t memory:$total[1]\t pid:".Daemon::$process->getPid());
        }
    }

    public function total($label) {
        $self = self::getInstance();
        if (isset($self->times[$label])) {
            $obj = $self->times[$label];
            return [
                $obj['stop'][0] - $obj['start'][0],
                $obj['stop'][1] - $obj['start'][1],
            ];
        }
        return;
    }

    public static function clear() {
        self::getInstance()->times = [];
    }

}
