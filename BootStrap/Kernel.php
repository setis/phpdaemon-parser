<?php

namespace PHPDaemon\Applications\BootStrap;

use PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    PHPDaemon\Core\AppInstance;

class Kernel extends AppInstance {

    protected function init() {
        include __DIR__ . '/Env.php';
        \BootStrap\Env::$root = dirname(__DIR__) . '/';
    }

    protected function getConfigDefaults() {
        return [];
    }

    /**
     * 
     * @param array $cfg
     * @return $this
     */
    public function load(array $cfg) {
        foreach ($cfg as $name => $setting) {
            $this->config->{$name} = $setting;
        }
        return $this;
    }

    /**
     * 
     * @param string $name
     * @param array $cfg
     * @return $this
     */
    public function set($name, array $cfg = null) {
        if ($cfg !== null) {
            $this->config->{$name} = $cfg;
        }
        if (!isset($this->config->mode[$name])) {
            $this->config->mode[$name] = false;
        } elseif ($this->config->mode[$name] === true) {
            $this->config->mode[$name] = true;
            $this->{$name}($cfg);
        }
        return $this;
    }

    /**
     * 
     * @param string $name
     * @return array
     */
    public function get($name) {
        if (isset($this->config->{$name})) {
            return $this->config->{$name};
        }
        return [];
    }

    /**
     * 
     * @param string $name
     * @return $this
     */
    public function on($name) {
        if (is_string($name)) {
            $this->config->mode[$name] = true;
            $this->{$name}($this->get($name));
        } elseif (is_array($name)) {
            foreach ($name as $name1) {
                $this->config->mode[$name1] = true;
                $this->{$name1}($this->get($name1));
            }
        }
        return $this;
    }

    public function console(array $cfg = null) {
        include __DIR__ . '/Console.php';
    }

    /**
     * 
     * @param string $name
     * @return $this
     */
    public function off($name) {
        $this->config->mode[$name] = false;
        return $this;
    }

    public function autoload(array $cfg) {
        $id = 'W#' . Daemon::$process->getPid();
        if (!isset($cfg['autoload'])) {
            Daemon::log($id . ' BootStrap->autoload.register:' . "not input autoload");
        } elseif (!file_exists($cfg['autoload'])) {
            Daemon::log($id . ' BootStrap->autoload.register:' . "not found file autoload");
        } elseif (!is_readable($cfg['autoload'])) {
            Daemon::log($id . ' BootStrap->autoload.register:' . "not permission read file autoload ({$cfg['autoload']})");
        } else {
            Daemon::log($id . ' BootStrap->autoload.register:' . "load autoload ({$cfg['autoload']})");
        }
        include $cfg['autoload'];
        if (!isset($cfg['include'])) {
            Daemon::log($id . ' BootStrap->autoload.include:' . "not input include");
        } elseif (!is_dir($cfg['include'])) {
            Daemon::log($id . ' BootStrap->autoload.include:' . "not found dir include");
        } elseif (!is_readable($cfg['include'])) {
            Daemon::log($id . ' BootStrap->autoload.include:' . "not permission read dir include ({$cfg['include']})");
        } else {
            Daemon::log($id . ' BootStrap->autoload.include:' . "set include path ({$cfg['include']})");
        }
        $path = (is_array($cfg['include'])) ? implode(PATH_SEPARATOR, $cfg['include']) : $cfg['include'];
        set_include_path(get_include_path() . PATH_SEPARATOR . $path);
    }

    public function pollGuzzleHttp() {
        \BootStrap\GuzzleHttp::getInstance();
    }

    public function events(array $cfg) {
        \BootStrap\Events::getInstance()->loadList($cfg);
    }

    public function recaptcha(array $cfg) {
        foreach ($cfg as $domain => $config) {
            \Captcha\Recaptcha::getInstance($domain)->configure($config);
        }
    }

    public function poll(array $cfg) {
        \BootStrap\Poll::getInstance()->configure($cfg);
    }

    public function functions(array $cfg) {
        $id = 'W#' . Daemon::$process->getPid();
        foreach ($cfg as $option) {
            $file = $option['path'];
            $func = (isset($option['functions'])) ? $option['functions'] : [];
            if (file_exists($file)) {
                include $file;
                Daemon::log($id.' BootStrap->func: loaded ' . "{$file}->" . implode(',', array_keys($func)));
            } else {
                Daemon::log($id.' BootStrap->func: not loaded ' . "{$file}->" . implode(',', array_keys($func)));
            }
        }
    }

    public function gearman(array $cfg) {
        /* @var $gearman \PHPDaemon\Applications\Gearman\Worker */
        $gearman = \PHPDaemon\Applications\Gearman\Worker::getInstance('');
        $jobs = $cfg['jobs'];
        $id = 'W#' . Daemon::$process->getPid();
        foreach ($cfg['functions'] as $name => $option) {
            if (isset($jobs[$name]) && $jobs[$name] === true) {
                $func = $option['call'];
                $timeout = (isset($option['timeout'])) ? $option['timeout'] : null;
                $file = null;
                if (isset($option['path'])) {
                    $file = $option['path'];
                    if (!file_exists($file)) {
                        Daemon::log($id . ' BootStrap->gearman: not found file:' . "name->$name,file->{$file}");
                    } elseif (!is_readable($file)) {
                        Daemon::log($id . ' BootStrap->gearman: not is read file:' . "name->$name,file->{$file}");
                    } else {
                        include realpath($file);
                    }
                }
                if (!is_callable($func)) {
                    Daemon::log(__METHOD__ . ':' . __LINE__, Debug::dump($name));
                    Daemon::log($id . ' BootStrap->gearman: not call:' . "name->$name" . ($file !== null) ? ",file->{$file}" : '' . ",call->" . (is_array($func)) ? json_encode($func) : 'callable');
                } else {
                    $gearman->addFunction($name, $func, $timeout);
                }
            }
        }
    }

    public function app(array $cfg) {
        \BootStrap\AppConfig::getInstance()->config($cfg);
    }

    public function onReady() {
        if ($this->isEnabled()) {
            $id = 'W#' . Daemon::$process->getPid();
            /* @var $cfg PHPDaemon\Config\Section */
            $cfg = $this->config->toArray();
            $i = 0;
            start_config:
            $i++;
            if (!isset($cfg['config'])) {
                Daemon::log($id.' BootStrap->boot.config:' . "not input include");
            } elseif (!is_dir($cfg['config'])) {
                $dir = \BootStrap\Env::$root . $cfg['config'];
                if (is_dir($dir) && $i === 1) {
                    $this->config->config = $cfg['config'] = $dir;
                    goto start_config;
                }
                Daemon::log($id. ' BootStrap->boot.config:' . "not found dir include");
            }
//            elseif (!is_readable($cfg['config'])) {
//                Daemon::log('BootStrap->boot.config:' . "not permission read dir include ({$cfg['config']})");
//            } 
            else {
                \BootStrap\Env::$config = realpath($cfg['config']) . '/';
                Daemon::log($id.' BootStrap->boot.config:' . "set dir config ({$cfg['config']})");
            }
            $i = 0;
            start_boot:
            $i++;
            if (!isset($cfg['boot'])) {
                Daemon::log($id.' BootStrap->boot.loader:' . "not input");
            } elseif (!file_exists($cfg['boot'])) {
                $file = \BootStrap\Env::$root . $cfg['boot'];
                if (file_exists($file) && $i === 1) {
                    $this->config->boot = $cfg['boot'] = $file;
                    goto start_boot;
                }
                Daemon::log($id.' BootStrap->boot.loader:' . "not found file {$cfg['boot']}");
            } elseif (!is_readable($cfg['boot'])) {
                Daemon::log($id.' BootStrap->boot.loader:' . "not permission read file ({$cfg['boot']})");
            } else {
                $this->config->boot = realpath($cfg['boot']);
                $bootstrap = $this;
                include $cfg['boot'];
                Daemon::log($id.' BootStrap->boot.loader:' . "load file ({$cfg['boot']})");
            }
        }
    }

}
