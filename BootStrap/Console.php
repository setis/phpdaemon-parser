<?php

namespace BootStrap {

    class Console {

        public $dir = '/var/www/phpdaemon/';
        public $uri = 'http://localhost/phpdaemon/';
        public $file;
        public $protocol = self::json;
        public $logger = [];

        /**
         *
         * @var \SplObjectStorage 
         */
        protected $_logger;

        const json = 1, xml = 2, html = 3, plain = 0;

        use \Traits\getInstance;

        public function __construct() {
            $this->_logger = new \SplObjectStorage();
        }

        public function info() {
            $file = microtime(true) . $this->ext();
            $path = $this->dir . $file;
            $url = $this->uri . $file;
            $data = func_get_args();
            \PHPDaemon\FS\FileSystem::open($path, 'a+!', function (\PHPDaemon\FS\File $file)use($data, $url, $path) {
                $file->write($this->pack($data), function($file)use($url, $path) {
                    \PHPDaemon\Core\Daemon::log('console.info -> ' . $url . '   ' . 'file://' . $path);
                    $file->close();
                });
            });
        }

        public function msg($name) {
            $data = func_get_args();
            unset($data[0]);
            $name = str_replace('\\', '.', $name);
            $file = microtime(true) . '-' . $name . $this->ext();
            $path = $this->dir . $file;
            $url = $this->uri . $file;
            \PHPDaemon\FS\FileSystem::open($path, 'a+!', function (\PHPDaemon\FS\File $file)use($name, $data, $url, $path) {
                $content = $this->pack($data);
                $cb = function($file)use($url, $path, $name) {
                    \PHPDaemon\Core\Daemon::log('console.info -> ' . " name: $name " . $url . '   ' . 'file://' . $path);
                    $file->close();
                };
                setTimeout(function($event)use($content, $file, $cb) {
                    $file->write($content, $cb);
                    $event->finish();
                }, 1e4);
            });
        }

        public function logger($name) {
            foreach ($this->_logger as $obj) {
                if ($this->_logger[$obj] === $name) {
                    return $obj;
                }
            }
            return false;
        }

        public function log($name, $content) {
            if (isset($this->logger[$name])) {
                $path = $this->logger[$name];
            }
            \PHPDaemon\FS\FileSystem::open($path, 'a+', function (\PHPDaemon\FS\File $file)use($content, $name) {
                if ($this->_logger->contains($file) !== false) {
                    $this->_logger->attach($file, $name);
                }
                $file->write($content . "\n");
            });
        }

        public static function console() {
            $result = [];
            $result[] = '#W' . \PHPDaemon\Core\Daemon::$process->getPid();
            foreach (func_get_args() as &$value) {
                switch (gettype($value)) {
                    case 'object':
                        if ($value instanceof \Exception) {
                            $result[] = [
                                'exception' => get_class($value),
                                'code' => $value->getCode(),
                                'msg' => $value->getMessage(),
                                'line' => $value->getLine(),
                                'file' => $value->getFile(),
                                'trace' => $value->getTrace()
                            ];
                        } else {
                            $result[] = ['class' => get_class($value), 'vars' => json_encode($value, JSON_PARTIAL_OUTPUT_ON_ERROR)];
                        }
                        break;
                    case 'array':
                        if (is_object($value)) {
                            $result[] = ['class' => get_class($value), 'vars' => json_encode($value, JSON_PARTIAL_OUTPUT_ON_ERROR)];
                        } else {
                            $result[] = json_encode($value, JSON_PARTIAL_OUTPUT_ON_ERROR);
                        }
                        break;
                    default:
                        $result[] = $value;
                        break;
                }
            }
//        $sizes = array_map('strlen', $result);
//        if (25 < max($sizes)) {
//            $result = implode(' ', $result);
//        }
            \PHPDaemon\Core\Daemon::log($result);
        }

        public function dump() {
            $data = func_get_args();
            \PHPDaemon\FS\FileSystem::tempnam($this->dir, function ($file)use($data) {
                $file->write($data);
                $file->close();
//                \PHPDaemon\Core\Daemon::log('dump -> ' . $url . '   ' . $path);
            });
        }

        public function ext() {
            switch ($this->protocol) {
                case self::json:
                    return '.json';
                case self::html:
                    return '.html';
                case self::xml:
                    return '.xml';
            }
        }

        public function pack($data) {
            switch ($this->protocol) {
                case self::json:
                    return $this->json($data);
                case self::html:
                    return '.html';
                case self::xml:
                    return '.xml';
            }
        }

        public function json($data) {
            if (($result = json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR)) === false) {
                if (json_last_error() === 6) {
                    return json_encode($data, JSON_PARTIAL_OUTPUT_ON_ERROR);
                }
                return json_encode([
                    'result' => false,
                    'error' => [
                        'exception' => 'json_encode',
                        'code' => json_last_error(),
                        'msg' => json_last_error_msg(),
//                        'src' => $this->dump($data)
                    ]
                ]);
            } else {
                return $result;
            }
        }

    }

}
