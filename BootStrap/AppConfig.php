<?php

namespace BootStrap;

use PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug;

class AppConfig {

    use \Traits\getInstance;

    public $apps = [];
    public $onCache = true;
    public $Cache = [];

    public function __construct(array $cfg = null) {
        if ($cfg !== null) {
            $this->config($cfg);
        }
    }

    public function config(array $cfg) {
        if (isset($cfg['apps']) && is_array($cfg['apps'])) {
            $this->apps = $cfg['apps'];
        }
        if (isset($cfg['cache']) && is_bool($cfg['cache'])) {
            $this->onCache = $cfg['cache'];
        }
    }

    /**
     * 
     * @param object|string $section
     * @param string $name
     * @return array
     */
    public static function load($section, $name = null, $cache = null) {
        if ($cache !== null && $cache !== $this->onCache) {
            $self = self::getInstance();
            $self->onCache = $cache;
            $result = $self->get($section, $name);
            $self->onCache = ($cache === false);
            return $result;
        } else {
            return self::getInstance()->get($section, $name);
        }
    }

    /**
     * 
     * @param object|string $section
     * @param string $name
     * @return array
     */
    public function get($section, $name = null) {
        if (is_object($section)) {
            $class = get_class($section);
            if (isset($this->apps[$class])) {
                $app = $this->apps[$class];
                if ($this->onCache) {
                    $id = $app . '.' . $name;
                    if (isset($this->Cache[$id])) {
                        return $this->Cache[$id];
                    }
                }
                if ($name !== 'default' && $name !== '' && $name !== null) {
                    $path = Env::$appConfig . $app . '/' . $name . '.php';
                } else {
                    $path = Env::$appConfig . $app . '.php';
                }
                $cfg = self::file($path, $this->apps[$class]);
                if ($this->onCache) {
                    $this->Cache[$id] = $cfg;
                }
                return $cfg;
            }
        } else {
            if (isset($this->apps[$section])) {
                $app = $this->apps[$section];
                if ($this->onCache) {
                    $id = $app . '.' . $name;
                    if (isset($this->Cache[$id])) {
                        return $this->Cache[$id];
                    }
                }
                if ($name !== 'default' && $name !== '') {
                    $path = Env::$appConfig . $app . '/' . $name . '.php';
                } else {
                    $path = Env::$appConfig . $app . '.php';
                }
                $cfg = self::file($path, $this->apps[$section]);
                if ($this->onCache) {
                    $this->Cache[$id] = $cfg;
                }
                return $cfg;
            } else {
                if ($this->onCache) {
                    $id = $section . ($name !== null) ? '.' . $name : '';
                    if (isset($this->Cache[$id])) {
                        return $this->Cache[$id];
                    }
                }
                if ($name !== 'default' && $name !== '' && $name !== null) {
                    $path = Env::$appConfig . $section . '/' . $name . '.php';
                } else {
                    $path = Env::$appConfig . $section . '.php';
                }
                $cfg = self::file($path, ($name !== null) ? $this->apps[$section] : '');
                if ($this->onCache) {
                    $this->Cache[$id] = $cfg;
                }
                return $cfg;
            }
        }
    }

    /**
     * 
     * @param string $path
     * @param string $name
     * @return boolean|array
     */
    public static function file($path, $name = '') {
        $id = 'W#' . Daemon::$process->getPid();
        if (!file_exists($path)) {
            Daemon::log($id . ' AppConfig->not found file:' . $path . ' name:' . $name);
            return false;
        } elseif (!is_readable($path)) {
            Daemon::log($id . ' AppConfig->not access read is file:' . $path . ' name:' . $name);
            return false;
        } else {
            Daemon::log($id . ' AppConfig->load:' . $path . ' name:' . $name);
        }
        return include $path;
    }

}
