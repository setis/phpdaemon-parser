<?php

namespace BootStrap;

class Env {

    /**
     *
     * @var array
     */
    public static $cfg = [];

    /**
     *
     * @var string dir config
     */
    public static $root;

    /**
     *
     * @var string dir config
     */
    public static $config;

    /**
     *
     * @var string dir events
     */
    public static $events;

    /**
     *
     * @var string dir functions
     */
    public static $functions;

    /**
     *
     * @var string dir functions
     */
    public static $gearman;

    /**
     *
     * @var string dir functions
     */
    public static $appConfig;

    /**
     *
     * @var string dir functions
     */
    public static $ext;

    /**
     *
     * @var \PHPDaemon\Applications\ApiServer\Response
     */
    public static $Response;

    /**
     *
     * @var \PHPDaemon\HTTPRequest\Generic
     */
    public static $Request;
    /**
     *
     * @var \PHPDaemon\Applications\BootStrap\Kernel
     */
    public static $BootStrap;
}
