#!/bin/bash
php="5.5.9";
name="grabber";
package=true;
if($package) then
   dir="$PWD/vendor/kakserpom/phpdaemon";
else
    dir="$PWD/vendor/phpdaemon";
fi;
source $HOME/.phpbrew/bashrc
phpbrew use php-$php
phpbrew switch php-$php
path="$dir/PHPDaemon/Applications"
if [ -d "$path" ]; then
    sudo rm -rf $path;
fi
if [ -L "$path" ]; then
    sudo rm -f $path;
fi
ln -s $PWD $path
file="$PWD/phpd";
if [ -f "$file" ]; then
    rm -f $file
fi
if [ -L "$file" ];then
    rm -f $file;
fi
ln -s $dir/bin/phpd $PWD/phpd
chmod +x $dir/bin/phpd
echo '
#!/bin/bash
php="'$php'";
source '$HOME'/.phpbrew/bashrc
if [ -f "/var/log/phpdaemon-'$name'.log"]; then
  rm -f /var/log/phpdaemon-'$name'.log
fi
clear
php '$dir'/bin/phpd restart
php '$dir'/bin/phpd log' > restart.sh
chmod +x restart.sh
if [ -L "$PWD/conf" ];then
    rm $PWD/conf;
fi
ln -s $dir/conf $PWD/conf;
sudo touch /var/run/phpd-$name.pid
sudo touch /var/log/phpdaemon-$name.log
sudo chmod -R 777 /var/log/phpdaemon-$name.log
file="$dir/conf/conf.d/phpd-$name.conf";
if [ -f "$file" ]; then
    rm -rf $file
fi
ln -s $PWD/phpd-$name.conf $file
file="$dir/conf/phpd.conf";
if [ -L "$file" ]; then
    rm -f $file
fi
ln -s $PWD/phpd.conf $file
file="$dir/conf/AppResolver.php";
if [ -L "$file" ] || [ -f "$file" ]; then
    rm -f $file;
fi
ln -s $PWD/BootStrap/AppResolver.php $file

