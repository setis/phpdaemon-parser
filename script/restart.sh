#!/bin/bash
source cfg.sh
$phpd stop
source clear.sh
if $restart_gearman
then
	if $on_gearman_db
	then
		source gearman_restart.sh
	else
		service gearman-job-server restart
	fi
fi
#service mysql restart
$phpd restart
$phpd log
