#!/bin/bash
php="5.5.9";
sudo apt-get install -y curl
curl -L -O https://raw.github.com/phpbrew/phpbrew/master/phpbrew
chmod +x phpbrew
sudo mv phpbrew /usr/bin/phpbrew
phpbrew init
source ~/.phpbrew/bashrc
phpbrew lookup-prefix homebrew
sudo apt-get install -y zlib1g-dev libncurses5-dev libreadline6 libreadline6-dev gcc make build-essential libxml2-dev libpcre3 libpcre3-dev libmcrypt-dev libcurl4-openssl-dev libpcre++-dev php5 php5-dev php-pear autoconf automake curl build-essential libxslt1-dev re2c libxml2 libxml2-dev php5-cli bison libbz2-dev libreadline-dev libfreetype6 libfreetype6-dev libpng12-0 libpng12-dev libjpeg-dev libjpeg8-dev libjpeg8 libgd-dev libgd3 libxpm4 libssl-dev openssl gettext libgettextpo-dev libgettextpo0 libicu-dev libmhash-dev libmhash2 libmcrypt-dev libmcrypt4 libmysqlclient-dev libmysqld-dev php5-dev libevent-dev libbz2-dev libxslt1-dev
sudo apt-get install -y mariadb-server
rm -rf $HOME/.phpbrew/build/*
phpbrew install $php +xml+pcre+sockets+mhash+json+curl+ipc+posix+dom+ctype+mbstring+mbregex+mcrypt+filter+openssl+pcntl+pdo -- --with-libdir=lib/x86_64-linux-gnu --enable-maintainer-zts --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd
phpbrew use php-$php
phpbrew switch php-$php
phpbrew install-composer
phpbrew install-phpunit
phpbrew ext install gd -- --with-libdir=lib/i386-linux-gnu --with-gd=shared --enable-gd-native-ttf --with-jpeg-dir=/usr --with-png-dir=/usr --with-freetype-dir=/usr/include/freetype2/freetype
sudo apt-get install -y mongodb
phpbrew ext install mongo
sudo pecl install inotify eio redis proctitle-alpha
echo 'extension=inotify.so'>~/.phpbrew/php/php-$php/var/db/inotify.ini
echo 'extension=eio.so'>~/.phpbrew/php/php-$php/var/db/eio.ini
echo 'extension=redis.so'>~/.phpbrew/php/php-$php/var/db/redis.ini
echo 'extension=proctitle.so'>~/.phpbrew/php/php-$php/var/db/proctitle.ini
sudo apt-get install -y gearman-server libgearman-dev libgearman7
pecl install gearman
echo 'extension=gearman.so'>~/.phpbrew/php/php-$php/var/db/gearman.ini
phpbrew ext install gearman
pecl install http://pecl.php.net/get/event-1.6.1.tgz
echo 'extension=event.so'>$HOME/.phpbrew/php/php-$php/var/db/event.ini
echo 'date.timezone = ("Europe/Moscow");'>>$HOME/.phpbrew/php/php-$php/etc/php.ini
curl -sS https://getcomposer.org/installer | php
mv composer.phar $HOME/.phpbrew/php/php-$php/bin/composer
sudo chmod +x composer
$HOME/.phpbrew/php/php-$php/bin/composer selfupdate
cd /tmp
git clone git://github.com/zenovich/runkit.git
cd runkit
pecl install package.xml
echo "extension=runkit.so
runkit.internal_override=1">$HOME/.phpbrew/php/php-$php/var/db/runkit.ini

