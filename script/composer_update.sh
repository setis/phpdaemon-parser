#!/bin/bash
source cfg.sh
cd $dir
composer self-update
if $event; then
    pecl uninstall event
    pecl install http://pecl.php.net/get/event-1.6.0.tgz
fi;
composer update
if $event; then
    pecl uninstall event
    pecl install event
    phpbrew ext install event
fi;