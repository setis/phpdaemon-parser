#!/bin/bash
source cfg.sh
if $event; then
    pecl uninstall event
    pecl install http://pecl.php.net/get/event-1.6.0.tgz
fi;
vendor="$dir/vendor"
if [ -d "$vendor" ]; then
    rm -rf $vendor;
fi
lock="$dir/composer.lock"
if [ -f "$lock" ]; then
    rm -rf $lock;
fi;
composer install
if $event; then
    pecl uninstall event
    pecl install event
    phpbrew ext install event
fi;
source ./init.sh