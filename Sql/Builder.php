<?php

namespace PHPDaemon\Applications\Sql;

use PHPDaemon\Core\Debug,
    PHPDaemon\Core\Daemon,
    Exception;

Class Builder {

    use \Traits\getInstance;

    public $driverSelect = 'mysql';

    /**
     *
     * @var \SQLBuilder\Driver
     */
    public $driver;

    /**
     *
     * @var \SQLBuilder\QueryBuilder
     */
    public $builder;

    const timestamp = 'Y-m-d H:i:s';

    public function __construct() {
        $this->driver = new \SQLBuilder\Driver($this->driverSelect);
        $this->driver->quoter = function($string) {
            return '\'' . str_replace('\'', "\\'", $string) . '\'';
        };
        $this->driver->escaper = function($string) {
            return '\'' . $string . '\'';
        };
        $this->builder = new \SQLBuilder\QueryBuilder($this->driver);
    }

    /**
     * 
     * @return \SQLBuilder\QueryBuilder
     */
    public function SQLBuilder() {
        return clone $this->builder;
    }

    /**
     * 
     * @return \SQLBuilder\QueryBuilder
     */
    public static function Sql() {
        return clone self::getInstance()->builder;
    }

    /**
     * 
     * @param int $time
     * @return string
     */
    public static function timestamp($time = null) {
        if ($time === null) {
            $time = time();
        }
        return date(self::timestamp, $time);
    }

    /**
     * 
     * @return string
     */
    public static function last_insert_id() {
        return 'SELECT LAST_INSERT_ID() AS result;';
    }

    public static function joinSql() {
        if (func_num_args() === 0) {
            return '';
        }
        return implode(' ', func_get_args());
    }

    public static function QueryTransAction(\PHPDaemon\Clients\MySQL\Connection $Connection = null, $sql, $onSucces = null, $onError = null) {
        if ($Connection === null) {
            $Connection = self::Connection();
        }
        $sql = TransAction::query($sql);
        $Error = function($e)use($onError, $Connection) {
            return TransAction::onHandler($e, $onError, $Connection);
        };
        $Connection->getConnection(function ($query) use ($sql, $onSuccess, $Error) {
            if ($query->isConnected()) {
                $query->query($sql, function($query, $t)use($onSuccess, $Error) {
                    if ($query->errno > 0) {
                        $e = new Exception($query->errmsg, $query->errno);
                        if ($Error) {
                            call_user_func($Error, $e);
                        } else {
                            throw $e;
                        }
                    } else {
                        if ($onSuccess) {
                            call_user_func($onSuccess);
                        }
                    }
                });
            } else {
                $e = new Exception($query->errmsg, $query->errno);
                if ($Error) {
                    call_user_func($Error, $e);
                } else {
                    throw $e;
                }
            }
        });
    }

    /**
     * 
     * @param \PHPDaemon\Applications\Sql\PHPDaemon\Clients\MySQL\Connection $connect
     * @param \Closure $callback
     */
    public static function lastInsertId($connect, \Closure $callback) {
        call_user_func($callback, $connect->insertId);
    }

    /**
     * 
     * 
     * @param string $sql
     * @param callable $onSuccess
     * @param callable $onError
     * @param \PHPDaemon\Clients\MySQL\Pool $Connection
     */
    public static function Query($sql, $onSuccess = null, $onError = null, $Connection = null) {
        if ($Connection === null) {
            $Connection = self::Connection();
        }
        $Connection->getConnection(function ($query) use ($sql, $onSuccess, $onError) {
            if ($query->isConnected()) {
                $query->query($sql, function($query, $t)use($onSuccess, $onError, $sql) {
//                    console(__METHOD__ . ':' . __LINE__, $sql, $t, "code:{$query->errno} msg:{$query->errmsg}");
                    if ($query->errno > 0) {
                        $e = new Exception($query->errmsg . ' ' . $sql, $query->errno);
                        if ($onError) {
                            call_user_func($onError, $e, $query);
                        } else {
                            throw $e;
                        }
                    } else {
                        if ($onSuccess) {
                            call_user_func($onSuccess, $query);
                        }
                    }
                });
            } else {
                $e = new Exception($query->errmsg, $query->errno);
                if ($onError) {
                    call_user_func($onError, $e, $query);
                } else {
                    throw $e;
                }
            }
        });
    }

    /**
     * 
     * 
     * @param string $sql
     * @param callable $onSuccess
     * @param callable $onError
     * @param \PHPDaemon\Clients\MySQL\Pool $Connection
     */
    public static function QueryMulty($sql, $onSuccess = null, $onError = null, $Connection = null) {
        if ($Connection === null) {
            $Connection = self::Connection();
        }
        $Connection->getConnection(function ($query) use ($sql, $onSuccess, $onError) {
            if ($query->isConnected()) {
                if (is_array($sql)) {
                    $sqls = $sql;
                    $count = count($sql) - 1;
                } else {
                    $sqls = explode(';', $sql);
                    $count = count($sqls) - 2;
                }
                console(__METHOD__, $sqls);
                $error = false;
                foreach ($sqls as $i => $sql) {
                    if ($error === true) {
                        return;
                    }
                    console(__METHOD__ . ':' . __LINE__, $i, $count);
                    if ($count >= $i) {

                        $query->query($sql, function($query, $t)use($onSuccess, $onError, $sql, $i, $count, &$error) {
                            if ($query->errno > 0) {
                                console(__METHOD__ . ':' . __LINE__, $sql, $i);
                                $e = new Exception($query->errmsg, $query->errno);
                                $error = true;
                                if ($onError) {
                                    call_user_func($onError, $e, $query->errmsg, $query->errno, $sql, $query);
                                } else {
                                    throw $e;
                                }
                            } else {
                                if ($onSuccess && $i === $count) {
                                    call_user_func($onSuccess, $query);
                                }
                            }
                        });
                    }
                }
            } else {
                $e = new Exception($query->errmsg, $query->errno);
                if ($onError) {
                    call_user_func($onError, $e, $query);
                } else {
                    throw $e;
                }
            }
        });
    }

    /**
     * 
     * @param string $sql
     * @param callable $onSuccess
     * @param callable $onError
     * @param callable|string $onResult
     * @param \PHPDaemon\Clients\MySQL\Pool $Connection
     */
    public static function Fetch($sql, $onSuccess = null, $onError = null, $onResult = null, \PHPDaemon\Clients\MySQL\Pool $Connection = null) {
        if ($Connection === null) {
            $Connection = self::Connection();
        }
        $Connection->getConnection(function ($query) use ($sql, $onSuccess, $onError, $onResult) {
            if ($query->isConnected()) {
                $query->query($sql, function($query, $t)use($onSuccess, $onError, $onResult) {
                    if ($query->errno > 0) {
                        $e = new Exception($query->errmsg, $query->errno);
                        if ($onError) {
                            call_user_func($onError, $e, $query);
                        } else {
                            throw $e;
                        }
                    } else {
                        $result = $query->resultRows;
                        if ($onResult) {
                            if (is_numeric($onResult)) {
                                $result = $result[$onResult];
                            } elseif (is_string($onResult)) {
                                $path = explode('.', $onResult);
                                foreach ($path as $key) {
                                    $result = $result[$key];
                                }
                            } elseif (is_int($onResult)) {
                                $result = $result[$onResult];
                            } elseif (is_callable($onResult)) {
                                return call_user_func_array($onResult, [$onSuccess, $query->resultRows, $query]);
                            }
                        }
                        if ($onSuccess) {
                            call_user_func($onSuccess, $result, $query);
                        }
                    }
                });
            } else {
                $e = new Exception($query->errmsg, $query->errno);
                if ($onError) {
                    call_user_func($onError, $e, $query);
                } else {
                    throw $e;
                }
            }
        });
    }

    /**
     * 
     * @param \PHPDaemon\Clients\MySQL\Pool $Connection
     * @param string $sql
     * @param callable $onSucces
     * @param callable $onError
     * @param callable|string $onResult
     * @return type
     */
    public static function FetchTransAction(\PHPDaemon\Clients\MySQL\Pool &$Connection = null, $sql, $onSuccess = null, $onError = null, $onResult = null) {
        if ($Connection === null) {
            $Connection = self::Connection();
        }
        $sql = TransAction::query($sql);
        $Error = function($e)use($onError, $Connection) {
            return TransAction::onHandler($e, $onError, $Connection);
        };
        $Connection->getConnection(function ($query) use ($sql, $onSuccess, $Error, $onResult) {
            if ($query->isConnected()) {
                $query->query($sql, function($query, $t)use($onSuccess, $Error, $onResult) {
//                    \PHPDaemon\Core\Daemon::log('fetch :'.\PHPDaemon\Core\Debug::dump($t));
                    if ($query->errno > 0) {
                        $e = new Exception($query->errmsg, $query->errno);
                        if ($Error) {
                            call_user_func($Error, $e);
                        } else {
                            throw $e;
                        }
                    } else {
                        $result = $query->resultRows;
                        if ($onResult) {
                            if (is_string($onResult)) {
                                foreach (explode('.', $onResult) as $key) {
                                    $result = $result[$key];
                                }
                            } else {
                                return call_user_func_array($onResult, [$onSuccess, $query->resultRows]);
                            }
                        }
                        if ($onSuccess) {
                            call_user_func($onSuccess, $result);
                        }
                    }
                });
            } else {
                $e = new Exception($query->errmsg, $query->errno);
                if ($Error) {
                    call_user_func($Error, $e);
                } else {
                    throw $e;
                }
            }
        });
    }

    /**
     * 
     * @return \PHPDaemon\Clients\MySQL\Pool
     */
    public static function Connection() {
        return \PHPDaemon\Clients\MySQL\Pool::getInstance();
    }

    /**
     * 
     * @param string $table
     * @param string $prefix
     * @return string
     */
    public static function table($table, $prefix = null, $plode = '_') {
        if ($prefix === null) {
            return $table;
        }
        return $prefix . $plode . $table;
    }

}
