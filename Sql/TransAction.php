<?php

namespace PHPDaemon\Applications\Sql;

use PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug;

class TransAction {

    public static function start() {
        return 'START TRANSACTION;';
    }

    public static function rollback() {
        return 'ROLLBACK;';
    }

    public static function commit() {
        return 'COMMIT;';
    }

    public static function query($sql) {
        return self::start() . $sql . self::commit();
    }

    public static function onHandler(Exception $e, $cb = null, \PHPDaemon\Clients\MySQL\Connection $Connection = null) {
        Daemon::log('code: ' . $e->getCode() . ' msg: ' . $e->getMessage() . ' trace: ' . $e->getTraceAsString());
        Builder::Query($Connection, self::rollback());
        if ($cb) {
            call_user_func($cb, $e->getCode());
        }
    }

}
