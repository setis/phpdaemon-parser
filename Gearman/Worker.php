<?php

namespace PHPDaemon\Applications\Gearman;

use PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    PHPDaemon\Core\AppInstance;

class Worker {

    use \Traits\getInstance;

    /**
     *
     * @var \GearmanWorker
     */
    public $Worker;

    /**
     *
     * @var int
     */
    public $timer;

    /**
     *
     * @var int|float
     */
    public $timeout;

    /**
     *
     * @var boolean
     */
    public $wait = false;

    /**
     *
     * @var \ArrayObject
     */
    protected $config;

    protected function __construct() {
        if (!class_exists('GearmanWorker', true)) {
            Daemon::log('W#' . Daemon::$process->getPid() . ' ERROR: not found class GearmanWorker (pecl install gearman or phpbrew ext install gearman)');
            $this->config->enable->value = 0;
            return;
        } else {
            $this->config = new \ArrayObject();
            $this->config->setFlags(\ArrayObject::ARRAY_AS_PROPS);
            $this->config->exchangeArray(\BootStrap\AppConfig::load($this));
            $this->Worker = new \GearmanWorker();
            $this->setOptions($this->config->server);
            $this->register($this->config->worker);
            $this->timeout = &$this->config->timeout;
            if ($this->timer === null) {
                $this->timer = setTimeout([&$this, 'run'], $this->timeout);
            }
        }
    }

    public function register(array $functions) {
        foreach ($functions as $name => $values) {
            if (is_array($values)) {
                list($callable, $timeout) = $values;
                $this->Worker->addFunction($name, $callable, null, $timeout);
            } else {
                $this->Worker->addFunction($name, $values);
            }
            Daemon::log('W#' . Daemon::$process->getPid() . ' Gearman.Worker->register: ' . $name);
        }
    }

    public function unregister($functions) {
        if (is_array($functions)) {
            foreach ($functions as $name) {
                $this->Worker->unregister($name);
                Daemon::log('Gearman.Worker->unregister: ' . $name);
            }
        } else {
            $this->Worker->unregister($functions);
            Daemon::log('Gearman.Worker->unregister: ' . $functions);
        }
    }

    public function setOptions(array $cfg) {
        if (isset($cfg['server']) && (is_array($cfg['server']) && count($cfg['server']) > 0)) {
            $this->Worker->addServers(implode(',', $cfg['server']));
        } else {
            $this->Worker->addServer();
        }
        if (isset($cfg['options']) && is_array($cfg['options']) && count($cfg['options']) > 0) {
            foreach ($cfg['options'] as $options) {
                $this->Worker->addOptions($options);
            }
        }
//        $timeout = (isset($cfg['timeout']) && (is_int($cfg['timeout']) || is_numeric($cfg['timeout']))) ? $cfg['timeout'] : 0;
//        $this->Worker->setTimeout($timeout);
        if (isset($cfg['id']) && !empty($cfg['id'])) {
            $this->Worker->setId($cfg['id']);
        }
    }

    public function getOptions() {
        return [
            'options' => $this->Worker->options(),
            'timeout' => $this->Worker->timeout()
        ];
    }

    public function addFunction($name, $func, $timeout = null) {
        $this->config->worker[$name] = ($timeout !== null) ? [$func, $timeout] : $func;
        $this->Worker->addFunction($name, $func, null, $timeout);
        Daemon::log('W#' . Daemon::$process->getPid() . ' Gearman.Worker->register:' . "$name  ");
    }

    public function removeFunction($name) {
        unset($this->config->worker[$name]);
        $this->Worker->unregister($name);
        Daemon::log('W#' . Daemon::$process->getPid() . ' Gearman.Worker->unregister:' . $name);
    }

    public function run($event) {
//        if ($this->wait === true) {
//            return;
//        }
        $id = 'W#' . Daemon::$process->getPid();
        $this->Worker->work();
        $code = $this->Worker->returnCode();
        switch ($code) {
            case GEARMAN_IO_WAIT:
                $this->Worker->wait();
                break;
            case GEARMAN_NO_JOBS:
                $event->timeout(1e6);
//                Daemon::log('gearman.worker->GEARMAN_NO_JOBS');
                return;
            case GEARMAN_NO_REGISTERED_FUNCTIONS:
                $event->timeout(1e6);
                Daemon::log('gearman.worker->GEARMAN_NO_REGISTERED_FUNCTIONS');
                return;
//            case GEARMAN_TIMEOUT:
//                throw new \Exception('not gearman client', $code);
//                return;
            case GEARMAN_SUCCESS:
                $event->timeout($this->timeout);
//                Daemon::log('gearman.worker->GEARMAN_SUCCESS');
                return;
            default:
                Daemon::log($id . ' Gearman.Worker->returnCode:' . $code);
                break;
        }
        if ($this->Worker->getErrno()) {
            Daemon::log($id . ' Gearman.Worker->error:' . $this->Worker->error() . ' code:' . $this->Worker->getErrno());
        }
        $event->timeout($this->timeout);
    }

    public function onReady() {
        if ($this->isEnabled()) {
            $this->setOptions($this->config->server);
            $this->register($this->config->worker);
            if ($this->timer === null) {
                $this->timer = setTimeout([&$this, 'run'], $this->timeout);
            }
        }
    }

    public function __destruct() {
        if ($this->timer) {
            Timer::clearTimeout($this->time);
            $this->timer = null;
        }
        return true;
    }

}
