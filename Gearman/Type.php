<?php

namespace PHPDaemon\Applications\Gearman;

interface Type {

    const
            Async = 0,
            Sync = 1,
            Async_Background = 3,
            Sync_Background = 4;

}
