<?php

namespace PHPDaemon\Applications\Gearman;

use PHPDaemon\Core\AppInstance,
    PHPDaemon\Core\Debug,
    PHPDaemon\Core\Daemon;

class Client extends AppInstance implements Type, Priority {

    /**
     *
     * @var int
     */
    public $timer;

    /**
     *
     * @var \GearmanClient|\SplObjectStorage
     */
    public $Pool;

    /**
     *
     * @var \GearmanClient|\SplObjectStorage
     */
    public $PoolWork;

    /**
     *
     * @var string
     */
    public $cfg;

    public function setOptions(\GearmanClient &$client, array $cfg) {
        if (isset($cfg['server']) && (is_array($cfg['server']) && count($cfg['server']) > 0)) {
            $client->addServers(implode(',', $cfg['server']));
        } else {
            if (empty($cfg['server'])) {
                $client->addServers();
            } else {
                $client->addServers($cfg['server']);
            }
        }
        if (isset($cfg['options']) && is_array($cfg['options']) && count($cfg['options']) > 0) {
            foreach ($cfg['options'] as $options) {
                $client->addOptions($options);
            }
        }
        $timeout = (isset($cfg['timeout']) && (is_int($cfg['timeout']) || is_numeric($cfg['timeout']))) ? $cfg['timeout'] : 0;
        $client->setTimeout($timeout);
    }

    public function getOptions(\GearmanClient &$client) {
        return [
            'timeout' => $client->timeout(),
        ];
    }

    public static $methods;

    public static function getMethods() {
        if (self::$methods === null) {
            self::$methods = get_class_methods('GearmanClient');
        }
        return self::$methods;
    }

    public function setCallback(\GearmanClient &$client, array $cfg) {
        $methods = self::getMethods();
        foreach ($cfg as $name => $callback) {
            $func = 'set' . ucfirst($name) . 'Callback';
            if (in_array($func, $methods)) {
                call_user_func([$client, $func], $callback);
            } else {
                Daemon::log(__METHOD__ . ' not func: ' . $func);
            }
        }
    }

    public function init() {
        $this->Pool = new \SplObjectStorage();
        $this->cfg = \BootStrap\AppConfig::load($this, $this->name);
    }

    protected function getConfigDefaults() {
        return [
            'enable' => 1,
        ];
    }

    public function onShutdown($graceful = false) {
        if ($this->timer) {
            Timer::clearTimeout($this->timer);
            $this->timer = null;
        }
    }

    /**
     * 
     * @return \GearmanClient
     */
    public function &getClient($callback = null, $safe = true) {
        if (($client = $this->client()) === false) {
            $client = $this->clientNew($callback);
        } else {
            $callback($client);
        }
        if ($safe) {
            $this->Pool->attach($client);
        }
        return $client;
    }

    /**
     * 
     * @return \GearmanClient|false
     */
    public function client() {
        if ($this->Pool->count() === 0) {
            return false;
        }
        $this->Pool->rewind();
        for ($i = 0; $i < $this->Pool->count(); $i++) {
            /** @var $client \GearmanClient */
            $client = $this->Pool->current();
            $code = $client->returnCode();
            if ($code === GEARMAN_NO_JOBS) {
                return $client;
            }
            $this->Pool->next();
        }
        return false;
    }

    /**
     * 
     * @return \GearmanClient
     */
    public function clientNew($callback) {
        $cfg = $this->cfg['server'];
        $client = new \GearmanClient();
        $client->addServers($cfg['server']);
        if (isset($cfg['options'])) {
            $client->addOptions($cfg['options']);
        }
        if (isset($cfg['timeout'])) {
            $client->setTimeout($cfg['timeout']);
        }
        call_user_func($callback, $client);
        return $client;
    }

    public function onReady() {
        $this->timer = setTimeout(function($timer) {
            $this->runTasks();
            $timer->timeout();
        }, $this->cfg['timeout']);
    }

    public function runTasks() {
        if ($this->Pool->count() === 0) {
            return;
        }
        $this->Pool->rewind();
        $count = $this->Pool->count();
        for ($i = 0; $i < $count; $i++) {
            /** @var $client \GearmanClient */
            $client = $this->Pool->current();
            $code = $client->returnCode();
            $f = true;
            switch ($code) {
                case GEARMAN_SUCCESS:
                case GEARMAN_NO_JOBS:
                    if ($count > 15) {
                        $this->Pool->detach($client);
                        $client = null;
                        $f = false;
                    }
                    break;
                case GEARMAN_IO_WAIT:
                    $client->wait();
                    break;
                case GEARMAN_TIMEOUT:
                    $f = false;
                    break;
                case GEARMAN_COULD_NOT_CONNECT:
                    $f = false;
                    break;
                default:
                    $this->Pool[$client] = $code;
                    Daemon::log('returnCode:' . $code);
                    break;
                case GEARMAN_NO_SERVERS:
                    console(new \Exception('gearman GEARMAN_NO_SERVERS', GEARMAN_NO_SERVERS));
                    $this->Pool->detach($client);
                    $f = false;
                    break;
            }
            if ($f && !@$client->runTasks()) {
                $errno = $client->getErrno();
                if ($errno) {
                    Daemon::log('runTasks error:' . $client->error() . " code: $errno");
                    info(new \Exception($client->error(), $errno));
                }
                switch ($errno) {
                    case GEARMAN_NO_ACTIVE_FDS:
                        $client->echo('ping');
                        break;
                }
            }
            $this->Pool->next();
        }
    }

    /**
     * 
     * @param string $job
     * @param mixed $workload
     * @param Type $type
     * @param Priority $priority
     * @param \Gearman $client
     * @return \GearmanTask
     */
    public function queue($job, $workload, $type = self::Async, $priority = self::Low, \Closure $callback = null, \Gearman $client = null, array $callbacks = null) {
        if ($callbacks !== null) {
            $this->setCallback($callbacks, $client);
        }
        if (!is_string($workload)) {
            $workload = json_encode($workload);
        }
        $func = '';
        $funcEnd = '';
        switch ($type) {
            case Type::Sync:
                $func.='do';
                break;
            case Type::Async:
                $func.='addTask';
                break;
            case Type::Sync_Background:
                $func.='do';
                $funcEnd = 'Background';
                break;
            case Type::Async_Background:
                $func.='addTask';
                $funcEnd = 'Background';
                break;
        }
        switch ($priority) {
            case Priority::Low:
                $func.='Low';
                break;
            case Priority::Normal:
                if ($type === Type::Sync) {
                    $func.='Normal';
                }
                break;
            case Priority::High:
                $func.='High';
                break;
        }
        $func.=$funcEnd;
        $safe = ($type !== Type::Async || $type !== Type::Async_Background);
        if ($client === null) {
            $this->getClient(function(\GearmanClient $client)use($func, $job, $workload, $callback) {
                if ($callback === null) {
                    call_user_func([&$client, $func], $job, $workload);
                } else {
                    call_user_func($callback, call_user_func([&$client, $func], $job, $workload));
                }
            }, $safe);
        } else {
            call_user_func([&$client, $func], $job, $workload);
        }
    }

    /**
     * 
     * @param \GearmanTask $task
     * @param array|callable $callbacks
     */
    public function onHandlers() {
        $this->setCallback($this->config->handlers, $this->Client);
    }

    public function offHandlers() {
        $this->Client->clearCallbacks();
    }

}
