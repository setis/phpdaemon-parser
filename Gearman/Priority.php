<?php

namespace PHPDaemon\Applications\Gearman;

interface Priority {

    const
            Low = 1,
            Normal = 2,
            High = 3;

}
