<?php

namespace Test;

use Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent,
    GuzzleHttp\Event\CompleteEvent,
    GuzzleHttp\Event\ErrorEvent,
    Grabber\Consts\Http;

class Api {

    public static function response() {
        response(true);
    }

    public static function phpinfo() {
        echo phpinfo();
    }

    public static function queueCall() {
        \Grabber\Functions\queueCall([__CLASS__, 'jobCall']);
        response(true);
    }

    public static function jobCall(\GearmanJob $job) {
        $job->sendStatus(1, 1);
        console(__METHOD__, $job->handle(), $job->returnCode());
        $job = null;
    }

    public static function vk() {
        \ApiClient\Kernel::getInstance()->api('vk', 'users.get', [
            'fields' => 'sex, bdate, city, country, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, photo_id, online, online_mobile, domain, has_mobile, contacts, connections, site, education, universities, schools, can_post, can_see_all_posts, can_see_audio, can_write_private_message, status, last_seen',
            'user_ids' => 11817975
                ], [], function($data) {
            response($data);
        }, function() {
            response(func_get_args());
        });
    }

    public static function vk_createAlbum() {
        $title = 'title test';
        $description = 'description test';
        $config = new \ApiClient\Config([
            'name' => 'vk',
            'access' => '1e1b8ad517dae6615aab9a04a18ecaffade9e2b106b2c362242d7f11786c44f9452bf79d89508da49b1d0',
            'group_id' => 59076975,
            'secret' => '50026e1f3d477735ac',
            'app_id' => 4704340,
            'scopes' => [
                'offline',
                'nohttps',
                'stats',
                'notify',
                'friends',
                'photos',
                'docs',
                'notes',
                'status',
                'groups'
            ]
        ]);
        \ApiClient\Kernel::getInstance()->service('vk')->api($config, 'photos.createAlbum', [
            'title' => $title,
            'group_id' => $config->group_id,
            'description' => $description,
            'privacy' => 0,
            'comment_privacy' => 0,
            'upload_by_admins_only' => 1,
            'comments_disabled' => 0,
            'access_token' => $config->access
                ], [], function()use($config) {
            response(func_get_args());
            \Grabber\Model\album_relations_access::getInstance()->insert([
                'service' => $config->name,
                'album' => '',
                'access' => '',
                'id' => $config->name
            ]);
        }, function(\ApiClient\Service\Vk\Exception $error, $event) {
            response($error->getContent());
//            console(__FILE__ . ':' . __FILE__, $error);
        });
    }

    public static function mongo() {
        $collector = new \Grabber\Collector();
        $collector->name();
        response($collector);
    }

    public static function is_event($name) {
        $events = \BootStrap\Events::getInstance();
        response($events->EventDispatcher->hasListeners($name));
    }

    public static function event($name) {
        $events = \BootStrap\Events::getInstance();
        $events->EventDispatcher->dispatch($name);
        response($events->EventDispatcher->hasListeners($name));
    }

    public static function lists() {
        events()->EventDispatcher->dispatch('readmanga.action.list');
        response(true);
    }

    public static function naruto() {
        events()->EventDispatcher->dispatch('readmanga.content');
        response(true);
    }

    public static function guzzle() {
        $data = [
            'method' => 'get',
            'url' => '/the_breaker__new_waves',
            'options' => [
                'cookies' => true,
                'base_url' => 'http://readmanga.me',
                'future' => true,
                'defaults' => [
                    'debug' => false
                ]
            ]
        ];
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient($data['options']);
        unset($data['options']['base_url']);
        unset($data['options']['defaults']);
        $requests[] = $client->createRequest($data['method'], $data['url'], $data['options']);
        $events['complete'] = function(CompleteEvent $e) {
            $body = $e->getResponse()->getBody();
            $html = '';
            while (!$body->eof()) {
                $html.=$body->read(1024);
            }
            response(\Grabber\Parse\readmanga::content($html));
            return true;
        };
        $events['error'] = function(ErrorEvent $e) {
            response(dumpException($e));
            finish();
            return true;
        };
        wakeup();
        \GuzzleHttp\Pool::send($client, $requests, $events);
    }

    public static function content() {
        events()->EventDispatcher->dispatch('grabber.queue', new GenericEvent(null, [
            'http.cfg' => [
                'method' => 'get',
                'url' => '/the_breaker__new_waves',
                'events' => [
                    Http::Complete => [
                        'readmanga.content',
                        'grabber.db.short',
                        'grabber.db.long',
                        'grabber.date.year',
                        'grabber.relations',
                        'grabber.status',
                        'grabber.info',
                        'grabber.complete'
                    ],
                    Http::Error => [
                        'grabber.error',
                    ]
                ],
                'options' => [
                    'base_url' => 'http://readmanga.me',
                    'future' => true,
                    'defaults' => [
                        'debug' => true
                    ]
                ],
            ],
            'params' => [
                'lang' => 'ru',
                'source' => 'readmanga'
            ]
        ]));
        response(true);
    }

    public static function download() {
        events()->EventDispatcher->dispatch('grabber.queue', new GenericEvent(null, [
            'http.cfg' => [
                'method' => 'get',
                'url' => [
                    "http://d.readmanga.ru/uploads/pics/00/05/317_o.png",
                    "http://d.readmanga.ru/uploads/pics/00/08/030_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/12/493_o.png",
                    "http://d.readmanga.ru/uploads/pics/00/12/494_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/12/497_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/12/498_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/12/499_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/13/234_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/13/236_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/20/352_o.png",
                    "http://d.readmanga.ru/uploads/pics/00/20/351_o.png",
                    "http://d.readmanga.ru/uploads/pics/00/22/951_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/22/953_o.png",
                    "http://d.readmanga.ru/uploads/pics/00/29/544_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/29/547_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/29/548_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/31/138_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/35/941_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/246_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/247_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/248_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/249_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/250_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/251_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/253_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/254_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/255_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/256_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/257_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/258_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/259_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/260_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/261_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/262_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/263_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/42/264_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/533_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/534_o.png",
                    "http://d.readmanga.ru/uploads/pics/00/43/535_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/536_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/537_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/538_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/539_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/540_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/43/543_o.png",
                    "http://d.readmanga.ru/uploads/pics/00/43/544_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/45/055_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/45/057_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/45/058_o.jpg",
                    "http://d.readmanga.ru/uploads/pics/00/45/059_o.jpg"
                ],
                'events' => [
                    Http::Complete => [
                        'grabber.info',
//                        'grabber.complete'
                    ],
                    Http::Error => [
                        'grabber.error',
                    ]
                ],
                'options' => [
                    'base_url' => 'http://readmanga.me',
                    'future' => true,
                    'defaults' => [
                        'debug' => false
                    ]
                ],
            ],
            'params' => [
                'lang' => 'ru',
                'source' => 'readmanga',
                'download' => [
                    'dir' => '/var/www/phpdaemon',
                    'file' => 'original'
                ]
            ],
            'mode' => 'download',
        ]));
        response(true);
    }

}
