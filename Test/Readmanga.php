<?php

namespace Test;

use Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\GenericEvent,
    GuzzleHttp\Event\CompleteEvent,
    GuzzleHttp\Event\ErrorEvent,
    Grabber\Consts\Http;

//use function \Grabber\Functions\queueCall;
class Readmanga {

    public static function event_content1() {
        \Grabber\Functions\queue([

            'http.cfg' => [
                'method' => 'get',
                'url' => 'http://readmanga.me/ability',
                'events' => [
                    Http::Complete => [
                        'readmanga.content',
                        'grabber.db.short',
                        'grabber.db.long',
                        'grabber.date.year',
                        'grabber.relations',
                        'grabber.status',
                        'readmanga.image',
                        'grabber.complete',
                    ],
                    Http::Error => [
                        'grabber.error',
                    ]
                ],
                'options' => [
                    'base_url' => 'http://readmanga.me',
                    'future' => true,
                    'defaults' => [
                        'debug' => false
                    ]
                ]
            ],
            'params' => [
                'lang' => 'ru',
                'source' => 'readmanga'
            ]
        ]);
        response(true);
    }

    public static function event_content() {
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        $request = $client->createRequest('GET', 'http://readmanga.me/one__piece');
        $request->getEmitter()->on('complete', function(CompleteEvent &$e) {
            $reponse = $e->getResponse();
            $content = $reponse->getBody()->getContents();
            $url = $reponse->getEffectiveUrl();
            \Grabber\Events\readmanga::content($content, $url);
            wakeup();
            echo $content;
            finish();
        });
        $client->send($request);
    }

    public static function event_lists1() {
        \Grabber\Events\readmanga::action_list();
        response(true);
    }

    public static function event_lists() {
        \Grabber\Queue\readmanga::queue_lists();
        response(true);
    }

    public static function queue_content() {
        \Grabber\Queue\readmanga::queue_content('http://readmanga.me/the_breaker__new_waves');
        response(true);
    }

    public static function content() {
        $url = 'http://readmanga.me/beelzebub';
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        $request = $client->createRequest('GET', $url);
        $request->getEmitter()->on('complete', function(CompleteEvent $e) {
            $reponse = $e->getResponse();
            $content = $reponse->getBody()->getContents();
            $url = $reponse->getEffectiveUrl();
            wakeup();
            echo $content;
            finish();
            \Grabber\Queue\readmanga::job_content($content, $url);
        });
        $client->send($request);
    }

    public static function chapter1() {
        $result['chapters'] = [
            [
                'name' => null,
                'volume' => 5,
                'chapter' => 36,
                'link' => "/the_breaker__new_waves/vol5/36?mature=1"
            ],
            [
                'name' => "Новый Кумурен. Ученик заменяет учителя.",
                'volume' => 5,
                'chapter' => 35,
                'link' => "/the_breaker__new_waves/vol5/35?mature=1"
            ]
        ];
        $id = 5;
        $source = 1;
        $lang = 1;
        \Grabber\Functions\queueCall(['Grabber\Queue\readmanga', 'chapter'], [$result, $id, $source, $lang]);
        response(true);
    }

    public static function chapter() {
        $result = [
            [
                'name' => null,
                'volume' => 5,
                'chapter' => 36,
                'link' => "/the_breaker__new_waves/vol5/36?mature=1"
            ],
            [
                'name' => "Новый Кумурен. Ученик заменяет учителя.",
                'volume' => 5,
                'chapter' => 35,
                'link' => "/the_breaker__new_waves/vol5/35?mature=1"
            ]
        ];
        $gc = new \Grabber\Index\Collector\One();
        foreach ($result as $value) {
            $chapter = new \Grabber\Index\Data\Chapter();
            $chapter->name = $value['name'];
            $chapter->chapter = $value['chapter'];
            $gc->add($chapter);
        }

        $gc->overall(function($storage, $self) {
            $i = 0;
            foreach ($storage as $object) {
                $result[] = $object->toArray();
            }
            response($result);
        });
    }

    public static function lists() {
        events()->EventDispatcher->dispatch('readmanga.action.list');
        response(true);
    }

    public static function hash() {
        $value = 'Ability';
        $hash = md5($value);
        \PHPDaemon\Core\Daemon::log(__METHOD__, hex2bin($hash));
        out(hex2bin($hash));
        finish();
    }

    public static function data() {
        $value = 'Ability';
        $lang = \Grabber\Index\Storage::lang('en');
        $name = new \Grabber\Index\Data\Short(['lang' => $lang, 'value' => $value]);
        console(__METHOD__ . ':' . __LINE__, $name->locked());
        $collection = new \Grabber\Index\Collection();
        $collection->add($name)->each(function(\Grabber\Index\Data\Short $result) {
            $arr = (array) $result->toArray();
            \PHPDaemon\Core\Daemon::log(__METHOD__, $arr);
            response(true);
        });
    }

    public static function parse_list() {
        $data = [
            'method' => 'get',
            'url' => '/list?type=&sortType=rate&max=100000000',
            'options' => [
                'base_url' => 'http://readmanga.me',
                'future' => true,
                'defaults' => [
                    'debug' => false
                ]
            ]
        ];
        $events['complete'] = function(CompleteEvent $e) {
            $html = $e->getResponse()->getBody()->getContents();
            $result = \Grabber\Parse\readmanga::content($html);
            \console\msg(__METHOD__, $result);
            response($html, null, false);
        };
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient($data['options']);
        unset($data['options']['base_url']);
        unset($data['options']['defaults']);
        $requests[] = $client->createRequest($data['method'], $data['url'], $data['options']);
        \GuzzleHttp\Pool::send($client, $requests, $events);
    }

    public static function parse_content() {
        $data = [
            'method' => 'get',
            'url' => 'http://readmanga.me/ability',
            'options' => [
                'base_url' => 'http://readmanga.me',
                'future' => true,
                'defaults' => [
                    'debug' => false
                ]
            ]
        ];
        $events['complete'] = function(CompleteEvent $e) {
            $html = $e->getResponse()->getBody()->getContents();
            $result = \Grabber\Parse\readmanga::content($html);
            \console\msg(__METHOD__, $result);
            response($html, null, false);
        };
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient($data['options']);
        unset($data['options']['base_url']);
        unset($data['options']['defaults']);
        $requests[] = $client->createRequest($data['method'], $data['url'], $data['options']);
        \GuzzleHttp\Pool::send($client, $requests, $events);
    }

    public function info() {
        response([
            'class' => __CLASS__,
            'methods' => get_class_methods(__CLASS__)
        ]);
    }

}
