<?php

namespace Test;

class data {

    public function locked() {
        return true;
    }

}

class Index {

    public static function one_limit_add() {
        $gc = new \Grabber\Index\Collector\One();
        $gc->add(new data());
        response(true);
    }
    public static function one_limit_reload() {
        $gc = new \Grabber\Index\Collector\One();
        $gc->add(new data());
        setTimeout(function($event)use(&$gc){
            $gc->add(new data());
            $event->finish();
        },3e6);
        response(true);
    }
    public static function one_limit_overall() {
        $gc = new \Grabber\Index\Collector\One();
        $gc->add(new data());
        $gc->overall(function(){});
        response(true);
    }

}
