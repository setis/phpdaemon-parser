<?php

namespace Traits;

trait getInstance {

    /**
     *
     * @var $this|self|static
     */
    public static $instance;

    /**
     * 
     * @return this|self|static
     */
    public static function &getInstance() {
        if (self::$instance === null) {
            self::$instance = new self;
            if (method_exists(self::$instance, '__construct')) {
                call_user_func_array([self::$instance, '__construct'], func_get_args());
            }
        }
        return self::$instance;
    }

    public static function clearInstance() {
        self::$instance = null;
    }

    public static function setInstance($obj) {
        if(!$obj instanceof self){
            throw new Exception('not set instance not found class('.__CLASS__.') in '.  get_class($obj));
        }
        self::$instance = $obj;
    }

}
