<?php

namespace Traits;

trait initInstance {

    public function __construct() {
        if (self::$instance === null) {
            if (method_exists($this, 'init')) {
                call_user_func_array([$this, 'init'], func_get_args());
            }
            self::$instance = $this;
        } else {
            $this = &self::$instance;
        }
    }

}
