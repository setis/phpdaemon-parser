<?php

namespace Traits;

trait newInstance {

    /**
     * 
     * @return $this|self|static
     */
    public static function &newInstance($mode = false) {
        if ($mode === true || self::$instance === null) {
            $obj = new self;
            if (method_exists($obj, '__construct')) {
                $args = func_get_args();
                if (count($args) > 0) {
                    unset($args[0]);
                }
                if (count($args) > 0) {
                    call_user_func_array([$obj, '__construct'], $args);
                } else {
                    call_user_func([$obj, '__construct']);
                }
                self::$instance = $obj;
            }
        }
        return self::$instance;
    }

}
