<?php

namespace mvc\model\exception;

class sql extends \Exception {

    const unknown = 0;
    const connected_close = 1;

    protected $sql;

    public function __construct($message, $code, $sql = null, $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->sql = $sql;
    }

    public function getSql() {
        return $this->sql;
    }

}
