<?php

namespace mvc\model;

class odm {

    /**
     *
     * @var string
     */
    protected $_collection;

    /**
     *
     * @var string|null
     */
    protected $prefix;

    /**
     *
     * @var string|null
     */
    protected $prefix_plode = '_';

    /**
     *
     * @var string
     */
    public $collection;

    /**
     *
     * @var string
     */
    protected $connect;

    /**
     *
     * @var \PHPDaemon\Clients\Mongo\Pool
     */
    protected $_connection;

    /**
     *
     * @var boolean
     */
    public $throw = false;

    public function __construct() {
        $this->init();
        if ($this->_connection === null) {
            $this->setConnect();
        }
        if ($this->prefix) {
            $this->setPrefix($this->prefix);
        }
    }
    public function __clone() {
//        $this->setConnect();
        $this->Collection();
    }

    /**
     * 
     * @param string|null $name
     * @return $this|this|self|static
     */
    public function setConnect($name = null) {
        if ($name !== null) {
            $this->connect = $name;
        }
        if ($this->connect === null) {
            $this->_connection = \PHPDaemon\Clients\Mongo\Pool::getInstance(['maxconnperserv' => 1000]);
        } else {
            $this->_connection = \BootStrap\Poll::getInstance()->connectByName($this->connect);
        }
        return $this;
    }

    /**
     * 
     * @return string|null
     */
    public function getConnect() {
        return $this->connect;
    }

    /**
     * 
     * @param string $prefix
     * @param string $plode
     * @param boolean $after
     * @return $this|self|static|this
     */
    public function setPrefix($prefix, $plode = null) {
        if ($plode !== null) {
            $this->prefix_plode = $plode;
        }
        $this->prefix = $prefix;
        $this->collection = $this->_collection . $this->prefix_plode . $this->prefix;
        return $this;
    }

    /**
     * 
     * @param boolean $plode
     * @return string
     */
    public function getPrefix($plode = true) {
        return ($plode) ? $this->prefix_plode . $this->prefix : $this->prefix;
    }

    /**
     * @return \PHPDaemon\Clients\Mongo\Collection
     */
    public function Collection() {
        return $this->_connection->{$this->collection};
    }

    public function insert($array) {
        /**
         *  @var $collection \PHPDaemon\Clients\Mongo\Collection 
         */
        $collection = $this->Collection();
        $collection->insert($array, function() {
            \console\msg(__CLASS__ . ':' . __LINE__, func_get_args());
        });
    }

    public function __call($name, $arguments) {
        if (method_exists($this, $name)) {
            call_user_func_array([&$this, $name], $arguments);
        } else {
            call_user_func_array([$this->Collection(), $name], $arguments);
        }
    }

    public function init() {
        
    }

    public function finish() {
        
    }

}
