<?php

namespace mvc\model;

use \mvc\model\exception\sql as Exception;

class sql {

    /**
     * 
     * @param \PHPDaemon\Clients\MySQL\Pool $connection
     * @param string $sql
     * @param callable $onSuccess
     * @param callable $onError
     * @param boolean $throw
     */
    public static function query(&$connection, $sql, $onSuccess = null, $onError = null, $reconnect = null, $throw = false) {
        $connection->getConnection(function ($query) use ($sql, $onSuccess, $onError, $reconnect, $throw) {
            $cb = function($query, $t)use($onSuccess, $onError, $sql, $throw) {
                if ($query->errno > 0) {
                    $e = new Exception($query->errmsg, $query->errno, $sql);
                    if ($onError !== null) {
                        call_user_func($onError, $e, $query);
                    } elseif ($throw === true) {
                        throw $e;
                    }
                } else {
                    if ($onSuccess) {
                        call_user_func($onSuccess, $query);
                    }
                }
            };
            if ($query->isConnected()) {
                try {
                    $query->query($sql, $cb);
                } catch (\Exception $e) {
                    if ($e instanceof \PHPDaemon\Clients\MySQL\ConnectionFinished && $reconnect !== null) {
                        call_user_func([__CLASS__, __FUNCTION__], $reconnect(), $sql, $onSuccess, $onError, null, $throw = false);
                        return;
                    } else {
                        $e = new Exception('erro unknown', Exception::unknown, $sql, $e);
                    }
                    if ($onError !== null) {
                        call_user_func($onError, $e, $query);
                    } elseif ($throw === true) {
                        throw $e;
                    }
                }
            } else {
                if ($reconnect !== null) {
                    call_user_func([__CLASS__, __FUNCTION__], $reconnect(), $onSuccess, $onError, null, $throw = false);
                    return;
                }
                $e = new Exception($query->errmsg, $query->errno, $sql);
                if ($onError !== null) {
                    call_user_func($onError, $e, $query);
                } elseif ($throw === true) {
                    throw $e;
                }
            }
        });
    }

    /**
     * 
     * @param \PHPDaemon\Clients\MySQL\Pool $connection
     * @param string $sql
     * @param callable $onSuccess
     * @param callable $onError
     * @param boolean $throw
     */
    public static function fetch(&$connection, $sql, $onSuccess = null, $onError = null, $reconnect = null, $throw = false) {
        $connection->getConnection(function ($query) use ($sql, $onSuccess, $onError, $reconnect, $throw) {
            $cb = function($query, $t)use($sql, $onSuccess, $onError, $throw) {
                if ($query->errno > 0) {
                    $e = new Exception($query->errmsg, $query->errno, $sql);
                    if ($onError !== null) {
                        call_user_func($onError, $e, $query);
                    } elseif ($throw === true) {
                        throw $e;
                    }
                } else {
                    if ($onSuccess) {
                        call_user_func($onSuccess, $query->resultRows, $query);
                    }
                }
            };

            if ($query->isConnected()) {
                try {
                    $query->query($sql, $cb);
                } catch (\Exception $e) {
                    if ($e instanceof \PHPDaemon\Clients\MySQL\ConnectionFinished && $reconnect !== null) {
                        if(is_string($reconnect)){
                            throw new \Exception(null, 0, $e);
                        }
                        call_user_func([__CLASS__, __FUNCTION__], $reconnect(), $onSuccess, $onError, null, $throw = false);
                        return;
                    } else {
                        $e = new Exception('erro unknown', Exception::unknown, $sql, $e);
                    }
                    if ($onError !== null) {
                        call_user_func($onError, $e, $query);
                    } elseif ($throw === true) {
                        throw $e;
                    }
                }
            } else {
                if ($reconnect !== null) {
                    call_user_func([__CLASS__, __FUNCTION__], $reconnect(), $onSuccess, $onError, null, $throw = false);
                    return;
                }
                $e = new Exception($query->errmsg, $query->errno, $sql);
                if ($onError !== null) {
                    call_user_func($onError, $e, $query);
                } elseif ($throw === true) {
                    throw $e;
                }
            }
        });
    }

    public static function connect($connect = null) {
        if (is_string($connect)) {
            if ($connect === null) {
                return \PHPDaemon\Clients\MySQL\Pool::getInstance('');
            } else {
                return \BootStrap\Poll::getInstance()->connectByName($connect);
            }
        }
        if ($connect instanceof \PHPDaemon\Network\Client) {
            return $connect;
        }
        throw new Exception('not connect');
    }

    const
            fetch = 1,
            query = 0;

    public static function run($connect, $sql, $onSuccess = null, $onError = null, $reconnect = null, $type = self::query) {
        $connection = self::connect($connect);
        if (is_string($connection) && $reconnect === true) {
            $reconnect = function()use($connection) {
//                \console\msg(__FUNCTION__.':'.__LINE__,$reconnect);
                return self::connect($connection);
            };
        } elseif ($reconnect !== null && $reconnect !== false) {
            $reconnect = function()use($reconnect) {
//                \console\msg(__FUNCTION__.':'.__LINE__,$reconnect);
                return self::connect($reconnect);
            };
        }
//        else{
//            \console\msg(__FUNCTION__.':'.__LINE__,$reconnect);
//        }
        switch ($type) {
            case self::fetch:
                self::fetch($connection, $sql, $onSuccess, $onError, $reconnect);
                break;
            case self::query:
            default:
                self::query($connection, $sql, $onSuccess, $onError, $reconnect);
                break;
        }
    }

}
