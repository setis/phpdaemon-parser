<?php

namespace mvc\model;

class install {

    public static function run($connect, $sql, $onSuccess, $onError) {
        \mvc\model\sql::run($connect, $sql, $onSuccess, $onError);
    }

    public static function config(\mvc\model $model) {
        return [
            $model->getConnect(),
            $model->table
        ];
    }

    public static function cb($connect, $table, $onSql, $onSuccess, $onError) {
        return function()use($table, $connect, $onSql, $onSuccess, $onError) {
            \mvc\model\sql::run($connect, call_user_func($onSql, $table), $onSuccess, $onError);
        };
    }

    public static function model2cb(\mvc\model &$model, $onSuccess, $onError) {
        list($connect, $table) = self::config($model);
        self::cb($connect, $table, [get_class($model), 'create_table'], $onSuccess, $onError);
    }

    public static function func($connect, $table, $onSql) {
        return function($onSuccess, $onError)use($table, $connect, $onSql) {
            \mvc\model\sql::run($connect, call_user_func($onSql, $table), $onSuccess, $onError);
        };
    }

    public static function model2func(\mvc\model &$model) {
        list($connect, $table) = self::config($model);
        return self::func($connect, $table, [get_class($model), 'create_table']);
    }

}
