<?php

namespace mvc;

use Exception;

abstract class model {

    const timestamp = 'Y-m-d H:i:s';

    /**
     *
     * @var string table
     */
    protected $_table;

    /**
     * 
     * @var string table+prefix
     */
    public $table;

    /**
     *
     * @var string prefix for table
     */
    protected $prefix;

    /**
     *
     * @var string prefix for table
     */
    protected $prefix_plode = '_';

    /**
     *
     * @var boolean prefix for table
     */
    protected $prefix_after = true;

    /**
     *
     * @var string primary key
     */
    protected $id;

    /**
     *
     * @var \BootStrap\Events
     */
    public $Events;

    /**
     *
     * @var \SQLBuilder\QueryBuilder
     */
    protected static $builder;

    /**
     *
     * @var string
     */
    protected $connect;

    /**
     *
     * @var \PHPDaemon\Clients\MySQL\Pool
     */
    protected $_connection;

    /**
     *
     * @var boolean
     */
    public $throw = false;

    public function __construct() {
        if (self::$builder === null) {
            self::$builder = \PHPDaemon\Applications\Sql\Builder::getInstance()->builder;
        }
        $this->Events = \BootStrap\Events::getInstance();
        $this->init();
        if ($this->_connection === null) {
            $this->setConnect();
        }
        $this->setTable((empty($this->_table)) ? get_class($this) : $this->_table);
    }

    /**
     * 
     * @param int $time
     * @return string
     */
    public static function timestamp($time = null) {
        if ($time === null) {
            $time = time();
        }
        return date(self::timestamp, $time);
    }

//    public function __clone() {
//        $this->setConnect();
//    }
    /**
     * 
     * @param string|null $name
     * @return $this|this|self|static
     */
    public function setConnect($name = null) {
        if ($name !== null) {
            $this->connect = $name;
        }
        if ($this->connect === null) {
            $this->_connection = \PHPDaemon\Clients\MySQL\Pool::getInstance('');
        } else {
            $this->_connection = \BootStrap\Poll::getInstance()->connectByName($this->connect);
        }
        return $this;
    }

    /**
     * 
     * @return string|null
     */
    public function getConnect() {
        return $this->connect;
    }

    public static function in(array $data) {
        return '\'' . implode("','", $data) . '\'';
    }

    /**
     * 
     * @param string $prefix
     * @param string $plode
     * @param boolean $after
     * @return $this|self|static|this
     */
    public function setPrefix($prefix, $plode = null, $after = null) {
        if ($plode !== null) {
            $this->prefix_plode = $plode;
        }
        if ($after !== null) {
            $this->prefix_after = $after;
        }
        $this->prefix = $prefix;
        $this->table = self::table($this->_table, $this->prefix, $this->prefix_plode, $this->prefix_after);
        return $this;
    }

    /**
     * 
     * @param boolean $plode
     * @return string
     */
    public function getPrefix($plode = true) {
        return ($plode) ? ((!$this->prefix_after) ? $this->prefix . $this->prefix_plode : $this->prefix_plode . $this->prefix) : $this->prefix;
    }

    /**
     * 
     * @param type $table
     * @return $this|self|static|this
     */
    public function setTable($table) {
        $this->_table = $table;
        $this->table = self::table($this->_table, $this->prefix, $this->prefix_plode, $this->prefix_after);
        return $this;
    }

    public function install($onSuccess = null, $onError = null) {
        $this->Query(self::create_table($this->table), $onSuccess, $onError);
    }

    /**
     * 
     * @param boolean $prefix
     * @return string
     */
    public function getTable($prefix = true) {
        return($prefix) ? $this->table : $this->_table;
    }

    /**
     * 
     * @return \SQLBuilder\QueryBuilder
     */
    public function Builder() {
        $builder = clone self::$builder;
        $builder->table($this->table);
        return $builder;
    }

    /**
     * 
     * @param string $sql
     * @param callable $onSuccess
     * @param callable $onError
     * @param callable|string $onResult
     * @param \PHPDaemon\Clients\MySQL\Pool $Connection
     */
    public function rawFetch($sql, $onSuccess = null, $onError = null, $throw = null) {
        if ($throw === null) {
            $throw = $this->throw;
        }
        model\sql::fetch($this->_connection, $sql, $onSuccess, $onError, $this->getConnect(), $throw);
    }

    /**
     * 
     * @param string $sql
     * @param callable $onSuccess
     * @param callable $onError
     */
    public function rawQuery($sql, $onSuccess = null, $onError = null, $throw = null) {
        if ($throw === null) {
            $throw = $this->throw;
        }
        model\sql::query($this->_connection, $sql, $onSuccess, $onError, $this->getConnect(), $throw);
    }

    public function __destruct() {
        $this->EventDispatcher = null;
        $this->_connection = null;
//        $this->close();
    }

    /**
     * 
     * @param string $table
     * @param string $prefix
     * @return string
     */
    public static function table($table, $prefix = null, $plode = '_', $after = true) {
        if ($prefix === null) {
            return $table;
        }
        return (!$after) ? $prefix . $plode . $table : $table . $plode . $prefix;
    }

    /*
      public function __set($name, $value) { if ($throw === null) {
      $throw = $this->throw;
      }
      switch ($name) {
      case 'prefix':
      case 'table':
      case 'prefix_plode':
      case 'prefix_after':
      $this->{$name} = $value;
      $this->table = self::table($this->_table, $this->prefix, $this->prefix_plode, $this->prefix_after);
      break;
      }
      }

      public function __get($name) {
      switch ($name) {
      case 'prefix':
      case 'prefix_plode':
      case 'prefix_after':
      return $this->{$name};
      }
      }

      public function __unset($name) {
      switch ($name) {
      case 'prefix':
      $this->prefix = null;
      $this->table = $this->_table;
      break;
      case 'prefix_plode':
      $this->prefix_plode = '_';
      break;
      case 'prefix_after':
      $this->prefix_after = true;
      break;
      }
      }
     */

    public function id(array &$data, $ext = true) {
        if (!isset($data[$this->id])) {
            throw new Exception('not found id');
        }
        $result = $data[$this->id];
        if ($ext) {
            unset($data[$this->id]);
        }
        return $result;
    }

    public function select($onSuccess, $onError = null, $offset = 0, $limit = 100, $columns = '*', $sort = null, $order = null) {
        $builder = $this->Builder()->select($columns)->offset($offset)->limit($limit);
        if ($sort !== null && $sort !== null) {
            $builder->order($sort, $order);
        }
        $sql = $builder->build();
        $this->Fetch($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $sql = $this->Builder()->insert($data)->buildInsert();
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

    public function update(array $data, $onSuccess, $onError) {
        $id = $this->id($data);
        $sql = $this->Builder()->update($data)->where([$this->id => $id])->buildUpdate();
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

    public function delete($id, $onSuccess, $onError) {
        $sql = $this->Builder()->delete()->where([$this->id => $id])->buildDelete();
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

    public function Fetch($sql, $onSuccess, $onError = null, $onResult = null) {
        if (is_string($onSuccess)) {
            $success = $this->Events->dispatch_template($onSuccess, \BootStrap\Events::EVENT_SQL_SELECT_SUCCESS);
        } else {
            $success = $onSuccess;
        }
        if (is_string($onError)) {
            $error = $this->Events->dispatch_template($onError, \BootStrap\Events::EVENT_SQL_SELECT_ERROR);
        } else {
            $error = $onError;
        }
        $this->rawFetch($sql, $success, $error, $onResult);
    }

    public function Query($sql, $onSuccess = null, $onError = null) {
        if ($onSuccess === null) {
            $success = $onSuccess;
        } elseif (is_string($onSuccess)) {
            $success = $this->Events->dispatch_template($onSuccess, \BootStrap\Events::EVENT_SQL_INSERT_SUCCESS);
        } else {
            $success = function($connect)use($onSuccess) {
//                console(__METHOD__, gettype($onSuccess), gettype($connect->insertId));
                call_user_func($onSuccess, $connect->insertId);
            };
        }
        if (is_string($onError)) {
            $error = $this->Events->dispatch_template($onError, \BootStrap\Events::EVENT_SQL_ERROR);
        } else {
            $error = $onError;
        }
        $this->rawQuery($sql, $success, $error);
    }

    public function save(array $data, $onSuccess, $onError) {
        $error = function(\Exception $e)use($data, $onError, $onSuccess) {
            if ($e->getCode() === 1062) {
                $this->insert($data, $onSuccess, $onError);
            } else {
                if (is_string($onError)) {
                    $error = $this->Events->dispatch_template($onError, \BootStrap\Events::EVENT_SQL_ERROR);
                } else {
                    $error = $onError;
                }
                call_user_func_array($error, func_get_args());
            }
        };
        $this->insert($data, $onSuccess, $error);
    }

    public function init() {
        
    }

    public function finish() {
        
    }

}
