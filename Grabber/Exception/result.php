<?php

namespace Grabber\Exception;

use \GuzzleHttp\Event\CompleteEvent;

class result extends \Exception {

    const not_found_key = 0;

    public $content;
    public $key;
    public static $msg = [
        self::not_found_key => 'result not found key:'
    ];
    /**
     * 
     * @param int $code
     * @param array|string $key
     * @param string $content
     * @param \GuzzleHttp\Event\CompleteEvent $client
     * @param \Exception $previous
     */
    public function __construct($code, $key, $content, $client = null, $previous = null) {
        $this->code = $code;
        $this->key = $key;
        $this->content = $content;
//        \console\msg(__METHOD__,(is_array($this->key))?implode(',', $this->key):(string)$this->key);
        if (isset(self::$msg[$code])) {
            $message = self::$msg[$code].json_encode($this->key);
        } else {
            $message = 'unknown error';
        }
        $this->client($client);
        parent::__construct($message, $code, $previous);
    }

    public $client_url;
    public $client_content;

    public function client($client) {
        if ($client instanceof \GuzzleHttp\Event\CompleteEvent) {
            $this->client_url = $client->getResponse()->getEffectiveUrl();
            $this->client_content = $client->getResponse()->getBody()->getContents();
        }
    }

    public function toArray() {
        return [
            'exception' => __CLASS__,
            'code' => $this->code,
            'message' => $this->message,
            'key' => $this->key,
            'trace' => $this->getTraceAsString(),
            'client' => [
                'url' => $this->client_url,
                'content' => $this->client_content,
            ],
            'content' => $this->content
        ];
    }

}
