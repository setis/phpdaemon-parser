<?php

namespace Grabber\Queue;

use Grabber\Events\readmanga as event,
    GuzzleHttp\Event\CompleteEvent,
    Grabber\Index\Storage,
    Grabber\Index\Collector\One,
    Grabber\Index\Data\Short,
    Grabber\Index\Data\Long,
    Grabber\Index\Data\Status,
    Grabber\Index\Data\Year,
    Grabber\Index\Data\Chapter;

class readmanga {

    const name = 'Grabber\Queue\readmanga';

    public static function queue_lists() {
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        $request = $client->createRequest('GET', '/list?type=&sortType=rate&max=100000000');
        $request->getEmitter()->on('complete', function(CompleteEvent $e) {
            $reponse = $e->getResponse();
            $content = $reponse->getBody()->getContents();
            $url = $reponse->getEffectiveUrl();
            \Grabber\Functions\queueCall([self::name, 'job_lists'], [$content, $url]);
        });
        $client->send($request);
    }

    public static function job_lists($content, $url = null) {
        $result = \Grabber\Parse\readmanga::lists($content);
        $names = new One();
        foreach ($result as $value) {
            $requests[] = $value['link'];
            foreach ($value['name'] as $lang => $name) {
                $names->add(new Short([
                    'lang' => Storage::lang($lang),
                    'value' => $name
                ]));
            }
        }
        event::name($names);
        setTimeout(function($event)use(&$requests) {
            $model = \Grabber\Model\register\name::getInstance();
            $model->generate();
            foreach ($requests as $request) {
                \Grabber\Functions\queueCall([__CLASS__, 'queue_content'], [$request]);
            }
            $event->finish();
        }, 3e6);
    }

    public static function queue_content($url) {
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        $request = $client->createRequest('GET', $url);
        $request->getEmitter()->on('complete', function(CompleteEvent $e) {
            $reponse = $e->getResponse();
            $content = $reponse->getBody()->getContents();
            $url = $reponse->getEffectiveUrl();
            \Grabber\Functions\queueCall([__CLASS__, 'job_content'], [$content, $url]);
        });
        $client->send($request);
    }

    public static function job_content($content, $url) {
        try {
            $result = \Grabber\Parse\readmanga::content($content);
        } catch (\Exception $e) {
            $ex = new \Grabber\Exception\parse('fail parse');
            throw $ex;
        }
        $en = Storage::lang('en');
        $ru = Storage::lang('ru');
        $source = Storage::source('readmanga');
        $list = [
            'author',
            'description',
            'genre',
            'status',
            'name',
            'related',
            'like'
        ];
        $exclude = [];
        foreach ($list as $key) {
            switch ($key) {
                case 'author':
                case 'description':
                case 'genre':
                case 'status':
                case 'like':
                case 'cover':
                case 'year':
                    if (!isset($result[$key]) || empty($result[$key])) {
                        $exclude[] = $key;
                    }
                    break;
                case 'related':
                    if (!isset($result['related.manga']) || empty($result['related.manga'])) {
                        $exclude[] = $key;
                    }
                    break;
                case 'name':
                    if (!isset($result[$key]) || empty($result[$key]) || count($result[$key]) < 2) {
                        $exclude[] = $key;
                    }
                    break;
                default:
                    throw new \Exception('not found check for key: ' . $key);
            }
        }
        if (count($exclude) !== 0) {
            $e = new \Grabber\Exception\result(\Grabber\Exception\result::not_found_key, $exclude, $result);
            $e->client_content = $content;
            $e->client_url = $url;
            if (in_array('name', $exclude)) {
                \console\msg('exception', $e->toArray());
                throw $e;
            }
            if (in_array('related', $exclude) || in_array('like', $exclude)) {
                \console\msg('warning', $e->toArray());
            }
        }
        $params = [1e6, 30e6, true];
        /**
         * сборщик
         */
        $gc = new \Grabber\Index\Collector\Multi();
        /**
         * создание ключей
         */
        $names = new One($params);
        foreach ($result['name'] as $lang => $name) {
            $names->add(new Short([
                'lang' => Storage::lang($lang),
                'value' => $name
            ]));
        }
        $authors = new One($params);
        if (!in_array('author', $exclude)) {
            foreach ($result['author'] as $author) {
                $authors->add(new Short([
                    'value' => $author['name'],
                    'lang' => $en
                ]));
            }
        }
        $description = new One($params);
        if (!in_array('description', $exclude)) {
            $description->add(new Long([
                'value' => $result['description'],
                'lang' => $ru
            ]));
        }
        $genres = new One($params);
        if (!in_array('genre', $exclude)) {
            foreach ($result['genre'] as &$genre) {
                $genres->add(new Short([
                    'value' => $genre['name'],
                    'lang' => $ru
                ]));
            }
        }
//        $versionist = new One();
//        foreach ($result['translator'] as $value) {
//            $versionist->add(new Short([
//                'lang' => $ru,
//                'value' => $value['name']
//            ]));
//        }
        $status = new One($params);
        if (!in_array('status', $exclude)) {
            $status->add(new Status([
                'status' => (($result['status'] == "завершен") ? 1 : 0),
                'source' => $source
            ]));
        }
        $year = new One($params);
        if (!in_array('year', $exclude)) {
            foreach ($result['year'] as $y) {
                $year->add(new Year([
                    'year' => $y,
                    'source' => $source,
                ]));
            }
        }

        $related = $like = null;
        if (!in_array('related', $exclude)) {
            $related = new One($params);
            foreach ($result['related.manga'] as $manga) {
                $related->add(new Short([
                    'lang' => $en,
                    'value' => $manga['name']
                ]));
            }
            $gc->add($related, 'related');
        }
        if (!in_array('like', $exclude)) {
            $like = new One($params);
            foreach ($result['like'] as $manga) {
                $like->add(new Short([
                    'lang' => $en,
                    'value' => $manga['name']
                ]));
                $gc->add($like, $key);
            }
        }
//        $build = new \ArrayObject();
//        $build->setFlags(\ArrayObject::ARRAY_AS_PROPS);
//        $gc->addAll([
//            'name' => &$names,
//            'author' => &$authors,
//            'description' => &$description,
//            'genre' => &$genres,
////            'versionist' => $versionist,
//            'status' => &$status,
//            'year' => &$year
//        ]);
//        $gc->overall(function($storage)use(&$build) {
//            foreach ($storage as $object) {
//                $name = $storage[$object];
//                if ($object instanceof One) {
//                    $object->overall(function($storage)use(&$build, $name) {
//                        foreach ($storage as $object) {
//                            $result[] = $object->toArray();
//                        }
//                        $build->{$name} = &$result;
//                    });
//                } elseif ($object instanceof \Grabber\Index\Collector\Hand) {
//                    $build->{$name} = $object->storage;
//                }
//            }
//            setTimeout(function($event)use(&$build, &$storage) {
//                if ($storage->count() === $build->count()) {
//                    \console\msg('build', $build);
//                    $event->finish();
//                    return;
//                }
//                $event->timeout();
//            }, 1e3);
//        });

        $content_chapter = $result['chapters'];
        $chapter_list = function($id)use(&$content_chapter, $ru, $source) {
//            \Grabber\Functions\queueCall([__CLASS__, 'chapter'], [$content_chapter, $id, $source, $ru]);
        };

        $chapter = function($related)use(&$content_chapter, $ru, $source) {

            event::content_chapter($content_chapter, $source, $ru, $related);
        };
        if (!in_array('cover', $exclude)) {
            $handler_cover = $result['cover'];
            $cover = function($related)use(&$handler_cover, $source) {
                self::queue_cover($handler_cover, $source, $related);
            };
        }
        event::name($names, function($name)use( &$authors, &$description, &$genres, &$status, &$year, &$related, &$like, &$chapter, &$chapter_list, &$cover) {
            $chapter($name);
            $authors->overall(event::db([
                        '\Grabber\Model\short',
                        'author',
                        'name_relations_author',
                        'name_relations_autor_source'
                            ], $name));

            $description->overall(event::db([
                        '\Grabber\Model\long',
                        'description',
                        'name_relations_description',
                        'name_relations_description_source'
                            ], $name));
            $genres->overall(event::db([
                        '\Grabber\Model\short',
                        'genre',
                        'name_relations_genre',
                        'name_relations_genre_source'
                            ], $name));
            foreach ($status->storage as $object) {
                $object->name = $name;
            }
            foreach ($year->storage as $object) {
                $object->name = $name;
            }
            $status->overall(event::db([
                        '\Grabber\Model\status',
                        'status',
                        'name_relations_status',
                        'name_relations_status_source'
                            ], $name));
            $year->overall(event::db([
                        '\Grabber\Model\year',
                        'year',
                        'name_relations_year',
                        'name_relations_year_source'
                            ], $name));
            if ($related !== null) {
                $related->overall(event::db([
                            '\Grabber\Model\short',
                            'name',
                            'name_relations_related',
                            'name_relations_related_source'
                                ], $name));
            }
            if ($like !== null) {
                $like->overall(event::db([
                            '\Grabber\Model\short',
                            'name',
                            'name_relations_like',
                            'name_relations_like_source'
                                ], $name));
            }
//            $chapter_list($name);
//            $cover($name);
        });
    }

    public static function queue_cover($requests, $source, $related) {
        event::nameFindByRelated($related, function($id)use(&$requests, $source) {
            foreach ($requests as $request) {
                \Grabber\Functions\queueCall([__CLASS__, 'job_cover'], [$request, $id, $source]);
            }
        });
    }

    public static function job_cover($url, $id, $source) {
//        \console\msg(__METHOD__, func_get_args());
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
            'defaults' => [
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
                ],
            ]
        ]);
        $request = $client->createRequest('GET', $url);
        $request->getEmitter()->on('complete', function(CompleteEvent &$e)use( $id, $source, $url) {
//            \PHPDaemon\Core\Daemon::log('job_cover', $e->getResponse()->getStatusCode(), $url, $e->getResponse()->getBody()->getSize());
            event::cover($e->getResponse()->getBody()->getContents(), $e->getResponse()->getEffectiveUrl(), $id, $source);
        });
        try {
            $client->send($request);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function queue_chapter_list($content_chapter, $related, $source, $lang) {
        event::nameFindByRelated($related, function($id)use(&$content_chapter, $source, $lang) {
//            \console\msg(__METHOD__, $requests);
//            foreach ($content_chapter as $value) {
//                \Grabber\Functions\queueCall([__CLASS__, 'job_chapter_list'], [$value['link'], $id, $source, $lang]);
//            }
            \Grabber\Functions\queueCall([__CLASS__, 'chapter'], [$content_chapter, $id, $source, $lang]);
        });
    }

    public static function job_chapter_list($url, $id, $source, $lang) {
//        \console\msg(__METHOD__, func_get_args());
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        $request = $client->createRequest('GET', $url, ['query' => ['mature' => 1]]);
        $request->getEmitter()->on('complete', function($e)use($id, $source, $lang) {
            $reponse = $e->getResponse();
            $content = $reponse->getBody()->getContents();
            $url = $reponse->getEffectiveUrl();
//            \console\msg(__FUNCTION__, $content);
            try {
                $result = \Grabber\Parse\readmanga::chapter_list($content);
            } catch (\Exception $e) {
                $ex = new \Grabber\Exception\parse('fail parse');
                throw $ex;
            }
            if (!is_array($result) || count($result) === 0) {
                $e = new \Grabber\Exception\result(\Grabber\Exception\result::not_found_key, ['chapter_list'], $result);
                $e->client_content = $content;
                $e->client_url = $url;
                \console\msg('exception', $e->toArray());
                throw $e;
            }
//            \console\msg(__METHOD__, [__CLASS__, 'chapter'], [$result, $id, $source, $lang]);
        });
        $client->send($request);
    }

    public static function chapter($result, $related, $source, $lang) {
        \console\msg(__METHOD__, func_get_args());
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        foreach ($result as $arr) {
            $chapter = $arr['chapter'];
            $request = $client->createRequest('GET', $arr['link'], ['query' => ['mature' => 1]]);
            $request->getEmitter()->on('complete', function(CompleteEvent $e)use($chapter, $related, $source, $lang) {
                $reponse = $e->getResponse();
                $content = $reponse->getBody()->getContents();
                $url = $reponse->getEffectiveUrl();
                self::queue_chapter($content, $url, $related, $source, $lang, $chapter);
            });
            $client->send($request);
        }
    }

    public static function queue_chapter($content, $url, $related, $source, $lang, $chapter) {
        try {
            $result = \Grabber\Parse\readmanga::chapter_image($content);
        } catch (\Exception $e) {
            $ex = new \Grabber\Exception\parse('fail parse');
            throw $ex;
        }
        if (!is_array($result) || count($result) === 0) {
            $e = new \Grabber\Exception\result(\Grabber\Exception\result::not_found_key, ['chapter_image'], $result);
            $e->client_content = $content;
            $e->client_url = $url;
            \console\msg('exception', $e->toArray());
            throw $e;
        }
        \Grabber\Functions\queueCall([__CLASS__, 'job_chapter'], [$result, $related, $source, $lang, $chapter]);
    }

    public static function job_chapter($result, $related, $source, $lang, $chapter) {
//        \console\msg(__METHOD__, $result);
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        foreach ($result as $i => $link) {
            $page = $i + 1;
            $request = $client->createRequest('GET', $link);
            $request->getEmitter()->on('complete', function(CompleteEvent $e)use($chapter, $page, $related, $source, $lang) {
                $reponse = $e->getResponse();
                $content = $reponse->getBody()->getContents();
                $url = $reponse->getEffectiveUrl();
                event::chapter($content, $url, $related, $source, $lang, $chapter, $page);
            });
            $client->send($request);
        }
    }

}
