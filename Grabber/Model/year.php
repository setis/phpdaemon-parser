<?php

namespace Grabber\Model;

class year extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'year';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT "$year$source$name",
  `year` int(4) NOT NULL,
  `name` varbinary(767) NOT NULL,
  `source` int(3) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;';
    }

    public function years($onSuccess, $onError = null) {
        $this->Fetch("SELECT DISTINCT `year` FROM `$this->table`", $onSuccess, $onError);
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $sql = "INSERT INTO {$this->table} ( id,year,name,source) VALUES ('{$data['id']}',{$data['year']},{$data['name']},{$data['source']})";
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

}
