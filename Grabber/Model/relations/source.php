<?php

namespace Grabber\Model\relations;

class source extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'relations_source';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `'.$table.'` (
  `id` varchar(255) NOT NULL,
  `source` int(3) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `time` timestamp NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;';
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $sql = "INSERT INTO {$this->table} ( id,source,relation,time) VALUES ('{$data['id']}',{$data['source']},'{$data['relation']}','{$data['time']}')";
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

}
