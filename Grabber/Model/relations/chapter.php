<?php

namespace Grabber\Model\relations;

class chapter extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'relations';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT "$chapter.$page.$resource.$property.$link",
  `property`varchar(255) NOT NULL COMMENT "chapter_property_{$chapter}.id",
  `resource` int(3) NOT NULL,
  `link` varchar(255) NOT NULL COMMENT "chapter_link_{$chapter}.id",
  `chapter` int(3) NOT NULL,
  `page` int(3) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `property` (`property`),
  KEY `link` (`link`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;';
    }

    public static function sql_insert($table,array $data){
        return "INSERT INTO {$table} ( id,property,link,resource,chapter,page) VALUES ('{$data['id']}','{$data['property']}','{$data['link']}','{$data['resource']}','{$data['chapter']}','{$data['page']}')";
    }
    public function insert(array $data, $onSuccess = null, $onError = null) {
        $this->Query(self::sql_insert($this->table, $data), $onSuccess, $onError);
    }

}
