<?php

namespace Grabber\Model\image;

class property extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'image_property';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT \'$type.$hash.$height.$width\',
  `hash` binary(16) NOT NULL COMMENT \'md5\',
  `height` int(4) NOT NULL COMMENT \'высота\',
  `width` int(4) NOT NULL COMMENT \'ширина\',
  `type` int(2) NOT NULL COMMENT \'тип изображения 1 = GIF, 2 = JPG, 3 = PNG, 4 = SWF, 5 = PSD, 6 = BMP, 7 = TIFF (порядок байтов Интела), 8 = TIFF (порядок байтов Моторолы), 9 = JPC, 10 = JP2, 11 = JPX, 12 = JB2, 13 = SWC, 14 = IFF, 15 = WBMP, 16 = XBM\',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
    }

    public static function sql_insert($table, array $data) {
        return "INSERT INTO {$table} ( id,hash,height,width,type) VALUES ('{$data['id']}',UNHEX('{$data['hash']}'),{$data['height']},{$data['width']},{$data['type']})";
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $this->Query(self::sql_insert($this->table, $data), $onSuccess, $onError);
    }

}
