<?php

namespace Grabber\Model;

class relations extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'relations';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL,
  `src` varchar(255) NOT NULL,
  `dst` varchar(255) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `src` (`src`),
  KEY `dst` (`dst`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;';
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $sql = "INSERT INTO {$this->table} ( id,src,dst) VALUES ('{$data['id']}','{$data['src']}','{$data['dst']}')";
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

}
