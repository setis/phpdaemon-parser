<?php

namespace Grabber\Model;

class chapter extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
        $this->table = 'chapter';
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT \'$chapter|$hash|$size\',
  `chapter` int(3) NOT NULL COMMENT \'номер главы\',
  `name` varchar(255) CHARACTER SET utf8 NULL COMMENT \'название главы\',
  `hash` binary(16) NOT NULL COMMENT \'md5($title)\',
  `size` int(3) NOT NULL COMMENT \'strlen($title)\',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        if ($data['name'] === null) {
            $data['name'] = 'NULL';
        } else {
            $data['name'] = '\'' . str_replace('\'', "\\'", $data['name']) . '\'';
        }
        $sql = "INSERT INTO {$this->table} ( id,hash,size,name,chapter) VALUES ('{$data['id']}',UNHEX('{$data['hash']}'),{$data['size']},'{$data['name']}',{$data['chapter']})";
        $this->Query($sql, $onSuccess, $onError);
    }

    public function langs($onSuccess, $onError = null) {
        $this->Fetch("SELECT DISTINCT `lang` FROM `$this->table`", $onSuccess, $onError);
    }

}
