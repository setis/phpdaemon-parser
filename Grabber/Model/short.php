<?php

namespace Grabber\Model;

class short extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT "$hash[md5($value)]$lang:$size[len($value)]",
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `hash` binary(16) NOT NULL COMMENT "md5",
  `size` int(10) NOT NULL COMMENT "strlen($value)",
  `lang` int(2) NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $data['value'] = '\'' . str_replace('\'', "\\'", $data['value']) . '\'';
        $sql = "INSERT INTO {$this->table} ( id,hash,lang,size,value) VALUES ('{$data['id']}',UNHEX('{$data['hash']}'),{$data['lang']},{$data['size']},{$data['value']})";
        \console\log('sql', $sql);
        $this->Query($sql, $onSuccess, $onError);
    }
    
}
