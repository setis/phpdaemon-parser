<?php

namespace Grabber\Model;

class exception extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varbinary(767) CHARACTER SET utf8 NOT NULL COMMENT "$hash[sha1($value)]$lang:$size[strlen($value)]",
  `lang` int(3) NOT NULL,
  `value` text CHARACTER SET utf8 NOT NULL,
  `hash` binary(20) NOT NULL COMMENT "sha1",
  `size` int(10) NOT NULL COMMENT "strlen($value)",
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
';
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $data['value'] = '\'' . str_replace('\'', "\\'", $data['value']) . '\'';
        $sql = "INSERT INTO {$this->table} ( id,hash,lang,size,value) VALUES (UNHEX('{$data['id']}'),UNHEX('{$data['hash']}'),{$data['lang']},{$data['size']},{$data['value']})";
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

    public function langs($onSuccess, $onError = null) {
        $this->Fetch("SELECT DISTINCT `lang` FROM `$this->table`", $onSuccess, $onError);
    }

}
