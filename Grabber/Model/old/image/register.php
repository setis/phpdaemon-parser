<?php
namespace Grabber\Model\image;

class register extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'image_register';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) NOT NULL COMMENT '\"\$property:\$source:\$name\"',
  `property` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `property` (`property`),
  KEY `name` (`name`),
  KEY `source` (`source`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
