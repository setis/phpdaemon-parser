<?php
namespace Grabber\Model\image;

class source extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'image_source';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `uniq` varchar(255) NOT NULL COMMENT '\"\$service:\$name:\$size:\$md5\"',
  `md5` varchar(32) NOT NULL,
  `size` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` text CHARACTER SET utf8 NOT NULL,
  `source` varchar(255) CHARACTER SET ascii NOT NULL COMMENT 'сервиса',
  `id` varchar(255) NOT NULL COMMENT '\"\$service:\$size:\$md5\"',
  UNIQUE KEY `uniq` (`uniq`),
  KEY `name` (`name`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
