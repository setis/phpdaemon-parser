<?php

namespace Grabber\Model;

class source extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'source';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) NOT NULL,
  `resource` int(11) NOT NULL,
  `url` text CHARACTER SET utf8 NOT NULL,
  `tables` int(10) unsigned NOT NULL,
  `index` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`),
  KEY `index` (`index`),
  KEY `resource` (`resource`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
