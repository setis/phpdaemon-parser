<?php

namespace Grabber\Model;

class register extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'register';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` binary(767) NOT NULL,
  `src_lang` int(2) NOT NULL,
  `src_id` binary(767) NOT NULL,
  `dst_lang` int(2) NOT NULL,
  `dst_id` binary(2) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `src_id` (`src_id`),
  KEY `dst_id` (`dst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
