<?php

namespace Grabber\Model\storage;

class db extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'storage_db';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) CHARACTER SET ascii NOT NULL,
  `height` int(4) NOT NULL COMMENT 'высота',
  `width` int(4) NOT NULL COMMENT 'ширина',
  `depth` int(2) NOT NULL COMMENT 'глубина',
  `format` varchar(10) NOT NULL COMMENT 'формат',
  `size` int(10) unsigned NOT NULL COMMENT 'размер в байтах',
  `md5` binary(16) NOT NULL COMMENT 'hash_md5'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
