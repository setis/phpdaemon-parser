<?php

namespace Grabber\Model;

class success extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'success';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `task` varchar(255) CHARACTER SET ascii NOT NULL,
  `resource` text NOT NULL COMMENT 'html',
  `storage` text NOT NULL COMMENT 'storage',
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
