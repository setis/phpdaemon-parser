<?php

namespace Grabber\Model\access;

class vk extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'access_vk';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) NOT NULL DEFAULT '\$user_id.'':''.\$app_id',
  `user_id` bigint(20) NOT NULL,
  `app_id` bigint(20) NOT NULL,
  `scopes` varchar(255) NOT NULL,
  `access` varchar(255) NOT NULL,
  `secret` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
