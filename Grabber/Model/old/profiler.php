<?php

namespace Grabber\Model;

class profiler extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'profiler';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `task` varchar(255) CHARACTER SET ascii NOT NULL DEFAULT 'task.id',
  `event` varchar(255) CHARACTER SET ascii NOT NULL,
  `time` datetime NOT NULL,
  `start` float NOT NULL,
  `end` float NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
