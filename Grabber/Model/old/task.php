<?php

namespace Grabber\Model;

class task extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'task';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) NOT NULL COMMENT 'len(\$url):md5(\$url):md5(\$config)',
  `url` text NOT NULL,
  `config` text NOT NULL,
  `source` varchar(255) CHARACTER SET ascii DEFAULT NULL,
  `during` bit(1) NOT NULL DEFAULT b'1' COMMENT '0 - завершенно , 1 - в процессе',
  `created` datetime NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `source` (`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
