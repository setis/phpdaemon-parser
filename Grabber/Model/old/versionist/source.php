<?php

namespace Grabber\Model\versionist;

class source extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'versionist_source';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) CHARACTER SET ascii NOT NULL COMMENT '\"\$source:\$translator|\$name\"',
  `translator` varchar(5) NOT NULL COMMENT 'versionist_id.id',
  `name` varchar(5) CHARACTER SET utf8 NOT NULL COMMENT 'name.id(en:*)',
  `source` varchar(255) NOT NULL COMMENT 'readmanga',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
