<?php

namespace Grabber\Model\versionist;

class register extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
//        $this->_table = 'versionist_relations_name';
        $this->_table = 'versionist_register';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) CHARACTER SET ascii NOT NULL COMMENT '\"\$translator|\$name\"',
  `translator` varchar(5) NOT NULL COMMENT 'versionist_id.id',
  `name` varchar(5) CHARACTER SET utf8 NOT NULL COMMENT 'name.id(en:*)',
   UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
