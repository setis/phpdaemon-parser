<?php

namespace Grabber\Model\versionist;

class id extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'versionist_id';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) CHARACTER SET ascii NOT NULL COMMENT '\"\strlen(\$name):\$lang:\md5(\$name)\"',
  `lang` varchar(5) NOT NULL COMMENT 'код языка',
  `name` varchar(5) CHARACTER SET utf8 NOT NULL COMMENT 'ширина',
   UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
