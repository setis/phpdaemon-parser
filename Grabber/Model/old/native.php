<?php

namespace Grabber\Model;

class native extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'native';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) NOT NULL COMMENT 'name.id',
  `lang` varchar(5) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `lang` (`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
