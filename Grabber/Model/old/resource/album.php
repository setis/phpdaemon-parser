<?php

namespace Grabber\Model\resource;

class album extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'resource_album';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` varchar(255) CHARACTER SET ascii NOT NULL,
  `name` varchar(255) CHARACTER SET ascii NOT NULL,
  `part` int(4) NOT NULL,
  `chapter` int(10) NOT NULL,
  `lang` varchar(3) CHARACTER SET ascii NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

    public function find(array $params, $onSuccess, $onError) {
        $this->Fetch($this->Builder()->where($params)->build(), $onSuccess, $onError);
    }

}
