<?php

namespace Grabber\Model\resource;

class service extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'resource_service';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->Query($sql, $onSuccess, $onError);
    }

    public $services = [];

    public function load($cache, $onSuccess = null, $onError = null) {
        if ($cache === false || count($this->services) === 0) {
            $this->Fetch($this->Builder()->select('*')->build(), function($result)use($onSuccess) {
                $return = [];
                foreach ($result as $row) {
                    $return[$row['name']] = $row['id'];
                }
                if ($onSuccess !== null) {
                    call_user_func($onSuccess, $return);
                }
                $this->services = $return;
            }, $onError);
        } else {
            call_user_func($onSuccess, $this->services);
        }
    }

    public function event_reload() {
        Daemon::$process->IPCManager->sendBroadcastCall('\\' . get_class($this), 'load', [false]);
    }

    public function add($id, $name) {
        $this->services[$name] = $id;
    }

    public function event_add($id, $name) {
        Daemon::$process->IPCManager->sendBroadcastCall('\\' . get_class($this), 'add', [$id, $name]);
    }

    public function insert($name, $onSuccess, $onError = null) {
        $success = function($id)use($onSuccess,$name) {
            call_user_func_array($onSuccess, func_get_args());
            $this->event_add($id, $name);
        };
        parent::insert(['name' => $name], $success, $onError);
    }

    public function find(array $params, $onSuccess, $onError) {
        $this->Fetch($this->Builder()->where($params)->build(), $onSuccess, $onError);
    }

}
