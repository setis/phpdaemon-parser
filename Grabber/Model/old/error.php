<?php

namespace Grabber\Model;

class error extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'error';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public function install($onSuccess = null, $onError = null) {
        $sql = "CREATE TABLE `{$this->table}` (
  `id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `task` varchar(255) CHARACTER SET ascii NOT NULL,
  `event` varchar(255) CHARACTER SET ascii NOT NULL,
  `log` text NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->Query($sql, $onSuccess, $onError);
    }

}
