<?php

namespace Grabber\Model;

class status extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->_table = 'status';
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT "$status$source$name",
  `status` bit(1) NOT NULL COMMENT "0-продолжается 1 - завершенно ",
  `name` varbinary(767) NOT NULL,
  `source` int(3) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;';
    }

    public function langs($onSuccess, $onError = null) {
        $this->Fetch("SELECT DISTINCT `lang` FROM `$this->table`", $onSuccess, $onError);
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $sql = "INSERT INTO {$this->table} ( id,status,name,source) VALUES ('{$data['id']}','{$data['status']}','{$data['name']}',{$data['source']})";
        $this->Query($sql, $onSuccess, function($e, $connect)use($onError, $sql) {
            call_user_func($onError, $e, $connect, $sql);
        });
    }

}
