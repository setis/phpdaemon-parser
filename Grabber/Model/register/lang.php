<?php

namespace Grabber\Model\register;

class lang extends \mvc\model {

    use \Traits\getInstance;

    const table_default = 'lang';

    public function init() {
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
        $this->_table = self::table_default;
    }

    public static function create_table($table = self::table_default) {
        return 'CREATE TABLE `'.$table.'` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `lang` varchar(5) NOT NULL COMMENT "код языка ru,en",
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang` (`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
    }

}
