<?php

namespace Grabber\Model;

class chapter extends \mvc\model {

    use \Traits\getInstance;

    const table_default = 'chapter';

    public function init() {
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
        $this->_table = self::table_default;
    }

    public static function create_table($table = self::table_default) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT \'$lang.$name.$chapter\',
  `chapter` int(3) NOT NULL,
  `name` varchar(255) NOT NULL \'name.id\',
  `lang` int(2) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
    }

    public static function sql_insert($table, array $data) {
        return "INSERT INTO {$table} ( id,name,lang,chapter) VALUES ('{$data['id']}','{$data['name']}',{$data['lang']},{$data['chapter']})";
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $this->Query(self::sql_insert($this->table, $data), $onSuccess, $onError);
    }

}
