<?php

namespace Grabber\Model\register;

class name extends \mvc\model {

    use \Traits\getInstance;

    const table_default = 'list_name';

    public function init() {
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
        $this->_table = self::table_default;
    }

    public static function create_table($table = self::table_default) {
        return 'CREATE TABLE `' . $table . '` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT "название манги",
  `related` varchar(255) NOT NULL COMMENT "name.id",
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `related` (`related`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
    }

    public static function sql_generate($table = self::table_default) {
        return 'INSERT INTO `' . $table . '` (name,related) SELECT value, id FROM `name` WHERE lang=0;';
    }

    public function generate($onSuccess = null, $onError = null) {
        $this->Query(self::sql_generate($this->table), $onSuccess, $onError);
    }

    public function find($name, $onSuccess = null, $onError = null) {
        $sql = "SELECT id FROM `{$this->table}` WHERE name = '$name';";
        $this->Fetch($sql, function($result)use($onSuccess) {
            call_user_func($onSuccess, (isset($result[0])) ? $result[0] : null);
        }, $onError);
    }

    public function find_byKey_related($related, $onSuccess = null, $onError = null) {
        $sql = "SELECT id FROM `{$this->table}` WHERE related = '$related';";
        \console\msg('sql::'.__METHOD__,$sql,$related);
        $this->Fetch($sql, function($result)use($onSuccess) {
            call_user_func($onSuccess, ((isset($result[0]['id'])) ? $result[0]['id'] : null));
        }, $onError);
    }

}
