<?php

namespace Grabber\Model;

class url extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->connect = \BootStrap\Env::$cfg['Grabber'];
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT "$hash[sha1($value)]:$size[strlen($url)]",
  `host` varchar(255) NOT NULL,
  `hash` binary(20) NOT NULL COMMENT "sha1",
  `size` int(10) NOT NULL COMMENT "strlen($value)",
   `url` text CHARACTER SET utf8 NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
';
    }

    public static function sql_insert($table, array $data) {
        $data['url'] = '\'' . str_replace('\'', "\\'", urldecode($data['url'])) . '\'';
        return "INSERT INTO {$table} ( id,hash,size,url,host) VALUES ('{$data['id']}',UNHEX('{$data['hash']}'),{$data['size']},{$data['url']},{$data['host']})";
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $this->Query(self::sql_insert($this->table, $data), $onSuccess, $onError);
    }

}
