<?php

namespace Grabber\Index\Data;

use \Grabber\Index\Storage;

class Short extends \Grabber\Index\AbstractData {

    public $type = Storage::data_short;

    use Traits\Lang;

    /**
     *
     * @var string
     */
    protected $value;

    /**
     *
     * @var string hash
     */
    public $hash;

    /**
     *
     * @var int
     */
    public $size;

    public function id() {
        if ($this->id === null) {
            $this->id = $this->lang . self::plode . $this->hash . self::plode . $this->size;
        }
        return $this->id;
    }

    public function hash() {
        $this->hash = null;
        $this->hash = md5($this->value);
        $this->lock('hash', true);
        $this->addTimer(setTimeout(function($event) {
                    if ($this->hash === null) {
                        $event->timeout();
                        return false;
                    }
                    $this->lock('hash', false);
                    $this->removeTimer($event->id);
                    $event->finish();
                }, 1e3));
    }

    public static function initilize() {
        Storage::$import[Storage::data_short] = ['value', 'lang'];
        Storage::$lock[Storage::data_short] = 'hash';
    }

    public function __set($name, $value) {
        if ($this->wrapper === false) {
            $this->{$name} = $value;
            return false;
        }
        switch ($name) {
            case 'value':
                $this->value = $value;
                $this->size = strlen($value);
                $this->hash();
                $this->id = null;
                break;
            case 'lang':
                $this->lang = $value;
                $this->lang();
                $this->id = null;
                break;
            default :
                $this->{$name} = $value;
                break;
        }
    }

    public function toArray() {
        return [
            'id' => $this->id(),
            'hash' => $this->hash,
            'lang' => $this->lang,
            'size' => $this->size,
            'value' => $this->value
        ];
    }

}
