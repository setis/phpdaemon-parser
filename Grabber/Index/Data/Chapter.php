<?php

namespace Grabber\Index\Data;

use \Grabber\Index\Storage;

class Chapter extends \Grabber\Index\AbstractData {

    public $type = Storage::data_chapter;

    /**
     *
     * @var int
     */
    protected $chapter;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string hex
     */
    public $hash;

    /**
     *
     * @var int
     */
    public $size;

    public static function initilize() {
        Storage::$import[Storage::data_chapter] = ['chapter', 'name'];
        Storage::$lock[Storage::data_chapter] = 'hash';
    }

    public function hash() {
        $this->lock('hash', true);
        $this->hash = null;
        $this->hash = md5($this->name);
        $this->addTimer(setTimeout(function($event) {
                    if ($this->hash === null) {
                        $event->timeout();
                        return false;
                    }
                    $this->lock('hash', false);
                    $this->removeTimer($event->id);
                    $event->finish();
                }, 1e3));
    }

    public function id() {
        if ($this->id === null) {
            $this->id = $this->chapter . self::plode . $this->hash . self::plode . $this->size;
        }
        return $this->id;
    }

    public function __set($name, $value) {
        if ($this->wrapper === false) {
            $this->{$name} = $value;
            return false;
        }
        switch ($name) {
            case 'chapter':
                $this->chapter = $value;
                $this->id = null;
                break;
            case 'name':
                $this->name = $value;
                if ($value === null) {
                    $this->size = 0;
                } else {
                    $this->size = strlen($value);
                }
                $this->hash();
                $this->id = null;
                break;
            default :
                $this->{$name} = $value;
                break;
        }
    }

    public function toArray() {
        return [
            'id' => $this->id(),
            'hash' => $this->hash,
            'name' => $this->name,
            'chapter' => $this->chapter,
            'size' => $this->size,
        ];
    }

}
