<?php

namespace Grabber\Index\Data\Image;

use \Grabber\Index\Storage;

class chapter extends \Grabber\Index\AbstractData {

    public $type = Storage::data_image_chapter;
    public $wrapper = false;

    /**
     *
     * @var int
     */
    public $chapter;

    /**
     *
     * @var int
     */
    public $page;

    /**
     *
     * @var string
     */
    public $property;

    /**
     *
     * @var int
     */
    public $resource;

    /**
     *
     * @var string
     */
    public $link;

    public static function initilize() {
        Storage::$import[Storage::data_image_chapter] = ['property', 'link', 'resource', 'page', 'chapter'];
    }

    public function id() {
        if ($this->id === null) {
            $this->id = $this->chapter . self::plode . $this->page . self::plode . $this->resource . self::plode . $this->property . self::plode . $this->link;
        }
        return $this->id;
    }
  

    public function toArray() {
        return [
            'id' => $this->id(),
            'property' => $this->property,
            'link' => $this->link,
            'resource' => $this->resource,
            'chapter' => $this->chapter,
            'page' => $this->page,
        ];
    }

}
