<?php

namespace Grabber\Index\Data\Image;

use \Grabber\Index\Storage;

class property extends \Grabber\Index\AbstractData {

    public $type = Storage::data_image_property;

    /**
     *
     * @var string hex
     */
    public $hash;

    /**
     *
     * @var int
     */
    public $width;

    /**
     *
     * @var int
     */
    public $height;

    /**
     *
     * @var int
     */
    public $size;

    /**
     *
     * @var int
     */
    public $format;

    /**
     *
     * @var \GuzzleHttp\Event\CompleteEvent
     */
    protected $event;

    /**
     *
     * @var string
     */
    protected $content;

    /**
     *
     * @var string
     */
    protected $file;

    public function id() {
        if ($this->id === null) {
            $this->id = $this->format . self::plode . $this->hash . self::plode . $this->size . self::plode . $this->width . self::plode . $this->height;
        }
        return $this->id;
    }

    public static function initilize() {
        Storage::$import[Storage::data_image_property] = ['format', 'size', 'width', 'height', 'event', 'content', 'file'];
        Storage::$lock[Storage::data_image_property] = 'hash';
    }

    public function event(\GuzzleHttp\Event\CompleteEvent $event) {
        $body = $event->getResponse()->getBody();
        $image = $body->getContents();
        $this->hash = null;
        $this->hash = md5($image);
        $this->lock('hash', true);
        $this->size = $body->getSize();
        list($this->width, $this->height, $this->format) = getimagesizefromstring($image);
        $this->addTimer(setTimeout(function($event) {
                    if ($this->hash === null) {
                        $event->timeout();
                        return;
                    }
                    $this->lock('hash', false);
                    $this->removeTimer($event->id);
                    $event->finish();
                }, 1e3));
    }

    public function content($image) {
        $this->hash = null;
        $this->hash = md5($image);
        $this->lock('hash', true);
        $this->size = strlen($image);
        list($this->width, $this->height, $this->format) = getimagesizefromstring($image);
        $this->addTimer(setTimeout(function($event) {
                    if ($this->hash === null) {
                        $event->timeout();
                        return;
                    }
                    $this->lock('hash', false);
                    $this->removeTimer($event->id);
                    $event->finish();
                }, 1e3));
    }

    public function file($file) {
        $this->hash = md5_file($file);
        list($this->width, $this->height, $this->format) = getimagesize($file);
        $this->size = filesize($file);
    }

    public function __set($name, $value) {
        if ($this->wrapper === false) {
            $this->{$name} = $value;
            return false;
        }
        switch ($name) {
            case 'event':
                $this->event($value);
                $this->id = null;
                break;
            case 'content':
                $this->content($value);
                $this->id = null;
                break;
            case 'file':
                $this->file = $value;
                $this->file($value);
                $this->id = null;
                break;
            default :
                $this->{$name} = $value;
                break;
        }
    }

    public function toArray() {
        return [
            'id' => $this->id(),
            'hash' => $this->hash,
            'width' => $this->width,
            'height' => $this->height,
            'type' => $this->format
        ];
    }

}
