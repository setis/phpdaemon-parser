<?php

namespace Grabber\Index\Data;

use \Grabber\Index\Storage;

class Status extends \Grabber\Index\AbstractData {

    use Traits\Source;

    public $type = Storage::data_status;

    /**
     *
     * @var int
     */
    protected $status;

    /**
     *
     * @var string bstr
     */
    protected $name;

    public function id() {
        if ($this->id === null) {
            $this->id = $this->status . self::plode . $this->source . self::plode . $this->name;
        }
        return $this->id;
    }

    public static function initilize() {
        Storage::$import[Storage::data_status] = ['status', 'name', 'source'];
    }

    public function __set($name, $value) {
        if ($this->wrapper === false) {
            $this->{$name} = $value;
            return false;
        }
        switch ($name) {
            case 'status':
                $this->status = $value;
                $this->id = null;
                break;
            case 'source':
                $this->source = $value;
                $this->source();
                $this->id = null;
                break;
            case 'name':
                $this->name = $value;
                $this->id = null;
                break;
            default :
                $this->{$name} = $value;
                break;
        }
    }

    public function toArray() {
        return [
            'id' => $this->id(),
            'status' => $this->status,
            'name' => $this->name,
            'source' => $this->source
        ];
    }

}
