<?php

namespace Grabber\Index\Data;

use \Grabber\Index\Storage;

class Long extends \Grabber\Index\AbstractData {

    use Traits\Lang;

    public $type = Storage::data_long;

    /**
     *
     * @var string
     */
    protected $value;

    /**
     *
     * @var string hash
     */
    public $hash;

    /**
     *
     * @var int
     */
    public $size;

    public function size() {
        $this->size = null;
        $this->size = strlen($this->value);
    }

    public static function initilize() {
        Storage::$import[Storage::data_long] = ['value', 'lang'];
        Storage::$lock[Storage::data_long] = 'hash';
    }

    public function id() {
        if ($this->id === null) {
            $this->id = $this->lang . self::plode . $this->hash . self::plode . $this->size;
        }
        return $this->id;
    }

    public function hash($value) {
        $this->hash = null;
        $this->hash = sha1($value);
        $this->lock('hash', true);
        $this->addTimer(setTimeout(function($event) {
                    if ($this->hash === null) {
                        $event->timeout();
                        return false;
                    }
                    $this->lock('hash', false);
                    $event->finish();
                }, 1e3));
    }

    public function __set($name, $value) {
        if ($this->wrapper === false) {
            $this->{$name} = $value;
            return false;
        }
        switch ($name) {
            case 'value':
                $this->value = $value;
                $this->size();
                $this->hash($value);
                $this->id = null;
                break;
            case 'lang':
                $this->lang = $value;
                $this->lang();
                $this->id = null;
                break;
            case 'size':
                $this->size = $value;
                $this->size();
                $this->id = null;
                break;
            default :
                $this->{$name} = $value;
                break;
        }
    }

    public function toArray() {
        return [
            'id' => $this->id(),
            'hash' => $this->hash,
            'lang' => $this->lang,
            'size' => $this->size,
            'value' => $this->value
        ];
    }

}
