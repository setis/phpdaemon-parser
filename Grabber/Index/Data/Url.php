<?php

namespace Grabber\Index\Data;

use \Grabber\Index\Storage;

class Url extends \Grabber\Index\AbstractData {

    public $type = Storage::data_url;

    /**
     *
     * @var string url
     */
    protected $url;
    /**
     *
     * @var string
     */
    public $host;

    /**
     *
     * @var string bsize
     */
    public $bsize;

    /**
     *
     * @var int
     */
    protected $size;

    /**
     *
     * @var string sha1 
     */
    public $hash;

    /**
     *
     * @var \GuzzleHttp\Event\CompleteEvent
     */
    protected $event;

    public static function initilize() {
        Storage::$import[Storage::data_url] = ['url', 'event'];
        Storage::$lock[Storage::data_url] = 'url';
    }

    public function event(\GuzzleHttp\Event\CompleteEvent $event) {
        $this->url = $event->getResponse()->getEffectiveUrl();
    }

    public function __set($name, $value) {
        if ($this->wrapper === false) {
            $this->{$name} = $value;
            return false;
        }
        switch ($name) {
            case 'url':
                $this->url = $value;
                $this->size = strlen($value);
                $this->url();
                $this->id = null;
                break;
            case 'event':
                $this->event($value);
                break;
            default :
                $this->{$name} = $value;
                break;
        }
    }

    public function url() {
        $this->lock('url', true);
        $this->host = $this->hash = null;
        $this->hash = sha1($this->url);
        $this->host = parse_url($this->url, PHP_URL_HOST);
        $this->addTimer(setTimeout(function($event) {
                    if ($this->hash === null && $this->host === null) {
                        $event->timeout();
                        return;
                    }
                    $this->lock('url', false);
                    $this->removeTimer($event->id);
                    $event->finish();
                }, 1e3));
    }

    public function id() {
        if ($this->id === null) {
            $this->id = $this->host . self::plode . $this->hash . self::plode . $this->size;
        }
        return $this->id;
    }

    public function toArray() {
        return [
            'id' => $this->id(),
            'url' => $this->url,
            'hash' => $this->hash,
            'size' => $this->size
        ];
    }

}
