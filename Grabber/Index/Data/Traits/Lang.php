<?php

namespace Grabber\Index\Data\Traits;

use \Grabber\Index\Storage;

trait Lang {

    /**
     *
     * @var int
     */
    protected $lang;


    public function lang() {
        if(is_int($this->lang) ||  ctype_digit($this->lang)){
            return;
        }
        $this->lang = Storage::lang($this->lang);
    }

}
