<?php

namespace Grabber\Index\Data\Traits;

use \Grabber\Index\Storage;

trait Source {

    /**
     *
     * @var int
     */
    public $source;

    public function source() {
        if (is_int($this->source) || ctype_digit($this->source)) {
            return;
        }
        $this->source = Storage::source($this->source);
    }

}
