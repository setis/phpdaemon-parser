<?php

namespace Grabber\Index\Data;

use \Grabber\Index\Storage;

class Year extends \Grabber\Index\AbstractData {

    use Traits\Source;

    public $type = Storage::data_year;

    /**
     *
     * @var int
     */
    protected $year;

    /**
     *
     * @var string bstr
     */
    protected $name;

    public function id() {
        if ($this->id === null) {
            $this->id = $this->source . self::plode . $this->name . self::plode . $this->year;
        }
        return $this->id;
    }

    public static function initilize() {
        Storage::$import[Storage::data_year] = ['year', 'name', 'source'];
    }

    public function __set($name, $value) {
        if ($this->wrapper === false) {
            $this->{$name} = $value;
            return false;
        }
        switch ($name) {
            case 'year':
                $this->year = $value;
                $this->id = null;
                break;
            case 'source':
                $this->source = $value;
                $this->source();
                $this->id = null;
                break;
            case 'name':
                $this->name = $value;
                $this->id = null;
                break;
            default :
                $this->{$name} = $value;
                break;
        }
    }

    public function toArray() {
        return [
            'id' => $this->id(),
            'year' => $this->year,
            'name' => $this->name,
            'source' => $this->source
        ];
    }

}
