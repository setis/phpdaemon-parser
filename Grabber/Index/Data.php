<?php

namespace Grabber\Index;

class Data {

    public static function import($self, $type, array &$data) {
        foreach (Storage::$import[$type] as &$var) {
            if (isset($data[$var])) {
                $self->{$var} = $data[$var];
            }
        }
    }

    public static function export($self, $type) {
        $arr = new \ArrayObject();
        $arr->setFlags(\ArrayObject::ARRAY_AS_PROPS);
        foreach (Storage::$export[$type] as $k => $v) {
            $arr->{$k} = $self->{$v};
            console(__METHOD__, $self->{$v}, $k);
        }
        console(__METHOD__, $arr);
        return $arr;
    }

    public static function lock($self, $name, $lock) {
        $var = AbstractData::prefix_lock . $name;
        $self->{$var} = $lock;
    }

    public static function locked($self, $type) {
        foreach (Storage::$lock[$type] as $var) {
//            console(__METHOD__, $var, (!isset($self->{$var}) || $self->{$var} === true));
            if (!isset($self->{$var}) || $self->{$var} === true) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param binary $value
     * @param int $count
     * @return binary
     */
    public static function int($value, $count = 5) {
        return substr(str_repeat('0', $count), 0, $count - strlen($value)) . $value;
    }

}
