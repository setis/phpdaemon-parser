<?php

namespace Grabber\Index\Collector;

class Hand {

    /**
     *
     * @var boolean
     */
    protected $lock = true;
    /**
     *
     * @var mixed
     */
    public $storage;
//    /**
//     *
//     * @var callable
//     */
//    public $cb;
//
//    public function __construct($cb = null) {
//        if ($cb !== null) {
//            $this->cb = $cb;
//        }
//    }

    /**
     * 
     * @return boolean
     */
    public function locked() {
        return $this->lock;
    }

    /**
     * 
     * @return this|self|static
     */
    public function lock() {
        $this->lock = true;
        return $this;
    }

    /**
     * 
     * @return this|self|static
     */
    public function unlock() {
        $this->lock = false;
        return $this;
    }

}
