<?php

namespace Grabber\Index\Collector;

class One {

    /**
     *
     * @var \SplObjectStorage
     */
    public $collection;

    /**
     *
     * @var \SplObjectStorage
     */
    public $storage;

    /**
     *
     * @var boolean
     */
    public $lock = false;

    const timer_add = 0, timer_collection = 1;

    /**
     *
     * @var int
     */
    public $_overall_id;

    /**
     *
     * @var int
     */
    public $_each_id;

    /**
     *
     * @var int таймаут для прерывания после добавления объекта 
     */
    public $_live_add_timeout = 5e6;

    /**
     *
     * @var int таймаут для прерывания функций overall,each
     */
    public $_live_timeout = 5e6;

    /**
     *
     * @var boolean перезапускать таймер после добавления объекта
     */
    public $_live_reload = true;

    /**
     *
     * @var int индификатор таймера для функций add,addAll
     */
    public $_live_add_id;

    /**
     *
     * @var int индификатор таймера для функций overall,each
     */
    public $_live_id;

    /**
     *
     * @var \Exception
     */
    public $e;

    public function __construct(array $timeout = null, \SplObjectStorage &$storage = null) {
        if ($timeout !== null) {
            call_user_func_array([$this, 'setLive'], $timeout);
        }
        if ($storage === null) {
            $this->storage = new \SplObjectStorage();
        } else {
            $this->storage = $storage;
            if ($this->storage->count() !== 0) {
                $this->onLive(self::timer_add);
                $this->lock = true;
            }
        }
        $this->e = new \Exception();
    }

    public function __destruct() {
        if ($this->_each_id !== null) {
            clearTimeout($this->_each_id);
            $this->_each_id = null;
        }
        if ($this->_overall_id !== null) {
            clearTimeout($this->_each_id);
            $this->_overall_id = null;
        }
        foreach ($this->storage as $obj) {
            unset($this->storage[$obj]);
        }
        if ($this->_live_add_id !== null) {
            clearTimeout($this->_live_add_id);
            $this->_live_add_id = null;
        }
        if ($this->_live_id !== null) {
            clearTimeout($this->_live_id);
            $this->_live_id = null;
        }
    }

    public function setLive($add = 5e6, $timeout = 5e6, $reload = true) {
        if ($add !== null) {
            $this->_live_add_timeout = $add;
        }
        if ($timeout !== null) {
            $this->_live_timeout = $timeout;
        }
        if ($reload !== null) {
            $this->_live_reload = $reload;
        }
    }

    public function onLive($type) {
        switch ($type) {
            case self::timer_add:
                if ($this->_live_add_id === null) {
                    $this->_live_add_id = setTimeout(function($event) {
                        $e = $this->e;
                        $this->_live_add_id = null;
                        unset($this);
                        $event->finish();
                        throw $e;
//                        throw new \Exception('max timeout add time:'.$this->_live_add_timeout, 0, $e);
                    }, $this->_live_add_timeout);
                } elseif ($this->_live_reload === true) {
                    clearTimeout($this->_live_add_id);
                    $this->_live_add_id = setTimeout(function($event) {
                        $e = $this->e;
                        $this->_live_add_id = null;
                        unset($this);
                        $event->finish();
                        throw $e;
//                        throw new \Exception('max timeout add time:'.$this->_live_add_timeout, 0, $e);
                    }, $this->_live_add_timeout);
                }
                break;
            case self::timer_collection:
                if ($this->_live_id === null)
                    $this->_live_id = setTimeout(function($event) {
                        $e = $this->e;
                        $this->_live_id = null;
                        unset($this);
                        $event->finish();
                        throw $e;
//                        throw new \Exception('max timeout live time:'.$this->_live_add_timeout, 0, $e);
                    }, $this->_live_timeout);
                break;
            default:
                throw new \Exception('not found timer key:' . $type);
        }
    }

    public function offLive($type) {
        switch ($type) {
            case self::timer_add:
                if ($this->_live_add_id !== null) {
                    clearTimeout($this->_live_add_id);
                    $this->_live_add_id = null;
                }
                break;
            case self::timer_collection:
                if ($this->_live_id !== null) {
                    clearTimeout($this->_live_id);
                    $this->_live_id = null;
                }
                break;
            default:
                throw new \Exception('not found timer key:' . $type);
        }
    }

    /**
     * 
     * @param type $object
     * @return this|static|self
     */
    public function add($object) {
        $this->lock = true;
        $this->onLive(self::timer_add);
        $this->storage->attach($object);
        return $this;
    }

    /**
     * 
     * @param type $object
     * @return this|static|self
     */
    public function remove($object) {
        if ($this->storage->contains($object)) {
            $this->storage->detach($object);
        }
        return $this;
    }

    /**
     * 
     * @return this|self|static
     */
    public function addAll() {
        $this->lock = true;
        $this->onLive(self::timer_add);
        foreach (func_get_args() as $object) {
            $this->storage->attach($object);
        }
        return $this;
    }

    /**
     * 
     * @return boolean;
     */
    public function locked() {
        return $this->lock;
    }

    public function overall($cb) {
        if (!is_callable($cb)) {
            throw new \InvalidArgumentException('not argument callable');
        }
        if ($this->lock === false) {
            call_user_func($cb, $this->storage);
            return;
        }
        if ($this->_overall_id !== null) {
            throw new \Exception('the timer is already running func.overall');
        }
        $this->offLive(self::timer_add);
        $this->collection = new \SplObjectStorage();
        $this->collection->addAll($this->storage);
        $this->onLive(self::timer_collection);
        $this->_overall_id = setTimeout(function($event)use($cb) {
            foreach ($this->collection as $obj) {
                if ($obj->locked() === false) {
                    $this->collection->detach($obj);
//                    \console(__FUNCTION__ . ':' . __LINE__, $this->collection->count(), $obj->toArray());
                }
            }
            if ($this->collection->count() === 0) {
                $this->offLive(self::timer_collection);
                call_user_func($cb, $this->storage, $this);
                $this->lock = false;
                $event->finish();
                $this->_overall_id = null;
                return;
            }
            $event->timeout();
        }, 1e3);
    }

    public function each($cb) {
        if (!is_callable($cb)) {
            throw new \InvalidArgumentException('not argument callable');
        }
        if ($this->lock === false) {
            foreach ($this->storage as $object) {
                call_user_func($cb, $object);
            }
            return;
        }
        if ($this->_each_id !== null) {
            throw new \Exception('the timer is already running func.each');
        }
        $this->offLive(self::timer_add);
        $this->collection = new \SplObjectStorage();
        $this->collection->addAll($this->storage);
        $this->onLive(self::timer_collection);
        $this->_each_id = setTimeout(function($event)use($cb) {
            foreach ($this->collection as $object) {
                $object = $this->collection->current();
                if ($object->locked() === false) {
                    call_user_func($cb, $object, $this);
                    $this->collection->detach($object);
                }
            }
            if ($this->collection->count() === 0) {
                $this->offLive(self::timer_collection);
                $this->lock = false;
                $event->finish();
                $this->_each_id = null;
                return;
            }
            $event->timeout();
        }, 1e3);
    }

}
