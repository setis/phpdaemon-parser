<?php

namespace Grabber\Index\Collector;

class Multi {

    /**
     *
     * @var \SplObjectStorage
     */
    public $storage;

    /**
     *
     * @var \SplObjectStorage
     */
    public $collection;

    /**
     *
     * @var callable
     */
    public $cb;

    /**
     *
     * @var boolean
     */
    protected $lock = true;

    public function __construct(\SplObjectStorage $storage = null, $cb = null) {
        $this->storage = new \SplObjectStorage();
        if ($storage !== null) {
            $this->storage->addAll($storage);
        }
        $this->cb = $cb;
    }

    public function add($object, $label) {
        $this->lock = true;
        $this->storage->attach($object, $label);
        return $this;
    }

    public function addAll(array $storage) {
        $this->lock = true;
        foreach ($storage as $label => &$object) {
            $this->storage->attach($object, $label);
        }
        return $this;
    }

    public function locked() {
        return $this->lock;
    }

    public function overall($cb = null) {
        if ($cb === null) {
            $cb = $this->cb;
        }
        if ($this->lock === false) {
            call_user_func($cb, $this->storage, $this);
            return;
        }
        $this->collection = new \SplObjectStorage($this->storage);
        setTimeout(function($event)use($cb) {
            foreach ($this->collection as $object) {
                if ($object->locked() === false) {
                    $this->collection->detach($object);
                }
            }
            if ($this->collection->count() === 0) {
                call_user_func($cb, $this->storage, $this);
                $this->lock = false;
                $event->finish();
                return;
            }
            $event->timeout();
        }, 1e3);
    }

    public function each($cb = null) {
        if ($cb === null) {
            $cb = $this->cb;
        }
        if ($this->lock === false) {
            foreach ($this->storage as $label => $object) {
                call_user_func($cb, $object, $label, $this);
            }
            return;
        }
        $this->collection = new \SplObjectStorage($this->storage);
        $this->stack = new \SplStack();
        setTimeout(function($event)use($cb) {
            foreach ($this->collection as $label => $object) {
                if ($object->locked() === false) {
                    $this->collection->detach($object);
                    $this->stack->push($label);
                    call_user_func($cb, $object, $label, $this);
                }
            }
            if ($this->collection->count() === 0) {
                $this->lock = false;
                $event->finish();
                return;
            }
            $event->timeout();
        }, 1e3);
    }

    public function combine($each, $overall) {
        if ($this->lock === false) {
            foreach ($this->storage as $label => $object) {
                call_user_func($each, $object, $label, $this);
            }
            call_user_func($overall, $this->storage, $this);
            return;
        }
        $this->collection = new \SplObjectStorage($this->storage);
        setTimeout(function($event)use($each, $overall) {
            foreach ($this->collection as $label => $object) {
                if ($object->locked() === false) {
                    $this->collection->detach($object);
                    call_user_func($each, $object, $label, $this);
                }
            }
            if ($this->collection->count() === 0) {
                call_user_func($overall, $this->storage, $this);
                $this->lock = false;
                $event->finish();
                return;
            }
            $event->timeout();
        }, 1e3);
    }

}
