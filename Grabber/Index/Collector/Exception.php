<?php

namespace Grabber\Index\Collector;

class Exception {

    /**
     *
     * @var \SplObjectStorage
     */
    protected $exceptions;

    public function __construct() {
        $this->exceptions = new \SplObjectStorage();
    }

    /**
     * 
     * @return $this
     */
    public function add() {
        if (func_num_args() === 0) {
            return $this;
        }
        foreach (func_get_args() as $e) {
            if (!$this->exceptions->contains($e) && $e instanceof \Exception) {
                $this->exceptions->attach($e);
            }
        }
        return $this;
    }

    /**
     * 
     * @return $this
     */
    public function del() {
        if (func_num_args() === 0) {
            return $this;
        }
        foreach (func_get_args() as $e) {
            if ($this->exceptions->contains($e) && $e instanceof \Exception) {
                $this->exceptions->detach($e);
            }
        }
        return $this;
    }

    public function getMessage($string = true) {
        $result = [];
        foreach ($this->exceptions as $e) {
            $result[] = $e->getMessage();
        }
        if ($string) {
            return implode("\n", $result);
        }
        return $result;
    }

    public function getCode($string = true) {
        $result = [];
        foreach ($this->exceptions as $e) {
            $result[] = $e->getCode();
        }
        if ($string) {
            return implode(",", $result);
        }
        return $result;
    }

    public function getLine($string = true) {
        $result = [];
        foreach ($this->exceptions as $e) {
            $result[] = $e->getLine();
        }
        if ($string) {
            return implode(",", $result);
        }
        return $result;
    }

    public function getTrace() {
        $result = [];
        foreach ($this->exceptions as $e) {
            $result[] = $e->getTrace();
        }
        return $result;
    }

    public function getPrevious() {
        $result = [];
        foreach ($this->exceptions as $e) {
            $result[] = $e->getTrace();
        }
        return $result;
    }

    public function getTraceAsString($string = true) {
        $result = [];
        foreach ($this->exceptions as $e) {
            $result[] = $e->getTraceAsString();
        }
        if ($string) {
            return implode("\n", $result);
        }
        return $result;
    }

    public function __toString() {
        $result = [];
        foreach ($this->exceptions as $e) {
            $result[] = $e->__toString();
        }
        return implode("\n", $result);
    }

}
