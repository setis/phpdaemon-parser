<?php

namespace Grabber\Index\Collector;

class Relations {

    /**
     *
     * @var \SplObjectStorage
     */
    public $src_storage;

    /**
     *
     * @var \SplObjectStorage
     */
    public $dst_storage;

    /**
     *
     * @var \SplObjectStorage
     */
    public $collection;

    /**
     *
     * @var \SplObjectStorage
     */
    public $temp;

    /**
     *
     * @var callable
     */
    public $cb;

    /**
     *
     * @var boolean
     */
    protected $lock = true;
    /**
     * <pre><code>
     *  $name_relations_author = new self($names, $authors, function($names, $authors)use($en, $source) {
            foreach ($names as $name) {
                if ($name->lang === $en) {
                    break;
                }
            }
            $result = [];
            foreach ($authors as $author) {
                $iname = $name->id();
                $iauthor = $author->id();
                $result[] = [
                    'id' => $iname . $iauthor,
                    'src' => $iname,
                    'dst' => $iauthor,
                ];
                $source[] = [
                    'id' => Storage::binary(Storage::type_lang, $source) . $iname . $iauthor,
                    'source' => $source,
                    'relation' => $iname . $iauthor,
                    'time' => time()
                ];
            }
        });
        $name_relations_author->collection();
     * </code></code>
     * @param \SplObjectStorage $src_storage
     * @param \SplObjectStorage $dst_storage
     * @param \Grabber\Index\Collector\callable $cb
     */
    public function __construct(\SplObjectStorage $src_storage, \SplObjectStorage $dst_storage, callable $cb) {
        $this->src_storage = new \SplObjectStorage($src_storage);
        $this->dst_storage = new \SplObjectStorage($dst_storage);
        $this->cb = $cb;
    }

    public function locked() {
        return $this->lock;
    }

    public function collection($cb = null) {
        if ($cb === null) {
            $cb = $this->cb;
        }
        $this->collection = new \SplObjectStorage();
        $this->temp = new \SplObjectStorage();
        $this->temp->addAll($this->src_storage);
        $this->temp->addAll($this->dst_storage);
        setTimeout(function($event)use($cb) {
            foreach ($this->temp as $object) {
                if ($object->locked() === false) {
                    $this->collection->attach($object);
                    $this->temp->detach($object);
                }
            }
            if ($this->temp->count() === 0) {
                call_user_func($cb, $this->src_storage, $this->dst_storage);
                $this->lock = false;
                $event->finish();
                return;
            }
            $event->timeout();
        }, 1e3);
    }

    public function uniq($cb = null) {
        if ($cb === null) {
            $cb = $this->cb;
        }
        $this->collection = new \SplObjectStorage();
        $this->temp = new \SplObjectStorage();
        $this->lock = true;
        foreach ($this->src_storage as $object) {
            if ($this->temp->contains($object) === false) {
                $this->temp->attach($object);
            }
        }
        foreach ($this->dst_storage as $object) {
            if ($this->temp->contains($object) === false) {
                $this->temp->attach($object);
            }
        }
        setTimeout(function($event)use($cb) {
            foreach ($this->temp as $object) {
                if ($object->locked() === false) {
                    $this->collection->attach($object);
                    $this->temp->detach($object);
                }
            }
            if ($this->temp->count() === 0) {
                call_user_func($cb, $this->src_storage,  $this->dst_storage);
                $this->lock = false;
                $event->finish();
                return;
            }
            $event->timeout();
        }, 1e3);
    }

}
