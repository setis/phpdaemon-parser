<?php

namespace Grabber\Index;

use Utils\int2bstr;

class Storage {

    /**
     * type
     */
    const type_lang = 0;
    const type_source = 1;
    const type_status = 2;
    const type_chapter = 3;
    const type_page = 4;
    const type_image = 5;
    const type_resource = 6;
    const type_year = 7;
    const type_short = 8;
    const type_long = 9;
    const type_url = 10;
    const type_size_name = 11;
    const type_size_url = 12;
    const type_image_width = 13;
    const type_image_height = 14;

    /**
     * data
     */
    const data_short = 0;
    const data_long = 1;
    const data_status = 2;
    const data_year = 3;
    const data_image_chapter = 4;
    const data_image_property = 5;
    const data_page = 6;
    const data_url = 7;
    const data_chapter = 8;

    public static $lang;
    public static $source;
    public static $resource;
    public static $status;
    public static $import;
    public static $lock;
    public static $values;
    public static $bin;
    public static $length;

    public static function load(array $data, $type) {
        switch ($type) {
            case self::type_lang:
//                $bit = self::length(self::type_lang);
                self::$lang = array_combine(array_values($data), array_keys($data));
//                foreach (self::$lang as $v) {
//                    new int2bstr($v, $bit, function($bin)use($v) {
//                        Storage::$bin[self::type_lang][$v] = $bin;
//                    });
//                }
                break;
            case self::type_source;
//                $bit = self::length(self::type_source);
                self::$source = array_combine(array_values($data), array_keys($data));
//                foreach (self::$source as $v) {
//                    new int2bstr($v, $bit, function($bin)use($v) {
//                        Storage::$bin[self::type_source][$v] = $bin;
//                    });
//                }
                break;
            case self::type_resource;
//                $bit = self::length(self::type_resource);
                self::$resource = array_combine(array_values($data), array_keys($data));
//                foreach (self::$resource as $v) {
//                    new int2bstr($v, $bit, function($bin)use($v) {
//                        Storage::$bin[self::type_resource][$v] = $bin;
//                    });
//                }
                break;
            case self::type_status;
                $bit = self::length(self::type_status);
                self::$status = array_combine(array_values($data), array_keys($data));
                foreach (self::$status as $v) {
                    new int2bstr($v, $bit, function($bin)use($v) {
                        Storage::$bin[self::type_status][$v] = $bin;
                    });
                }
                break;
            case self::type_chapter:
                $bit = self::length(self::type_chapter);
                foreach (range($data[0], $data[1]) as $v) {
                    new int2bstr($v, $bit, function($bin)use($v) {
                        Storage::$bin[self::type_chapter][$v] = $bin;
                    });
                }
                break;
            case self::type_page:
                $bit = self::length(self::type_page);
                foreach (range($data[0], $data[1]) as $v) {
                    new int2bstr($v, $bit, function($bin)use($v) {
                        Storage::$bin[self::type_page][$v] = $bin;
                    });
                }
                break;
            case self::type_image:
                $bit = self::length(self::type_image);
                foreach (range($data[0], $data[1]) as $v) {
                    new int2bstr($v, $bit, function($bin)use($v) {
                        Storage::$bin[self::type_image][$v] = $bin;
                    });
                }
                break;
            case self::type_size_name:
                $bit = self::length(self::type_size_name);
                foreach (range($data[0], $data[1]) as $v) {
                    new int2bstr($v, $bit, function($bin)use($v) {
                        Storage::$bin[self::type_size_name][$v] = $bin;
                    });
                }
                break;
            default:
                throw new \Grabber\Exception\key('not found type:' . $type);
        }
    }

    public static function inlock($type, $name) {
        return (in_array($name, self::$lock[$type]));
    }

    /**
     * 
     * @param string $lang
     * @return int
     * @throws Exception
     */
    public static function lang($lang) {
        if (!isset(self::$lang[$lang])) {
            throw new \Grabber\Exception\key('not found key for lang:' . $lang . ' json-> ' . json_decode(self::$lang, true), \Grabber\Consts\Error::index_lang);
        }
        return self::$lang[$lang];
    }

    /**
     * 
     * @param string $source
     * @return int
     * @throws Exception
     */
    public static function source($source) {
        if (!isset(self::$source[$source])) {
            throw new \Grabber\Exception\key('not found key for source:' . $source, \Grabber\Consts\Error::index_source);
        }
        return self::$source[$source];
    }

    /**
     * 
     * @param string $source
     * @return int
     * @throws Exception
     */
    public static function resource($source) {
        if (!isset(self::$resource[$source])) {
            throw new \Grabber\Exception\key('not found key for resource:' . $source, \Grabber\Consts\Error::index_resource);
        }
        return self::$resource[$source];
    }

    /**
     * 
     * @param string $type
     * @return int
     * @throws Exception
     */
    public static function length($type) {
        if (!isset(self::$length[$type])) {
            throw new \Grabber\Exception\key('not found key for legth:' . $type, \Grabber\Consts\Error::count);
        }
        return self::$length[$type];
    }

    public static function bin($type, $key) {
        if (!isset(self::$bin[$type][$key])) {
            $e = new \Grabber\Exception\key('not found key ' . $key . ' for bin:' . $type, \Grabber\Consts\Error::bin);
            \console\msg('exception-key', $type, $key, dumpException($e), self::$bin[$type]);
            throw $e;
        }
        return self::$bin[$type][$key];
    }

}
