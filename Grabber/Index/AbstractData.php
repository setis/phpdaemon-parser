<?php

namespace Grabber\Index;

abstract class AbstractData {

    /**
     *
     * @var string hex
     */
    public $id;

    /**
     *
     * @var boolean
     */
    public $wrapper = true;

    /**
     *
     * @var int
     */
    public $type;

    const prefix_lock = '_lock_';
    const plode = '|';

    public $timer = [];

    abstract public function id();

    public function __toString() {
        return $this->id();
    }

    public function __clone() {
        if ($this->wrapper === true) {
            $this->wrapper = false;
            $this->clearTimer();
            foreach (Storage::$import as $var) {
                $this->{$var} = null;
            }
            $this->wrapper = true;
        } else {
            foreach (Storage::$import as $var) {
                $this->{$var} = null;
            }
        }
    }

    public function __destruct() {
        $this->clearTimer();
    }

    public function addTimer($id) {
        $this->timer[] = $id;
    }

    public function removeTimer($id) {
        $k = array_search($id, $this->timer);
        if ($k) {
            unset($this->timer[$k]);
        }
    }

    public function clearTimer() {
        foreach ($this->timer as $id) {
            clearTimeout($id);
        }
        $this->timer = null;
    }

    public function __construct(array $data = null, $wrapper = true) {
        if ($data === null) {
            return;
        }
        $this->init();
        $this->import($data, $wrapper);
    }

    public function init() {
        
    }

    public function import(array &$data = null, $wrapper = true) {
        if ($wrapper === true) {
            Data::import($this, $this->type, $data);
        } else {
            if (isset($data['wrapper'])) {
                unset($data['wrapper']);
            }
            $this->wrapper = false;
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
            $this->wrapper = true;
        }
    }

    public function __get($name) {
        switch ($name) {
            default:
                return $this->{$name};
        }
    }

    public function lock($name, $lock) {
//        \console\console(__METHOD__, $name, $lock, $this->locked());
        $var = self::prefix_lock . $name;
        $this->{$var} = $lock;
    }

    public function locked() {
        if (!isset(Storage::$lock[$this->type])) {
            return false;
        }
        if (is_string(Storage::$lock[$this->type])) {
            $var = self::prefix_lock . Storage::$lock[$this->type];
//            console(__METHOD__, $var, (!isset($this->{$var}) || $this->{$var} === true),$this->{$var});
            if (!isset($this->{$var}) || $this->{$var} === true) {
                return true;
            }
        } else {
            foreach (Storage::$lock[$this->type] as $var) {
                $var = self::prefix_lock . $var;
//            console(__METHOD__, $var, (!isset($this->{$var}) || $this->{$var} === true),$this->{$var});
                if (!isset($this->{$var}) || $this->{$var} === true) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 
     * @return array
     */
    abstract public function toArray();
}
