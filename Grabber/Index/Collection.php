<?php

namespace Grabber\Index;

class Collection {

    /**
     *
     * @var \SplObjectStorage
     */
    public $collection;

    /**
     *
     * @var \SplObjectStorage
     */
    public $storage;

    public function __construct() {
        $this->storage = new \SplObjectStorage();
    }

    /**
     * 
     * @param type $object
     * @return this|static|self
     */
    public function add(&$object) {
        if ($this->storage->contains($object) === false) {
            $this->storage->attach($object);
        }
        return $this;
    }

    /**
     * 
     * @param type $object
     * @return this|static|self
     */
    public function remove($object) {
        if ($this->storage->contains($object)) {
            $this->storage->detach($object);
        }
        return $this;
    }

    public function overall($cb) {
        $this->collection = new \SplObjectStorage();
        $this->collection->addAll($this->storage);
        setTimeout(function($event)use($cb) {
            foreach ($this->collection as $obj) {
                if ($obj->locked()) {
                    $this->stack->push($obj);
                    $this->collection->detach($obj);
                }
            }
            if ($this->collection->count() === 0) {
                call_user_func($cb, $this->stack);
                $event->finish();
                return;
            }
            $event->timeout();
        }, 1e3);
    }

    public function each($cb) {
        $this->collection = new \SplObjectStorage();
        $this->collection->addAll($this->storage);
        setTimeout(function($event)use($cb) {
            foreach ($this->collection as $object) {
                $object = $this->collection->current();
                if ($object->locked() === false) {
                    call_user_func($cb, $object);
                    $this->collection->detach($object);
                }
            }
            if ($this->collection->count() === 0) {
                $event->finish();
                return;
            }
            $event->timeout();
        }, 1e3);
    }

}
