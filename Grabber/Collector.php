<?php

namespace Grabber;

class Collector {

    /**
     *
     * @var \PHPDaemon\Clients\Mongo\Collection
     */
    public $mongo;

    /**
     *
     * @var string
     */
    public $collection = 'manga.keys';
    public $table;
    public $cursor;

    public function __construct() {
        $this->mongo = \PHPDaemon\Clients\Mongo\Pool::getInstance(['maxconnperserv' => 100]);
    }

    public function relations() {
        $model = \Grabber\Model\register::getInstance();
        $collection = $this->mongo->{'manga.keys'};
        $model->setPrefix('name');
        $model->Fetch($model->Builder()->select('*')->build(), function($result)use($collection) {
            foreach ($result as $values) {
                $collection->findOne(function($result)use($values) {
                    console(__FILE__, $result);
                    if ($result === false) {
                        $this->mongo->insert([
                            'name' => [
                                $values['src_lang'] => [$values['src_id']],
                                $values['dst_lang'] => [$values['dst_id']]
                            ]
                        ]);
                    } else {
                        $data = $result;
                        $data['name'][$values['dst_lang']][] = $values['dst_id'];
                        $collection->updateOne($result, $data);
                    }
                }, [
                    'name' => [
                        $values['src_lang'] => [$values['src_id']]
                    ]
                ]);
            }
        });
    }

    public function collections() {
        $collection = $this->mongo->{'manga.data'};
        $list = [
            'name',''
        ];
    }

}
