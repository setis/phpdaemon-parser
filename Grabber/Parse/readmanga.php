<?php

namespace Grabber\Parse;

use phpQuery;

class readmanga {

    const regex1 = "/.*?(\d+)\s+-\s+(\d+)\s+(.*?)$/mi";
    const regex2 = "/(\d+)\s+-\s+(\d+)/mi";
    const regex3 = "/(\d+)\s+Экстра(.*?)$/mi";

    /**
     * 
     * @param string $html
     * @return array
     */
    public static function lists($html) {
        $handler = phpQuery::newDocumentHTML($html);
        $array = $handler->find('div.leftContent > div > div');
        $result = [];
        foreach ($array as $value) {
            $value = pq($value);
            $result[] = [
                'link' => $value->find('div.img > a')->attr('href'),
                'name' => [
                    'ru' => $value->find('div.desc > h4')->text(),
                    'en' => $value->find('div.desc > h3')->text(),
                ]
            ];
        }
        return $result;
    }

    /**
     * 
     * @param string $html
     * @return array
     */
    public static function update($html) {
        $handler = phpQuery::newDocumentHTML($html);
        $result = [];
        foreach ($handler->find('table.cTable.newChapters tr')as $tr) {
            $tr = pq($tr);
            $a = $tr->find('a');
            $status = preg_match_all(self::regex, trim($a->text()), $obj);
            if ($status) {
                $result[] = [
                    'name' => (isset($obj[4])) ? $obj[4] : null,
                    'volume' => $obj[1],
                    'chapter' => $obj[2],
                    'link' => $a->attr('href'),
                    'date' => trim($tr->find('td')->eq(2)->text()) . '/' . date('Y')
                ];
            }
        }
        return $result;
    }

    /**
     * 
     * @param string $html
     * @return array
     */
    public static function content($html) {
        $handler = phpQuery::newDocumentHTML($html);
        $result['name'] = [
            'ru' => $handler->find('span.name')->text(),
            'en' => $handler->find('span.eng-name')->text()
        ];
        $dsc[] = $handler->find('div.leftContent.manga-page > div.manga-description > p:nth-child(1)')->text();
        $dsc[] = $handler->find('div.leftContent.manga-page > div.manga-description')->text();
        $dsc[] = $handler->find('div.leftContent.manga-page > div.manga-description > p')->text();
        $text = '';
        foreach ($handler->find('div.leftContent.manga-page > div.manga-description > p')as $p) {
            $p = pq($p);
            if ($p->attr() === 'text-align: justify;' || $p->attr() === '') {
                $text.= pq($p)->text();
            }
        }
        $dsc[] = $text;
        $text = '';
        foreach ($handler->find('div.leftContent.manga-page > div.manga-description > p > span')as $p) {
            $text.= pq($p)->text();
        }
        $dsc[] = $text;
        $dsc[] = $handler->find("div.leftContent > div.manga-description > p")->text();
        $s = $d = array_map('strlen',  $dsc);
        $i = array_search(rsort($s, SORT_NUMERIC | SORT_NATURAL)[0], $d);
        $result['dsc'][] = $dsc;
        $result['dsc'][] = $s;
        $result['dsc'][] = $d;
        $result['dsc'][] = $i;
        $result['dsc'][] = $dsc[$i];
        $result['description'] = &$dsc[$i];
        $result['description'] = $handler->find("div.leftContent > div.manga-description > p")->text();
        foreach ($handler->find('div.rightContent > div:nth-child(6) > ol > li') as $i => $li) {
            $a = pq($li)->find('a');
            $result['like'][] = [
                'name' => trim($a->eq(0)->text()),
                'link' => trim($a->eq(0)->attr('href')),
            ];
        }
        foreach ($handler->find('div.rightContent > div:nth-child(4) > ul > li > a') as $i => $a) {
            $a = pq($a);
            $result['related.anime'][] = [
                'name' => $a->text(),
                'link' => $a->attr('href'),
            ];
        }
        foreach ($handler->find('div.leftContent.manga-page > div.subject-cower a.full-list-pic') as $i => $a) {
            $result['cover'][] = pq($a)->attr('href');
        }
        foreach ($handler->find('div.leftContent.manga-page > div.subject-cower div div a') as $i => $a) {
            $result['cover'][] = pq($a)->attr('href');
        }
        foreach ($handler->find('div.rightContent > div:nth-child(4) > ol > li') as $li) {
            $a = pq($li)->find('a');
            $result['related.manga'][] = [
                'name' => trim($a->eq(0)->text()),
                'link' => trim($a->eq(0)->attr('href')),
            ];
        }

        foreach ($handler->find('head meta') as $meta) {
            $obj = pq($meta);
            if ($obj->attr('name') == 'description') {
                $result['meta.description'] = $obj->attr('content');
                break;
            }
        }
        $content = $handler->find('div.subject-meta');
        foreach ($content->find('span.elem_year a') as $a) {
            $result['year'][] = trim(pq($a)->text());
        }
        foreach ($content->find('span.elem_author a') as $a) {
            $a = pq($a);
            $result['author'][] = [
                'name' => trim($a->text()),
                'link' => trim($a->attr('href')),
            ];
        }

        foreach ($content->find('span.elem_genre a') as $el) {
            $el = pq($el);
            $result['genre'][] = [
                'link' => $el->attr('href'),
                'name' => $el->text()
            ];
        }

        foreach ($content->find('p:nth-child(5) > span.elem_translator > a.element-link') as $a) {
            $a = pq($a);
            $result['translator'][] = [
                'link' => $a->attr('href'),
                'name' => $a->text()
            ];
        }
        $content->find('p:nth-child(4) b')->remove();
        $result['status'] = trim($content->find('p:nth-child(4)')->text());
        $action = $handler->find('div.subject-actions');
        $result['read.start'] = $action->find('span.read-first a')->attr('href');
        $result['read.end'] = $action->find('span.read-last a')->attr('href');
        foreach ($handler->find('#chapters-list a') as $tr) {
            $a = pq($tr);
            $a->find('sup')->remove();
            $status = preg_match(self::regex1, trim($a->text()), $obj);
            if ($status === 0) {
                $status = preg_match(self::regex2, trim($a->text()), $obj);
            }
            if ($status === 0) {
                $status = preg_match(self::regex3, trim($a->text()), $obj);
                $result['extra'][] = [
                    'name' => ((!isset($obj[2])) ? null : trim($obj[2])),
                    'volume' => (int) $obj[1],
                    'link' => $a->attr('href'),
                ];
//                \PHPDaemon\Core\Daemon::log(__METHOD__, trim($a->text()), [
//                    'name' => ((!isset($obj[2])) ? null : trim($obj[2])),
//                    'volume' => (int) $obj[1],
//                    'link' => $a->attr('href'),
//                ]);
            } else {
                $result['chapters'][] = [
                    'name' => ((!isset($obj[3])) ? null : trim($obj[3])),
                    'volume' => (int) $obj[1],
                    'chapter' => (int) $obj[2],
                    'link' => $a->attr('href'),
                ];
            }
        }
        return $result;
    }

    /**
     * 
     * @param string $html
     * @return array
     */
    public static function chapter_list($html) {
        $handler = phpQuery::newDocumentHTML($html);
        $result = [];
        foreach ($handler->find('#chapterSelectorSelect option')as $option) {
            $option = pq($option);

            $status = preg_match(self::regex1, trim($option->text()), $obj);
            if ($status === 0) {
                $status = preg_match(self::regex2, trim($option->text()), $obj);
            }
            if ($status === 0) {
                $status = preg_match(self::regex3, trim($option->text()), $obj);
            }
//            \console\msg('preg', $status, trim($option->text()), $obj);
            $result[] = [
                'name' => (isset($obj[3])) ? trim($obj[3]) : null,
                'volume' => (int) $obj[1],
                'chapter' => (int) $obj[2],
                'link' => $option->val(),
            ];
        }
        return $result;
    }

    /**
     * 
     * @param string $html
     * @return array
     */
    public static function chapter_image($html) {
        $handler = phpQuery::newDocumentHTML($html);
        $js = $handler->find('div.pageBlock.reader-bottom script')->html();
        preg_match("/var pictures = (.*?);/ism", $js, $imgObj);
//        \console(__METHOD__, $js, $imgObj);
        $JsArray = str_replace(['url:', 'w:', 'h:'], ['"url":', '"w":', '"h":'], $imgObj[1]);
        foreach (json_decode($JsArray) as $value) {
            $result[] = $value->url;
        }
        return $result;
    }

}
