<?php

namespace Grabber;

use \GuzzleHttp\Event\CompleteEvent,
    \GuzzleHttp\Event\ErrorEvent,
    \GuzzleHttp\Client,
    \Symfony\Component\EventDispatcher\EventDispatcher,
    \Symfony\Component\EventDispatcher\GenericEvent;
use PHPDaemon\Core\Daemon,
    PHPDaemon\Core\Debug,
    PHPDaemon\Core\AppInstance;
use \GuzzleHttp\Pool;

class Kernel {

    use \Traits\getInstance;

    /**
     *
     * @var \BootStrap\Events
     */
    public $Events;

    /**
     *
     * @var \Symfony\Component\EventDispatcher\EventDispatcher 
     */
    public $EventDispatcher;

    /**
     *
     * @var \BootStrap\GuzzleHttp
     */
    public $GuzzleHttp;

    /**
     *
     * @var int
     */
    public $offsetCount = 3;

    /**
     *
     * @var int
     */
    public $offsetEvent = 2;

    /**
     *
     * @var Storage\Redis;
     */
    public $Storage;

    /**
     *
     * @var array
     */
    public $config;

    /**
     *
     * @var array
     */
    public static $source;

    /**
     *
     * @var array
     */
    public static $lang;

    /**
     *
     * @var array
     */
    public static $lock;

    /**
     *
     * @var array
     */
    public static $data;

    public function __construct() {
        $this->Events = \BootStrap\Events::getInstance();
        $this->EventDispatcher = $this->Events->EventDispatcher;
        $this->GuzzleHttp = \BootStrap\GuzzleHttp::getInstance();
        $this->Storage = Storage\Redis::getInstance();
//        $this->config = \BootStrap\AppConfig::load($this, null);
    }

    /**
     * 
     * @param array $events
     * @param \Symfony\Component\EventDispatcher\GenericEvent $event
     * @return Symfony\Component\EventDispatcher\EventDispatcher 
     */
    public function events($events, GenericEvent $event) {
        if (!isset($event['job'])) {
            throw new \Exception('not found job');
//            $event['exception'] = new \Exception('not found job');
//            $this->EventDispatcher->dispatch('error', $event);
//            return;
        }
        if (is_array($events)) {
            foreach ($events as $i => $eventName) {
//                  $event->setArgument('job.current', $i + $this->offsetEvent);
                $this->EventDispatcher->dispatch($eventName, $event);
            }
        } else {
            $this->EventDispatcher->dispatch($events, $event);
        }
    }

    public function call($call) {
        call_user_func($call);
    }

    public static function http(ErrorEvent $e) {
        return [
            'response' => [
                'url' => $e->getResponse()->getEffectiveUrl(),
                'code' => $e->getResponse()->getStatusCode(),
                'body' => $e->getResponse()->getBody(),
                'headers' => $e->getResponse()->getHeaders(),
            ],
            'request' => [
                'url' => $e->getRequest()->getUrl(),
                'method' => $e->getRequest()->getMethod(),
                'config' => $e->getRequest()->getConfig()->toArray(),
                'headers' => $e->getRequest()->getHeaders()
            ]
        ];
    }

    public static function exception(\Exception $e) {
        return [
            'code' => $e->getCode(),
            'line' => $e->getLine(),
            'trace.array' => $e->getTrace(),
            'trace.string' => $e->getTraceAsString(),
            'msg' => $e->getMessage(),
            'file' => $e->getFile()
        ];
    }


    public function event(array $cfg) {
        $this->EventDispatcher->dispatch($name, new GenericEvent(null, $cfg));
    }

    /**
     * 
     * @param array| \Symfony\Component\EventDispatcher\GenericEvent $cfg
     * @throws Exception
     */
    public function handler($cfg) {
//        \console\msg(__METHOD__, ($cfg instanceof GenericEvent) ? $cfg->getArguments() : $cfg);
//        console(__METHOD__, ($cfg instanceof GenericEvent) ? $cfg->getArguments() : $cfg);
        /* @var $job \GearmanJob */
        $job = $cfg['job'];
        $data = $cfg['http.cfg'];
        $on = (isset($cfg['job.on'])) ? $cfg['job.on'] : true;
        if ($on) {
            $count = (!isset($cfg['job.count'])) ? count($data['events'][Consts\Http::Complete]) : $cfg['job.count'];
        }
//        \console\msg(__METHOD__,$data);
        $client = $this->GuzzleHttp->getClient($data['options']);
        unset($data['options']['base_url']);
        unset($data['options']['defaults']);
        if (!isset($data['url'])) {
            \console\msg(__METHOD__ . ':' . __LINE__, $data);
            throw new Exception('not found url ' . __METHOD__);
        }
        if (is_array($data['url'])) {
            foreach ($data['url'] as $url) {
                $requests[] = $client->createRequest($data['method'], $url, $data['options']);
            }
            if ($on === true && isset($cfg['job.fix']) && $cfg['job.fix'] === true) {
                $count*=count($data['url']);
            }
        } elseif (is_string($data['url'])) {
            $requests[] = $client->createRequest($data['method'], $data['url'], $data['options']);
        } else {
            throw new Exception('not found url ' . __METHOD__);
        }
        if ($on) {
            $current = (!isset($cfg['job.current'])) ? 1 : $cfg['job.current'] + 1;
            $cfg['job.current'] = $current + 2;
            $count += $this->offsetCount;
            $cfg['job.count'] = $count;
        }
//        console(__METHOD__ . ':' . __LINE__, "$current/$count");
        if ($on) {
            $job->sendStatus($current, $count);
        }
        foreach ($data['events'] as $type => $listeners) {
            switch ((string) $type) {
                case 'complete':
                case Consts\Http::Complete:
                    $events['complete'] = function(CompleteEvent $e)use($cfg, $listeners) {
                        $cfg['url'] = $e->getResponse()->getEffectiveUrl();
                        $cfg['http.status'] = Consts\Http::Complete;
                        $cfg['http.event'] = $e;
                        $cfg['events'] = $listeners;
                        if (!isset($cfg['params'])) {
                            $cfg['params'] = [];
                        }
//                        console(__METHOD__ . ':' . __LINE__, $listeners, $cfg['job.current'] . '/' . $cfg['job.count']);
                        $this->events($listeners, (!$cfg instanceof GenericEvent) ? new GenericEvent(null, $cfg) : $cfg);
                        return true;
                    };
                    break;
                case 'error':
                case Consts\Http::Error:
                    $events['error'] = function(ErrorEvent $e)use($listeners) {
                        $cfg['http.exception'] = $e->getException();
                        $cfg['http.event'] = $e;
                        $cfg['http.log'] = Kernel::http($e);
                        $cfg['http.status'] = Consts\Http::Error;
                        $this->events($listeners, (!$cfg instanceof GenericEvent) ? new GenericEvent(null, $cfg) : $cfg);
                        return true;
                    };
                    break;
            }
        }
        if ($on) {
            $job->sendStatus(++$current, $count);
        }
//        console(__METHOD__ . ':' . __LINE__, "$current/$count");
        \GuzzleHttp\Pool::send($client, $requests, $events);
    }

}
