<?php

namespace Grabber\Storage;

class Redis {

    use \Traits\getInstance;

    /**
     *
     * @var  \PHPDaemon\Clients\Redis\Pool|\Redis
     */
    public $Redis;
    public $Event;

    /**
     *
     * @var int
     */
    public $id;

    /**
     *
     * @var int
     */
    public $ttl = 300;

    public function __construct() {
        $this->Redis = \PHPDaemon\Clients\Redis\Pool::getInstance('');
    }

    public function id($id) {
        $this->id = $id;
        return $this;
    }

    public function set($id, $data) {
        if(is_callable($data)){
            $data = call_user_func($data);
        }
        $this->Redis->hSet($this->id, $id, json_encode($data));
        setTimeout(function($event){
            $this->Redis->expire($this->id, $this->ttl);
            $event->finish();
        },7e5);
    }

    public function get($id, $callback) {
        $call = function($redis)use($callback) {
            call_user_func($callback, json_decode($redis->result[0]));
        };
        $this->Redis->hExists($this->id, $id, function($redis)use($id, $callback, $call) {
            if ($redis->result !== null) {
                $this->Redis->hGet($this->id, $id, $call);
            } else {
                call_user_func($callback);
            }
        });
    }

    public function del($id) {
        $this->Redis->hDel($this->id, $id);
    }

    public function loaded($callback) {
        $this->Redis->hKeys($this->id, function($redis)use($callback) {
            if ($redis->result !== null) {
                call_user_func($callback, $redis->result);
            } else {
                call_user_func($callback, []);
            }
        });
    }

    public function load($callback) {
        $call = function($redis)use($callback) {
            call_user_func($callback, $redis->result);
        };
        $this->Redis->keys($this->id, function($redis)use($callback, $call) {
            if ($redis->result !== null) {
                $this->Redis->hGetAll($this->id, $call);
            } else {
                call_user_func($callback, []);
            }
        });
    }

    public function unload($id) {
        $this->Redis->delete($id);
    }

}
