<?php

namespace Grabber\Consts;

interface Error {
    /**
     * не найден ключ для lang
     */
    const index_lang = 1;
    /**
     * не найден ключ для source
     */
    const index_source = 2;
    /**
     * не найден ключ для count
     */
    const count = 3;
    /**
     * не найден ключ для bin
     */
    const bin = 4;
    /**
     * не найден ключ для hex
     */
    const index_resource = 5;
}