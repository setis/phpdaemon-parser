<?php

namespace Grabber\Consts;

interface Stage {

    const
            Create = 0,
            Request = 1,
            Response = 2;

}
