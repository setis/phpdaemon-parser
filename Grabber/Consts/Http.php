<?php

namespace Grabber\Consts;

interface Http {

    const
            Complete = 1,
            Error = 0;

}
