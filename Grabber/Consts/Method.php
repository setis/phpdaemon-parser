<?php

namespace Grabber\Consts;

interface Method {

    const
            Get = 0,
            Post = 1,
            Put = 2,
            Delete = 3;

}
