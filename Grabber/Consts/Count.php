<?php

namespace Grabber\Consts;

interface Count{

    const lang = 0;
    const source = 1;
    const status = 2;
    const year = 3;
    const chapter = 4;
    const page = 5;
    const type = 6;
}

