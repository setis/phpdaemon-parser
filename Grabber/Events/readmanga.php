<?php

namespace Grabber\Events;

use GuzzleHttp\Event\CompleteEvent,
    Grabber\Index\Storage,
    Grabber\Index\Collector\One,
    Grabber\Index\Data\Short,
    Grabber\Index\Data\Long,
    Grabber\Index\Data\Status,
    Grabber\Index\Data\Year,
    Grabber\Index\Data\Chapter;

class readmanga {

    public static function db(array $models, $name) {
        $source = Storage::source('readmanga');
        return function($storage)use($models, $name, $source) {
            $error = function()use($models) {
                \PHPDaemon\Core\Daemon::log(\PHPDaemon\Core\Debug::dump($models));
            };
//            \console(__FILE__ . ':' . __LINE__);
            list($class, $t1, $t2, $t3) = $models;
            $model = clone $class::getInstance();
            $model->setTable($t1);
            $model_relations = clone \Grabber\Model\relations::getInstance();
            $model_relations->setTable($t2);
            $model_source = clone \Grabber\Model\relations\source::getInstance();
            $model_source->setTable($t3);
            foreach ($storage as $object) {
                $data = $object->toArray();
                $id = $object->id();
                $relations = [
                    'id' => $name . $id,
                    'src' => $name,
                    'dst' => $id,
                ];
                $sources = [
                    'id' => $name . $id,
                    'source' => $source,
                    'relation' => $name . $id,
                    'time' => \mvc\model::timestamp()
                ];
                $model->insert($data, null, self::error($model, $error));
                $model_relations->insert($relations, null, self::error($model_relations, $error));
                $model_source->insert($sources, null, self::error($model_source, $error));
            }
        };
    }

    public static function error($model, $error = null, $success = null, $duplicate = null) {
        list($connect, $table) = \mvc\model\install::config($model);
        $install = \mvc\model\install::func($connect, $table, [get_class($model), 'create_table']);
        $insert = function($sql)use($connect, $success, $error) {
            \mvc\model\sql::run($connect, $sql, $success, $error, true);
        };
        return function(\Exception $e, \PHPDaemon\Clients\MySQL\Connection $query)use($install, $insert, $duplicate, $error) {
            $sql = $e->getSql();
            $insertInto = function()use($sql, $insert) {
                $insert($sql);
            };
            switch ($e->getCode()) {
                case 1146:
                    $install($insertInto, function($e)use($insertInto, $duplicate, $error) {
                        switch ($e->getCode()) {
                            case 1062:
                                if ($duplicate !== null) {
                                    call_user_func($duplicate, $e);
                                }
                                return;
                            //таблица уже существует
                            case 1050:
                                $insertInto();
                                break;
                            default:
                                if ($error !== null) {
                                    call_user_func($error, func_get_args());
                                } else {
                                    throw $e;
                                }
                                break;
                        }
                    });
                    break;
                case 1062:
                    if ($duplicate !== null) {
                        call_user_func($duplicate);
                    }
                    return;
                case 1064:
                    \console\console('mysql error_code: 1064 sql: ' . $e->getSql());
                default:
                    if ($error !== null) {
                        call_user_func($error, func_get_args());
                    } else {
                        throw $e;
                    }
                    break;
            }
        };
    }

    public static function action_list() {
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        $request = $client->createRequest('GET', '/list?type=&sortType=rate&max=100000000');
        $request->getEmitter()->on('complete', function(CompleteEvent $e) {
            $reponse = $e->getResponse();
            $content = $reponse->getBody()->getContents();
            $url = $reponse->getEffectiveUrl();
            \Grabber\Events\readmanga::lists($content, $url);
        });
        $client->send($request);
    }

    public static function lists(CompleteEvent $event) {
        $result = \Grabber\Parse\readmanga::lists($event->getResponse()->getBody()->getContents());
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        $names = new One();
        foreach ($result as $value) {
            $requests[] = $client->createRequest('GET', $value['link']);
            foreach ($value['name'] as $lang => $name) {
                $names->add(new Short([
                    'lang' => Storage::lang($lang),
                    'value' => $name
                ]));
            }
        }
        self::name($names);
        $events = [
            'complete' => function(CompleteEvent $e) {
                self::content($e);
            }
        ];

        setTimeout(function($event)use(&$client, &$requests, &$events) {
            $model = \Grabber\Model\register\name::getInstance();
            $model->generate();
            \GuzzleHttp\Pool::send($client, $requests, $events);
            $event->finish();
        }, 3e6);
    }

    public static function name($names, $cb = null) {
        $names->overall(function($names)use($cb) {
            $en = Storage::lang('en');
            $source = Storage::source('readmanga');
            foreach ($names as $name) {
                if ($name->lang === $en) {
                    $iname = $name->id();
                    if ($cb !== null) {
                        call_user_func($cb, $iname);
                    }
                    break;
                }
            }
            $error = function( $e) {
                if ($e instanceof \Exception) {
                    $e->getTraceAsString();
                    throw $e;
                } else {
                    throw new \Exception();
                }
            };
            $model = clone \Grabber\Model\short::getInstance();
            $model->setTable('name');
            $model_relations = clone \Grabber\Model\relations::getInstance();
            $model_relations->setTable('name_relations_alias');
            $model_source = clone \Grabber\Model\relations\source::getInstance();
            $model_source->setTable('name_relations_alias_source');
            foreach ($names as $name1) {
                /* @var $name1 \Grabber\Index\Data\Short */
                $data = $name1->toArray();
                foreach ($names as $name2) {
                    if ($name1->lang !== $name2->lang) {
                        $id1 = $name1->id();
                        $id2 = $name2->id();
                        $data_relations = [
                            'id' => $id1 . $id2,
                            'src' => $id1,
                            'dst' => $id2
                        ];
                        $data_source = [
                            'id' => $iname . $id1 . $id2,
                            'source' => $source,
                            'relation' => $iname . $id1 . $id2,
                            'time' => \mvc\model::timestamp()
                        ];
                        $model_relations->insert($data_relations, null, self::error($model_relations, $error));
                        $model_source->insert($data_source, null, self::error($model_source, $error));
                    }
                }
                $model->insert($data, null, self::error($model, $error));
            }
        });
    }

    public static function action_cover($result, $source, $related) {
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
//        \console\msg(__METHOD__, $related, $result['cover']);
        foreach ($result['cover'] as $link) {
            $requests[] = $client->createRequest('GET', $link);
        }

        self::nameFindByRelated($related, function($id)use(&$client, &$requests, $source) {
//            \console\msg(__LINE__, $id, $source);
            $events = [
                'complete' => function($e)use($id, $source) {
                    \console\msg(__LINE__, $id, $source);
                    self::cover($e, $id, $source);
                }
            ];
            \GuzzleHttp\Pool::send($client, $requests, $events);
        });
    }

    public static function cover($content, $url, &$name_id, $resource) {
//        \console\msg(__METHOD__,  func_get_args());
        $gc = new \Grabber\Index\Collector\Multi();
        $gc->add(new \Grabber\Index\Data\Image\property(['content' => &$content]), 'property');
        $gc->add(new \Grabber\Index\Data\Url(['url' => $url]), 'link');
        $gc->overall(function($storage)use(&$name_id, $resource) {
//            \console\msg(__METHOD__, $name_id, $resource);
            $model = new \stdClass();
            $model->property = clone \Grabber\Model\image\property::getInstance();
            $model->link = clone \Grabber\Model\url::getInstance();
            $model->relations = clone \Grabber\Model\relations::getInstance();
            $model->property->setTable('cover_property_' . $name_id);
            $model->relations->setTable('cover_relations_' . $name_id);
            $model->link->setTable('cover_link_' . $name_id . '_' . $resource);
            $data = [];
            foreach ($storage as $object) {
                $label = $storage[$object];
                $model->{$label}->insert($object->toArray(), null, self::error($model->{$label}, null));
                $data[$label] = $object->id;
            }
            $model->relations->insert([
                'id' => $data['property'] . $data['link'],
                'src' => $data['property'],
                'dst' => $data['link']
                    ], null, self::error($model->relations, null));
        });
    }

    public static function action_chapter_list($content, $url, $related, $source, $lang) {
        try {
            $result = \Grabber\Parse\readmanga::chapter_list($content);
        } catch (\Exception $e) {
            $ex = new \Grabber\Exception\parse('fail parse');
            throw $ex;
        }
        if (!is_array($result) || count($result) === 0) {
            $e = new \Grabber\Exception\result(\Grabber\Exception\result::not_found_key, ['chapter_list'], $result);
            $e->client_content = $content;
            $e->client_url = $url;
            \console\msg('exception', $e->toArray());
            throw $e;
        }
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
        foreach ($result as $i => $arr) {
            $page = $i + 1;
            $chapter = $arr['chapter'];
            $request = $client->createRequest('GET', $arr['link']);
            $request->getEmitter()->on('complete', function(CompleteEvent &$e)use($chapter, $page, $related, $source, $lang) {
                self::chapter($e, $related, $source, $lang, $chapter, $page);
            });
            $client->send($request);
        }
    }

    public static function action_chapter($result, $related, $source, $lang) {
        $client = \BootStrap\GuzzleHttp::getInstance()->getClient([
            'base_url' => 'http://readmanga.me',
            'future' => true,
        ]);
//        \console\msg(__METHOD__, $related, $result);
        foreach ($result as $link) {
            $requests[] = $client->createRequest('GET', $link);
        }
        $source = Storage::resource('readmanga');
        self::nameFindByRelated($related, function($id)use(&$client, &$requests, $source, $lang) {
//            \console\msg(__LINE__, $id, $source);
            $events = [
                'complete' => function(CompleteEvent &$e)use($id, $source, $lang) {
                    $response = $e->getResponse();
                    \console\msg(__LINE__, $id, $source, $lang);
                    self::action_chapter_list($response->getBody()->getContents(), $response->getEffectiveUrl(), $id, $source, $lang);
                }
            ];
            \GuzzleHttp\Pool::send($client, $requests, $events);
        });
    }

    public static function chapter(&$content, $url, &$name_id, $resource, $lang, $chapter, $page) {
//        \console\msg(__METHOD__);
        $gc = new \Grabber\Index\Collector\Multi();
        $gc->add(new \Grabber\Index\Data\Image\property(['content' => $content]), 'property');
        $gc->add(new \Grabber\Index\Data\Url(['url' => $url]), 'link');
        $gc->overall(function($storage)use(&$name_id, $resource, $lang, $chapter, $page) {
//            \console\msg(__LINE__, $name_id, $resource);
            $model = new \stdClass();
            $model->property = clone \Grabber\Model\image\property::getInstance();
            $model->link = clone \Grabber\Model\url::getInstance();
            $model->property->setTable('chapter_property_' . $name_id);
            $model->link->setTable('chapter_link_' . $name_id . '_' . $lang . '_' . $resource);
            $data = [];
            foreach ($storage as $object) {
                $label = $storage[$object];
                $model->{$label}->insert($object->toArray(), null, self::error($model->{$label}, null));
                $data[$label] = $object->id;
            }
            $relations = clone \Grabber\Model\relations\chapter::getInstance();
            $relations->setTable('chapter_relations_' . $name_id);
            $relations->insert([
                'id' => Storage::bin(Storage::type_chapter, $chapter) . Storage::bin(Storage::type_page, $page) . Storage::bin(Storage::type_resource, $resource) . $data['property'] . $data['link'],
                'property' => $data['property'],
                'link' => $data['link'],
                'resource' => $resource,
                'chapter' => $chapter,
                'page' => $page,
                    ], null, self::error($relations, null));
        });
    }

    public static function nameFindByRelated($related, $cb) {
        $list = \Grabber\Model\register\name::getInstance();
        $success = function()use(&$list, $related, $cb) {
            $list->generate(function()use(&$list, $related, $cb) {
                $list->find_byKey_related($related, function($id)use($cb) {
                    call_user_func($cb, $id);
                }, function($e) {
                    throw $e;
                });
            }, function($e) {
                throw $e;
            });
        };
        $list->find_byKey_related($related, function($id)use($cb) {
            call_user_func($cb, $id);
        }, function($e)use(&$list, &$success) {
            switch ($e->getCode()) {
                case 1146:
                    $list->Query(\Grabber\Model\register\name::create_table(), $success, function($e) {
                        throw $e;
                    });
                    break;
                case 1050:
                    call_user_func($success);
                    break;
                default:
                    throw $e;
            }
        });
    }

    public static function content_chapter($result, $source, $lang, $related) {
        \console\msg(__METHOD__, func_get_args());
        self::nameFindByRelated($related, function($id)use(&$result, $source, $lang) {
            \console\msg('content_chapter:' . __LINE__, $id, $lang, $source);
            $chapter = new One([1e6, 15e6, true]);
            foreach ($result as $value) {
                $data = new Chapter();
                $data->name = $value['name'];
                $data->chapter = $value['chapter'];
                $chapter->add($data);
            }
            $chapter->overall(function($storage)use($source, $id, $lang) {
                $model = clone \Grabber\Model\chapter::getInstance();
                $model_source = clone \Grabber\Model\relations\source::getInstance();
                $table = 'chapter_' . (string) $id . '_' . $lang;
                \console\msg('content_chapter:' . __LINE__, $source, $id, $lang, $table);
                $model->setTable($table);
                $model_source->setTable($table . '_source');
                foreach ($storage as $object) {
                    $result[] = $object->toArray();
                    $model->insert($object->toArray(), null, self::error($model, null));
                    $model_source->insert([
                        'id' => $object->id,
                        'source' => $source,
                        'relation' => $object->id,
                        'time' => \mvc\model::timestamp()
                            ], null, self::error($model_source, null));
                }
                \console\msg(__FUNCTION__, $result);
            });
        });
    }

    public static function content($content, $url) {
        try {
            $result = \Grabber\Parse\readmanga::content($content);
        } catch (\Exception $e) {
            $ex = new \Grabber\Exception\parse('fail parse');
            throw $ex;
        }
        $en = Storage::lang('en');
        $ru = Storage::lang('ru');
        $source = Storage::source('readmanga');
        $list = [
            'author',
            'description',
            'genre',
            'status',
            'name',
            'related',
            'like'
        ];
        $exclude = [];
        foreach ($list as $key) {
            switch ($key) {
                case 'author':
                case 'description':
                case 'genre':
                case 'status':
                case 'like':
                case 'cover':
                case 'year':
                    if (!isset($result[$key]) || empty($result[$key])) {
                        $exclude[] = $key;
                    }
                    break;
                case 'related':
                    if (!isset($result['related.manga']) || empty($result['related.manga'])) {
                        $exclude[] = $key;
                    }
                    break;
                case 'name':
                    if (!isset($result[$key]) || empty($result[$key]) || count($result[$key]) !== 2) {
                        $exclude[] = $key;
                    }
                    break;
                default:
                    throw new \Exception('not found check for key: ' . $key);
            }
        }
        if (count($exclude) !== 0) {
            $e = new \Grabber\Exception\result(\Grabber\Exception\result::not_found_key, $exclude, $result);
            $e->client_content = $content;
            $e->client_url = $url;
            if (in_array('name', $exclude)) {
                \console\msg('exception', $e->toArray());
                throw $e;
            }
            if (in_array('related', $exclude) || in_array('like', $exclude)) {
                \console\msg('warning', $e->toArray());
            }
        }
        /**
         * сборщик
         */
        $gc = new \Grabber\Index\Collector\Multi();
        /**
         * создание ключей
         */
        $params = [1e6, 30e6, true];
        $names = new One($params);
        foreach ($result['name'] as $lang => $name) {
            $names->add(new Short([
                'lang' => Storage::lang($lang),
                'value' => $name
            ]));
        }
        $authors = new One($params);
        if (!in_array('author', $exclude)) {
            foreach ($result['author'] as $author) {
                $authors->add(new Short([
                    'value' => $author['name'],
                    'lang' => $en
                ]));
            }
        }
        $description = new One($params);
        if (!in_array('description', $exclude)) {
            $description->add(new Long([
                'value' => $result['description'],
                'lang' => $ru
            ]));
        }
        $genres = new One($params);
        if (!in_array('genre', $exclude)) {
            foreach ($result['genre'] as &$genre) {
                $genres->add(new Short([
                    'value' => $genre['name'],
                    'lang' => $ru
                ]));
            }
        }
//        $versionist = new One();
//        foreach ($result['translator'] as $value) {
//            $versionist->add(new Short([
//                'lang' => $ru,
//                'value' => $value['name']
//            ]));
//        }
        $status = new One($params);
        if (!in_array('status', $exclude)) {
            $status->add(new Status([
                'status' => (($result['status'] == "завершен") ? 1 : 0),
                'source' => $source
            ]));
        }
        $year = new One($params);
        if (!in_array('year', $exclude)) {
            foreach ($result['year'] as $y) {
                $year->add(new Year([
                    'year' => $y,
                    'source' => $source,
                ]));
            }
        }

        $related = $like = null;
        if (!in_array('related', $exclude)) {
            $related = new One($params);
            foreach ($result['related.manga'] as $manga) {
                $related->add(new Short([
                    'lang' => $en,
                    'value' => $manga['name']
                ]));
            }
            $gc->add($related, 'related');
        }
        if (!in_array('like', $exclude)) {
            $like = new One($params);
            foreach ($result['like'] as $manga) {
                $like->add(new Short([
                    'lang' => $en,
                    'value' => $manga['name']
                ]));
                $gc->add($like, $key);
            }
        }
        $build = new \ArrayObject();
        $build->setFlags(\ArrayObject::ARRAY_AS_PROPS);
        $gc->addAll([
            'name' => &$names,
            'author' => &$authors,
            'description' => &$description,
            'genre' => &$genres,
//            'versionist' => $versionist,
            'status' => &$status,
            'year' => &$year
        ]);
        $gc->overall(function($storage)use(&$build) {
            foreach ($storage as $object) {
                $name = $storage[$object];
                if ($object instanceof One) {
                    $object->overall(function($storage)use(&$build, $name) {
                        foreach ($storage as $object) {
                            $result[] = $object->toArray();
                        }
                        $build->{$name} = &$result;
                    });
                } elseif ($object instanceof \Grabber\Index\Collector\Hand) {
                    $build->{$name} = $object->storage;
                }
            }
            setTimeout(function($event)use(&$build, &$storage) {
                if ($storage->count() === $build->count()) {
                    \console\msg('build', $build);
                    $event->finish();
                    return;
                }
                $event->timeout();
            }, 1e3);
        });

        $chapter = function($related)use(&$result, $ru, $source) {
            self::content_chapter($result['chapters'], $source, $ru, $related);
        };
        $cover = function($related)use(&$result, $source) {
            self::action_cover($result, $source, $related);
        };
        self::name($names, function($name)use( &$authors, &$description, &$genres, &$status, &$year, &$related, &$like, &$chapter, &$cover) {
            $authors->overall(self::db([
                        '\Grabber\Model\short',
                        'author',
                        'name_relations_author',
                        'name_relations_autor_source'
                            ], $name));

            $description->overall(self::db([
                        '\Grabber\Model\short',
                        'description',
                        'name_relations_description',
                        'name_relations_description_source'
                            ], $name));
            $genres->overall(self::db([
                        '\Grabber\Model\short',
                        'genre',
                        'name_relations_genre',
                        'name_relations_genre_source'
                            ], $name));
            foreach ($status->storage as $object) {
                $object->name = $name;
            }
            foreach ($year->storage as $object) {
                $object->name = $name;
            }
            $status->overall(self::db([
                        '\Grabber\Model\status',
                        'status',
                        'name_relations_status',
                        'name_relations_status_source'
                            ], $name));
            $year->overall(self::db([
                        '\Grabber\Model\year',
                        'year',
                        'name_relations_year',
                        'name_relations_year_source'
                            ], $name));
            if ($related !== null) {
                $related->overall(self::db([
                            '\Grabber\Model\short',
                            'name',
                            'name_relations_related',
                            'name_relations_related_source'
                                ], $name));
            }
            if ($like !== null) {
                $like->overall(self::db([
                            '\Grabber\Model\short',
                            'name',
                            'name_relations_like',
                            'name_relations_like_source'
                                ], $name));
            }
            $chapter($name);
            $cover($name);
        });
    }

}
