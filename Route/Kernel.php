<?php

namespace Route;

use PHPDaemon\Core\Daemon;

class Kernel {

    const route_action_anonimous_function = 0;
    const route_action_function = 0;

    use \Traits\getInstance;

    public $route = [];

    public function __construct() {
        $this->route = [
            '~api/(dev|manga)/~ism' => [
                'name' => 'api.manga',
                'action' => function($match) {
                    return "\PHPDaemon\Applications\ApiServer\Server:{$match[1]}";
                },
                'mode' => self::route_action_anonimous_function
            ]
        ];
    }

    public function run($log, $uri) {
        foreach ($this->route as $pattern => &$route) {
//            Daemon::log(__METHOD__ . "\t->" . $uri);
            if (preg_match("/favicon.ico$/ism", $uri, $match)) {
//                Daemon::log(__METHOD__ . ' match->' . \PHPDaemon\Core\Debug::dump($match));
                return false;
            }
            if (preg_match($pattern, $uri, $match)) {
//                Daemon::log(__METHOD__ . ' match->' . \PHPDaemon\Core\Debug::dump($match));
                switch ($route['mode']) {
                    case self::route_action_anonimous_function:
                        $result = call_user_func_array($route['action'], [$match]);
                        break;
                }
                if ($result === false) {
                    return false;
                }
                Daemon::log($log . ' route:' . $route['name'] . ' class->' . $result);
                return $result;
            }
        }
        return false;
    }

}
