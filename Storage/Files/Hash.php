<?php

namespace Storage\Files;

class Hash {

    use \Traits\getInstance;

    /**
     *
     * @var \SplFixedArray
     */
    public static $algos;

    /**
     * 
     * @return \SplFixedArray
     */
    public function Algos() {
        if (self::$algos === null) {
            self::$algos = new \SplFixedArray(hash_algos());
        }
        return self::$algos;
    }

    public function File($file, array $algos = null, array &$notAlgo = []) {
        if (!file_exists($file)) {
            throw new \Exception('not found file:' . $file);
        }
        foreach ($algos as $algo) {
            if (in_array($algo, self::$algos)) {
                $result[$algo] = hash_file($algo, $file);
            } else {
                $notAlgo[] = $algo;
            }
        }
        return $result;
    }

    public function Content($content, array $algos = null, array &$notAlgo = []) {
        foreach ($algos as $algo) {
            if (in_array($algo, self::$algos)) {
                $result[$algo] = hash($algo, $content);
            } else {
                $notAlgo[] = $algo;
            }
        }
        return $result;
    }

}
