<?php

namespace Manga\Model;

class author extends \mvc\model {

    use \Traits\getInstance;

    public function init() {
        $this->connect = 'manga';
        $this->_table = 'author';
    }

    public static function create_table($table) {
        return 'CREATE TABLE `' . $table . '` (
  `id` varchar(255) NOT NULL COMMENT "$hash[sha1($value)]$lang:$size[strlen($value)]",
  `lang` int(3) NOT NULL,
  `value` text CHARACTER SET utf8 NOT NULL,
  `hash` binary(20) NOT NULL COMMENT "sha1",
  `size` int(10) NOT NULL COMMENT "strlen($value)",
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
';
    }

    public function insert(array $data, $onSuccess = null, $onError = null) {
        $this->Query($this->Builder()->insert($data)->build(),$onSuccess,$onError);
    }


}
