<?php

namespace Manga\Model;

class lang extends \mvc\model {

    use \Traits\getInstance;

    protected $id = 'code';
    protected $_table = 'lang';
    protected $connect = 'manga';

}
