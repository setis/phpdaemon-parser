--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.2.280.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 16.10.2014 15:23:33
-- Версия сервера: 5.5.38-0ubuntu0.14.04.1-log
-- Версия клиента: 4.1
--


USE manga;

CREATE TABLE `manga_chapters_$(manga_list.id)` (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL COMMENT 'оригинальное название манги',
  lang varchar(5) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'код языка',
  chapter int(5) NOT NULL COMMENT 'номер главы',
  `release` datetime DEFAULT NULL COMMENT 'дата выпуска',
  volume int(3) DEFAULT NULL COMMENT 'том главы',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `manga_pages_$(manga_list.id)_lang` (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  chapter int(6) NOT NULL,
  lang varchar(5) NOT NULL,
  date date DEFAULT NULL,
  count int(2) NOT NULL DEFAULT 1,
  uniq varchar(255) NOT NULL COMMENT 'chapter:lang',
  PRIMARY KEY (id),
  UNIQUE INDEX uniq (uniq)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `manga_pages_$lang_$(manga_list.id)_resource` (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  chapter int(6) NOT NULL,
  storage blob NOT NULL,
  width int(5) UNSIGNED NOT NULL,
  height int(5) NOT NULL,
  ext varchar(8) NOT NULL,
  date date NOT NULL,
  hash_md5 varchar(32) DEFAULT NULL,
  size_b bigint(20) UNSIGNED DEFAULT NULL,
  size_kb bigint(20) UNSIGNED DEFAULT NULL,
  lang varchar(5) NOT NULL,
  translation_id bigint(20) UNSIGNED NOT NULL COMMENT 'manga_translators.id',
  text text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `manga_pages_$lang_$(manga_list.id)_storage` (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  chapter int(6) NOT NULL,
  page int(2) UNSIGNED NOT NULL,
  lang varchar(5) NOT NULL,
  translation_id bigint(20) UNSIGNED NOT NULL COMMENT 'manga_translators.id',
  width int(5) UNSIGNED NOT NULL,
  height int(5) NOT NULL,
  ext varchar(8) NOT NULL,
  date date NOT NULL,
  text text DEFAULT NULL,
  storage blob NOT NULL,
  hash_md5 varchar(32) DEFAULT NULL,
  size_b bigint(20) UNSIGNED DEFAULT NULL,
  size_kb bigint(20) UNSIGNED DEFAULT NULL,
  uniq varchar(255) DEFAULT NULL COMMENT 'lang:chapter:translation_id:page:width:height',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_author (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  lang varchar(5) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_genres (
  id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  genres_id int(3) UNSIGNED NOT NULL,
  lang varchar(5) DEFAULT NULL,
  uniq varchar(255) DEFAULT NULL COMMENT 'genres_id:lang',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_genres_lang (
  id int(3) UNSIGNED NOT NULL,
  name varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_list (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL COMMENT 'название оригинальное',
  lang varchar(5) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'код язык (ru,en,jp)',
  volumes int(3) DEFAULT NULL COMMENT 'количество томов',
  chapters int(6) DEFAULT NULL COMMENT 'количество глав',
  premiere_date date DEFAULT NULL COMMENT 'дата выпуска',
  premiere_year int(4) DEFAULT NULL COMMENT 'год выпуска',
  `release` date DEFAULT NULL COMMENT 'дата следощего выпуска(главы)',
  is_finished bit(1) DEFAULT b'0' COMMENT '0 еще не закончена 1 - завершена',
  PRIMARY KEY (id),
  UNIQUE INDEX name (name)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_list_lang (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50) DEFAULT NULL COMMENT 'переведенное название манги ',
  lang varchar(5) CHARACTER SET armscii8 COLLATE armscii8_general_ci DEFAULT NULL COMMENT 'код языка ru,en,jp',
  manga_id bigint(20) DEFAULT NULL COMMENT 'manga_list.id',
  priority int(1) NOT NULL DEFAULT 0 COMMENT 'приоритет название',
  uniq varchar(255) NOT NULL COMMENT 'lang:manga_id:md5(name)',
  PRIMARY KEY (id),
  UNIQUE INDEX uniq (uniq)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_relation_list_author (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  manga_id bigint(20) UNSIGNED NOT NULL,
  author_id bigint(20) UNSIGNED NOT NULL,
  uniq varchar(255) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL COMMENT 'manga_id:author_id',
  PRIMARY KEY (id),
  UNIQUE INDEX uniq (uniq)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'таблица связей манга и автор';

CREATE TABLE manga_relation_list_genre (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  manga_id bigint(20) UNSIGNED NOT NULL,
  genre_id int(3) UNSIGNED NOT NULL,
  uniq varchar(255) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL COMMENT 'manga_id:genre_id',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_resource (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  type varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE manga_translators (
  id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50) DEFAULT NULL,
  home varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;
