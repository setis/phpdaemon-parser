<?php

namespace Manga\Action;
use PHPDaemon\Core\Daemon;
/**
 * @link http://itbuben.org/blog/Unix-way/2462.html пример
 */
class Task {

    public static function handler(\GearmanJob $job) {
        $data = json_decode($job->workload(), true);
    }

    public static function small($path, $new) {
        $proc = new \PHPDaemon\Core\ShellCommand();
        $proc->nice(256)->execute('/usr/bin/convert ' . $path . '  -resize 75x75 ' . $new);
    }

    public static function medium($path, $new) {
        $proc = new \PHPDaemon\Core\ShellCommand();
        $exec = '/usr/bin/convert ' . $path . '  -resize 200x200 ' . $new;
        Daemon::log($exec);
        $proc->nice(256)->execute($exec);
    }

}
