<?php

namespace Manga\Action;

use \PHPDaemon\Core\Daemon,
    PHPDaemon\FS\FileSystem;

class Upload {

    use \Traits\getInstance;

    public $dir = [
        'translator' => '/var/www/image/translator'
    ];
    public $mimetypes = [];

    public function __construct() {
        $this->mimetypes = [
            'image/png' => 'png',
            'image/jpeg' => 'jpg',
            'image/gif' => 'gif',
            'image/bmp' => 'bmp',
        ];
    }

    public function handler($name) {
        if (!isset(Daemon::$req->attrs->files['image'])) {
            return;
        }
        $dir = $this->dir[$name];
        $result = [];
        foreach (Daemon::$req->attrs->files['image'] as $file) {
            $ext = '.' . $this->mimetypes[$file['type']];
            $path = FileSystem::genRndTempnam($dir, '') . $ext;
            $result[$path] = $file;
            rename($file['tmp_name'], $path);
//            FileSystem::chown($path, 33);
//            FileSystem::read($file['tmp_name'], function() {
//            });
        }
        return $result;
    }

}
