<?php

namespace System\Events;

use Symfony\Component\EventDispatcher\EventDispatcherInterface,
    Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\EventDispatcher\Event;

class Dispatcher extends EventDispatcher implements EventDispatcherInterface {

    /**
     *
     * @var \SplQueue
     */
    protected $queue;

    /**
     *
     * @var string 
     */
    protected $dispatch;

    /**
     *
     * @var int
     */
    protected $timer;

    /**
     * @see EventDispatcherInterface::dispatch()
     *
     * @api
     */
    public function dispatch($eventName, Event $event = null) {
        if (null === $event) {
            $event = new Event();
        }

        $event->setDispatcher($this);
        $event->setName($eventName);

        if (!isset($this->listeners[$eventName])) {
            return $event;
        }

        $this->doDispatch($this->getListeners($eventName), $eventName, $event);

        return $event;
    }

    /**
     * Triggers the listeners of an event.
     *
     * This method can be overridden to add functionality that is executed
     * for each listener.
     *
     * @param callable[] $listeners The event listeners.
     * @param string     $eventName The name of the event to dispatch.
     * @param Event      $event     The event object to pass to the event handlers/listeners.
     */
    protected function doDispatch($listeners, $eventName, Event $event) {
        $this->queue = new \SplQueue();
        $this->dispatch = $eventName;
        foreach ($listeners as $listener) {
            $this->queue->push($listener);
        }

        foreach ($listeners as $listener) {
            call_user_func($listener, $event, $eventName, $this);
            if ($event->isPropagationStopped()) {
                break;
            }
        }
    }

    public function lock($eventName) {
        $this->lock[$eventName] = true;
    }

    public function unlock($eventName) {
        $this->lock[$eventName] = false;
    }

    public function timer($event) {
        if ($this->queue->count() === 0) {
            $event->finish();
            return;
        }
        $this->queue->pop();
    }

}
