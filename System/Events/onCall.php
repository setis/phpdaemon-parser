<?php

namespace System\Events;

class onCall {

    /**
     *
     * @var \SplQueue
     */
    public $events;

    public function __construct() {
        $this->events = new \SplQueue();
    }

    public function __invoke() {
        call_user_func_array([&$this,'dispatch'], func_get_args());
    }
    /**
     * 
     * @param callable $cb
     * @return this|self|static
     */
    public function addEvent($cb) {
        $this->events->push($cb);
        return $this;
    }

    /**
     * 
     * @param callable $cb
     * @return this|self|static
     */
    public function addEventAll() {
        foreach (func_get_args() as $cb) {
            $this->events->push($cb);
        }
        return $this;
    }
    /**
     * 
     * @param callable $cb
     * @return boolean
     */
    public function hashEvent($cb) {
        return (in_array($this->events, $cb));
    }
    /**
     * 
     * @param callable $cb
     * @return this|self|static
     */
    public function removeEvent($cb) {
        foreach ($this->events as $i => $cb1) {
            if ($cb === $cb1) {
                unset($this->events[$i]);
            }
        }
        return $this;
    }
    /**
     * @return void
     */
    public function dispatch() {
        $args = func_get_args();
        while ($this->events->count() !== 0) {
            call_user_func_array($this->events->shift(), $args);
        }
    }

}
