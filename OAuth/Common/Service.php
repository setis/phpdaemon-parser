<?php

namespace OAuth\Common;

abstract class Service {

    const REQUEST_METHOD_HEADER = 'header';
    const REQUEST_METHOD_QUERY = 'query';
    const SIGNATURE_METHOD_HMAC = 'HMAC-SHA1';
    const SIGNATURE_METHOD_RSA = 'RSA-SHA1';
    const SIGNATURE_METHOD_PLAINTEXT = 'PLAINTEXT';

    /**
     *
     * @var \SplFixedArray 
     */
    public $scopes;

    public function __construct() {
//        $this->scopes = new \SplFixedArray();
//        $this->scopes->fromArray($this->getScopes());
        $this->scopes = $this->getScopes();
    }

    public function __get($name) {
        return constant(get_class($this).'::'.$name);
    }

    
    /**
     * @return Array
     */
    abstract public function getScopes();

    /**
     * 
     * @param array $scopes
     * @throws InvalidScopeException
     */
    public function isValidScopes(array $scopes) {
        foreach ($scopes as $scope) {
            if (!in_array($scope, $this->scopes)) {
                throw new InvalidScopeException('Scope ' . $scope . ' is not valid for service ' . get_class($this));
            }
        }
    }

}
