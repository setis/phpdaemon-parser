<?php

class linkedin extends OAuth\Common\Service {

    const version = '1.0a';
    const request = 'https://api.linkedin.com/uas/oauth/requestToken?scope={SCOPE}';
    const access = 'https://api.linkedin.com/uas/oauth/accessToken';
    const authorize = 'https://api.linkedin.com/uas/oauth/authenticate';
    const signature = 'HMAC-SHA1';
    const method = 'GET';
    const header = true;
    const parameters = true;

    public static function getScopes() {
        return [];
    }

}
