<?php

namespace OAuth\Service;

use \OAuth\Token\TokenResponseException,
    \OAuth\Token\Token2;

class vk extends \OAuth\Common\Service {

    use \Traits\getInstance;

    const version = '2';
    const access = 'https://oauth.vk.com/access_token';
    const authorize = 'https://oauth.vk.com/authorize';
    const api = 'https://api.vk.com/method/';
    const signature = self::SIGNATURE_METHOD_HMAC;
    const method = 'GET';
    const header = true;
    const parameters = true;

    /**
     * Defined scopes
     *
     * @link http://vk.com/dev/permissions
     */
    const SCOPE_NOTIFY = 'notify';
    const SCOPE_FRIENDS = 'friends';
    const SCOPE_PHOTOS = 'photos';
    const SCOPE_AUDIO = 'audio';
    const SCOPE_VIDEO = 'video';
    const SCOPE_DOCS = 'docs';
    const SCOPE_NOTES = 'notes';
    const SCOPE_PAGES = 'pages';
    const SCOPE_APP_LINK = '';
    const SCOPE_STATUS = 'status';
    const SCOPE_OFFERS = 'offers';
    const SCOPE_QUESTIONS = 'questions';
    const SCOPE_WALL = 'wall';
    const SCOPE_GROUPS = 'groups';
    const SCOPE_MESSAGES = 'messages';
    const SCOPE_NOTIFICATIONS = 'notifications';
    const SCOPE_STATS = 'stats';
    const SCOPE_ADS = 'ads';
    const SCOPE_OFFLINE = 'offline';
    const SCOPE_NOHTTPS = 'nohttps';

    public function getScopes() {
        return [
            self::SCOPE_ADS,
            self::SCOPE_APP_LINK,
            self::SCOPE_AUDIO,
            self::SCOPE_DOCS,
            self::SCOPE_FRIENDS,
            self::SCOPE_GROUPS,
            self::SCOPE_MESSAGES,
            self::SCOPE_NOHTTPS,
            self::SCOPE_NOTES,
            self::SCOPE_NOTIFICATIONS,
            self::SCOPE_NOTIFY,
            self::SCOPE_OFFERS,
            self::SCOPE_OFFLINE,
            self::SCOPE_PAGES,
            self::SCOPE_PHOTOS,
            self::SCOPE_QUESTIONS,
            self::SCOPE_STATS,
            self::SCOPE_STATUS,
            self::SCOPE_VIDEO,
            self::SCOPE_WALL
        ];
    }

    public function parseAccessTokenResponse($responseBody) {
        $data = json_decode($responseBody, true);
        if (null === $data || !is_array($data)) {
            throw new TokenResponseException('Unable to parse response.');
        } elseif (isset($data['error'])) {
            throw new TokenResponseException('Error in retrieving token: "' . $data['error'] . '"');
        }
        $token = new Token2();
        $token->setAccessToken($data['access_token']);
        $token->setLifeTime($data['expires_in']);

        if (isset($data['refresh_token'])) {
            $token->setRefreshToken($data['refresh_token']);
            unset($data['refresh_token']);
        }

        unset($data['access_token']);
        unset($data['expires_in']);

        $token->setExtraParams($data);

        return $token;
    }

}
