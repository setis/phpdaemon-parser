<?php

namespace OAuth\DocsApi;

class Service extends \OAuth\Common\Service{
    /**
     *
     * @var vk
     */
    public $name;
    /**
     *
     * @var string 2.0 or 1.0 or 1.0a
     */
    public $version;
    /**
     *
     * @var string https://oauth.vk.com/access_token
     */
    public $access;
    /**
     *
     * @var string https://oauth.vk.com/authorize
     */
    public $authorize;
    /**
     *
     * @var string
     */
    public $request;
    /**
     *
     * @var string HMAC-SHA1
     */
    public $signature = 'HMAC-SHA1';
    /**
     *
     * @var string GET
     */
    public $method = 'GET';
    /**
     *
     * @var boolean 
     */
    public $header = true;
    /**
     *
     * @var boolean
     */
    public $parameters = true;
    
    
    /**
     * Parses the request token response and returns a TokenInterface.
     * This is only needed to verify the `oauth_callback_confirmed` parameter. The actual
     * parsing logic is contained in the access token parser.
     *
     * @abstract
     *
     * @param string $responseBody
     *
     * @return TokenInterface
     *
     * @throws TokenResponseException
     */
    public function parseRequestTokenResponse($responseBody){}

    /**
     * Parses the access token response and returns a TokenInterface.
     *
     * @abstract
     *
     * @param string $responseBody
     *
     * @return TokenInterface
     *
     * @throws TokenResponseException
     */
    public function parseAccessTokenResponse($responseBody){}
}