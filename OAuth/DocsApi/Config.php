<?php

namespace OAuth\DocsApi;

class Config{
    /**
     *
     * @var string
     */
    public $key;
    /**
     *
     * @var string|callable
     */
    public $secret;
    /**
     *
     * @var Array
     */
    public $scopes;
}