<?php

namespace OAuth\Token;


/**
 * Standard OAuth2 token implementation.
 * Implements OAuth\OAuth2\Token\TokenInterface for any functionality that might not be provided by AbstractToken.
 */
class Token2 extends AbstractToken implements TokenInterface
{
}
