<?php

namespace OAuth\Token;

/**
 * Exception relating to token response from service.
 */
class TokenResponseException extends \Exception
{
}
