<?php

namespace PHPDaemon\Applications\OAuth;

use PHPDaemon\Core\AppInstance;

class External extends AppInstance {

    /**
     *
     * @var \OAuth\Service
     */
    public $Service;

    /**
     *
     * @var \OAuth\Storage\Memory
     */
    public $Storage;

    protected function init() {
        if ($this->isEnabled()) {
            $this->Storage = new \OAuth\Storage\Memory();
            $this->Service = new \OAuth\Service($this->config->service, \BootStrap\GuzzleHttp::getInstance()->getClient(), $this->Storage, false);
        }
    }

    protected function getConfigDefaults() {
        return \BootStrap\AppConfig::load($this, $this->name);
    }

    /**
     * Creates Request.
     * @param object Request.
     * @param object Upstream application instance.
     * @return object Request.
     */
    public function beginRequest($req, $upstream) {
        return new ExternalRequest($this, $upstream, $req);
    }

}

class ExternalRequest extends \PHPDaemon\HTTPRequest\Generic {

    /**
     *
     * @var External
     */
    public $appInstance;
    protected $step = 0;

    protected function init() {
        $this->appInstance->Service->stateParameterInAuthUrl = false;
        $storage = $this->appInstance->Storage;
        $storage->clearAllTokens();
        $storage->clearAllAuthorizationStates();
    }

    protected function run() {
        $service = &$this->appInstance->Service;
        $v = (int) $service->service->version;
        if ($v === 2 && !empty($_GET['code']) && $this->step === 0) {
            /* @var $token \OAuth\OAuth2\Token\TokenInterface */
            $service->requestAccessToken([
                'code' => $_GET['code'],
                'state' => (empty($_GET['state'])) ? null : $_GET['state']
                    ], function($token) {
                
                $this->wakeup();
                var_export($token);
                $this->finish();
            }, function($e, \GuzzleHttp\Event\ErrorEvent $event) {
                $this->wakeup();
                echo $event->getResponse()->getBody()->getContents();
                $this->finish();
            });
//            $token->getAccessToken();
            $onSuccess = function($result) {
//                Daemon::log('onSuccess');
                $access = (int) $result[0]['access'];
                if ($access === 0) {
                    $this->header('Location: /reg', true, 303);
//                    Daemon::log(__METHOD__.' '.Debug::dump($_SESSION).' '.Debug::backtrace());
                    $_SESSION['asdasd'] = true;
                } else {
                    $_SESSION['user'] = $result[0]['user'];
                    $_SESSION['role'] = $result[0]['role'];
                    $this->header('Location: /', true, 303);
                }
            };
//            Daemon::log(__METHOD__.' '.Debug::dump($_SESSION).' '.Debug::backtrace());
//            $this->oauth->login($onSuccess, null, 'vk', $_SESSION['vk'], $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR']);
            $this->step++;
            $this->sleep(1);
        } elseif ($v === 1 && !empty($_GET['oauth_token']) && $this->step === 0) {
            $service->requestAccessToken([
                'token' => $_GET['oauth_token'],
                'verifier' => $_GET['oauth_verifier'],
                'tokenSecret' => $token->getRequestTokenSecret()
                    ], function($token) {
                $this->wakeup();
                $this->out(var_export($token, true));
                $this->finish();
            });
            $this->step++;
            $this->sleep(1);
        } elseif (isset($_GET['go']) && $_GET['go'] === 'go') {
            $service->getAuthorizationUri(function($url) {
                header('Location: ' . $url);
            }
//            , ['v' => '5.27', 'display' => 'page','response_type'=>'code']
                    );
        } else {
            $url = $service->getCallback() . '?go=go';
            echo "<a href='$url'>Login!</a>";
        }
    }

}
