<?php

namespace OAuth\Storage;

use OAuth\Token\TokenInterface;
use OAuth\Storage\Exception\TokenNotFoundException;
use OAuth\Storage\Exception\AuthorizationStateNotFoundException;


class Redis implements TokenStorageInterface {

    /**
     * @var string
     */
    protected $key;
    protected $stateKey;
    public $ttl = 5;

    /**
     * @var  \PHPDaemon\Clients\Redis\Pool|\Redis
     */
    protected $redis;

    /**
     * @var object|TokenInterface
     */
    protected $cachedTokens;

    /**
     * @var object
     */
    protected $cachedStates;

    /**
     * @param Predis $redis An instantiated and connected redis client
     * @param string $key The key to store the token under in redis
     * @param string $stateKey The key to store the state under in redis.
     */
    public function __construct($key, $stateKey, $redis = null) {
        if ($redis === null) {
            $this->redis = \PHPDaemon\Clients\Redis\Pool::getInstance('');
        }
        $this->key = $key;
        $this->stateKey = $stateKey;
        $this->cachedTokens = array();
        $this->cachedStates = array();
    }

    /**
     * {@inheritDoc}
     */
    public function retrieveAccessToken($service, $cb) {
        $result = function($redis)use($service, $cb) {
            $val = unserialize($redis->result[0]);
            $this->cachedTokens[$service] = $val;
            call_user_func($cb, $val);
        };

        $this->hasAccessToken($service, function()use($service, $result, $cb) {
            if (isset($this->cachedTokens[$service])) {
                call_user_func($cb, $this->cachedTokens[$service]);
            }
            $this->redis->hget($this->key, $service, $result);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function storeAccessToken($service, TokenInterface $token) {
        // (over)write the token
        $this->redis->hset($this->key, $service, serialize($token));
//        setTimeout(function($event) {
//            $this->redis->expire($this->key, $this->ttl);
//            $event->finish();
//        }, 7e5);
        $this->cachedTokens[$service] = $token;

        // allow chaining
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function hasAccessToken($service, $cb) {
        if (isset($this->cachedTokens[$service]) && $this->cachedTokens[$service] instanceof TokenInterface
        ) {
            return true;
        }

        return $this->redis->hExists($this->key, $service, function($redis)use($cb) {
                    if ($redis->result !== null) {
                        call_user_func($cb);
                    } else {
                        throw new TokenNotFoundException('Token not found in redis');
                    }
                });
    }

    /**
     * {@inheritDoc}
     */
    public function clearToken($service) {
        $this->redis->hdel($this->key, $service);
        unset($this->cachedTokens[$service]);

        // allow chaining
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function clearAllTokens() {
        // memory
        $this->cachedTokens = array();
        // redis
        $this->redis->hKeys($this->key, function($redis) {
            if ($redis->result !== null) {
                $keys = $redis->result;
                // pipeline for performance
                $this->redis->pipeline(
                        function ($pipe) use ($keys) {
                    foreach ($keys as $k) {
                        $pipe->hdel($this->key, $k);
                    }
                }
                );
            }
        });
        // allow chaining
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function retrieveAuthorizationState($service, $cb) {
        $this->hasAuthorizationState($service, function()use($cb, $service) {
            if (isset($this->cachedStates[$service])) {
                call_user_func($cb, $this->cachedStates[$service]);
                return;
            }
            $this->redis->hget($this->stateKey, $service, function($redis)use($service, $cb) {
                $val = $redis->result;
                $this->cachedStates[$service] = $val;
                call_user_func($cb, $val);
            });
        });
    }

    /**
     * {@inheritDoc}
     */
    public function storeAuthorizationState($service, $state) {
        // (over)write the token
        $this->redis->hset($this->stateKey, $service, $state);
        $this->cachedStates[$service] = $state;

        // allow chaining
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function hasAuthorizationState($service, $cb) {
        if (isset($this->cachedStates[$service]) && null !== $this->cachedStates[$service]
        ) {
            return true;
        }
        $this->redis->hExists($this->stateKey, $service, function($redis)use($cb) {
            if ($redis->result !== null) {
                call_user_func($cb);
            } else {
                throw new AuthorizationStateNotFoundException('State not found in redis');
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public function clearAuthorizationState($service) {
        $this->redis->hdel($this->stateKey, $service);
        unset($this->cachedStates[$service]);

        // allow chaining
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function clearAllAuthorizationStates() {
        // memory
        $this->cachedStates = array();
        // redis
        $this->redis->hKeys($this->stateKey, function($redis) {
            if ($redis->result !== null) {
                $keys = $redis->result;
                // pipeline for performance
                $this->redis->pipeline(
                        function ($pipe) use ($keys) {
                    foreach ($keys as $k) {
                        $pipe->hdel($this->key, $k);
                    }
                }
                );
            }
        });

        // allow chaining
        return $this;
    }

    /**
     * @return Predis $redis
     */
    public function getRedis() {
        return $this->redis;
    }

    /**
     * @return string $key
     */
    public function getKey() {
        return $this->key;
    }

}
