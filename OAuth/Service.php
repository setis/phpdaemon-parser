<?php

namespace OAuth;

use GuzzleHttp\Event\BeforeEvent,
    GuzzleHttp\Event\HeadersEvent,
    GuzzleHttp\Event\CompleteEvent,
    GuzzleHttp\Event\ErrorEvent;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

/**
 * @example http://www.sitepoint.com/using-guzzle-twitter-via-oauth/ oauth1 twitter
 */
class Service {

    /**
     *
     * @var string
     */
    public $request_method;

    /**
     *
     * @var \ArrayObject|\OAuth\DocsApi\Config
     */
    public $config;

    /**
     *
     * @var \OAuth\Storage\Redis|\OAuth\Storage\TokenStorageInterface
     */
    public $storage;

    /**
     *
     * @var \OAuth\DocsApi\Service
     */
    public $service;

    /**
     *
     * @var \GuzzleHttp\Client
     */
    public $client;

    /**
     * @var bool 
     */
    public $stateParameterInAuthUrl;

    public function __construct(array $config, \GuzzleHttp\Client $client, Storage\TokenStorageInterface &$storage, $stateParameterInAutUrl = false) {
        $this->config = $config;
        $this->storage = $storage;
        $this->stateParameterInAuthUrl = $stateParameterInAutUrl;
        $this->client = $client;
        $class = '\OAuth\Service\\' . $config['name'];
        try {
            $this->service = call_user_func([$class, 'getInstance']);
        } catch (\Exception $e) {
            throw new Exception("not service {$this->config['name']} class: $class", $e->getCode(), $e);
        }
        $this->service->isValidScopes($config['scopes']);
    }

    /**
     * 
     * @return string url callback
     */
    public function getCallback() {
        return (!is_callable($this->config['callback'])) ? $this->config['callback'] : call_user_func($this->config['callback']);
    }

    /**
     * 
     * @param \GuzzleHttp\Url|string $url
     * @param array $parameters
     * @return \GuzzleHttp\Url
     */
    public static function Url($url, array $parameters = []) {
        if ($url instanceof \GuzzleHttp\Url) {
            $objUrl = $url;
        } else {
            $objUrl = \GuzzleHttp\Url::fromString($url);
        }
        if (count($parameters) !== 0) {
            $objUrl->setQuery(array_merge($objUrl->getQuery()->toArray(), $parameters));
        }
        return $objUrl;
    }

    /**
     * 
     * @param array $additionalParameters
     * @return \GuzzleHttp\Url
     */
    public function getAuthorizationUri($cb, array $additionalParameters = []) {
        switch ($this->service->version) {
            case '2':
                $parameters = array_merge(
                        $additionalParameters, [
                    'type' => 'web_server',
                    'client_id' => $this->config['key'],
                    'redirect_uri' => $this->getCallback(),
                    'response_type' => 'code',
                        ]
                );
                $parameters['scope'] = implode(' ', $this->config['scopes']);
                if ($this->needsStateParameterInAuthUrl()) {
                    if (!isset($parameters['state'])) {
                        $parameters['state'] = $this->generateAuthorizationState();
                    }
                    $this->storeAuthorizationState($parameters['state']);
                }
                $url = self::Url($this->service->authorize, $parameters);
                call_user_func($cb, $url);
                break;
            case '1':
                $this->requestRequestToken(function($token, $cb)use($additionalParameters) {
                    $additionalParameters['oauth_token'] = $token->getRequestToken();
                    $url = self::Url($this->service->authorize, $additionalParameters);
                    call_user_func($cb, $url);
                });
                break;
        }
    }

    /**
     * 
     * @return \OAuth\OAuth2\Token\TokenInterface|\OAuth\OAuth1\Token\TokenInterface
     */
    public function requestAccessToken(array $cfg,$success,$error=null) {
        switch ($this->service->version) {
            case '2':
                $state = null;
                foreach (['state', 'code'] as $key) {
                    if (isset($cfg[$key])) {
                        ${$key} = $cfg[$key];
                    }
                }
                if (null !== $state) {
                    $this->validateAuthorizationState($state);
                }
                $request = $this->client->createRequest($this->service->method, $this->service->access, [
                    'query' => [
                        'code' => $cfg['code'],
                        'client_id' => $this->config['key'],
                        'client_secret' => $this->config['secret'],
                        'redirect_uri' => $this->getCallback(),
                        'grant_type' => 'authorization_code',
//                        'grant_type' => 'client_credentials',
                    ],
//                    'headers' => $this->getExtraOAuthHeaders(),
                    'events' => [
                        'complete' => function(CompleteEvent $e) use($success) {
                            $token = $this->parseAccessTokenResponse($e->getResponse()->getBody()->getContents());
                            $this->storage->storeAccessToken($this->config['name'], $token);
                            call_user_func($success, $token);
                        },
                        'error' => function(ErrorEvent $e)use($error) {
                            $exception = new \Exception(__METHOD__ . ' service: ' . $this->config['name']);
                            if ($error === null) {
                                throw $exception;
                            }
                            call_user_func($error, $exception, $e);
                        }
                    ]
                ]);
                break;

            case '1':
                foreach (['token', 'verifier', 'tokenSecret'] as $key) {
                    if (isset($cfg[$key])) {
                        ${$key} = $cfg[$key];
                    }
                }
                if (!isset($tokenSecret) || $tokenSecret === null) {
                    $storedRequestToken = $this->storage->retrieveAccessToken($this->config['name']);
                    $tokenSecret = $storedRequestToken->getRequestTokenSecret();
                }
                $this->signature->setTokenSecret($tokenSecret);

                $request = $this->client->createRequest($this->service->method, $this->service->access, [
                    'body' => [
                        'oauth_verifier' => $verifier,
                    ],
                    'headers' => $this->getExtraOAuthHeaders(),
                    'events' => [
                        'complete' => function(CompleteEvent $e) use($success) {
                            $token = $this->parseAccessTokenResponse($e->getResponse()->getBody()->getContents());
                            $this->storage->storeAccessToken($this->config['name'], $token);
                            call_user_func($success, $token);
                        },
                        'error' => function(ErrorEvent $e)use($error) {
                            $exception = new \Exception(__METHOD__ . ' service: ' . $this->config['name']);
                            if ($error === null) {
                                throw $exception;
                            }
                            call_user_func($error, $exception, $e);
                        }
                    ]
                ]);
                $oauth = new Oauth1([
                    'consumer_key' => $this->config[key],
                    'consumer_secret' => $this->config[secret],
                    'request_method' => $this->service[request_method],
                    'token' => $token
//                    'token_secret' => $tokenSecret
                ]);
                $request->getEmitter()->attach($oauth);
                break;
        }
        $this->client->send($request);
    }

    /**
     * Refreshes an OAuth2 access token.
     *
     * @param TokenInterface $token
     *
     * @return TokenInterface $token
     *
     * @throws MissingRefreshTokenException
     */
    public function refreshAccessToken(TokenInterface $token, $success, $error = null) {
        switch ($this->service->version) {
            case '2':
                $refreshToken = $token->getRefreshToken();
                if (empty($refreshToken)) {
                    throw new MissingRefreshTokenException();
                }
                $request = $this->client->createRequest($this->service->method, $this->service->access, [
                    'body' => [
                        'grant_type' => 'refresh_token',
                        'type' => 'web_server',
                        'client_id' => $this->config['key'],
                        'client_secret' => $this->config['secret'],
                        'refresh_token' => $refreshToken,
                    ],
                    'headers' => $this->getExtraOAuthHeaders(),
                    'events' => [
                        'complete' => function(CompleteEvent $e) use($success) {
                            $token = $this->parseAccessTokenResponse($e->getResponse()->getBody()->getContents());
                            $this->storage->storeAccessToken($this->config['name'], $token);
                            call_user_func($success, $token, $e);
                        },
                        'error' => function(ErrorEvent $e)use($error) {
                            $exception = new \Exception(__METHOD__ . ' service: ' . $this->config['name']);
                            if ($error === null) {
                                throw $exception;
                            }
                            call_user_func($error, $exception, $e);
                        }
                    ]
                ]);
                break;
            default :
            case '1':
                throw new \Exception('not method refreshAccessToken Oauth v' . $this->service->version . ': service' . $this->config['name']);
        }
        $this->client->send($request);
    }

    /**
     * Check if the given service need to generate a unique state token to build the authorization url
     *
     * @return bool
     */
    public function needsStateParameterInAuthUrl() {
        return $this->stateParameterInAuthUrl;
    }

    /**
     * Validates the authorization state against a given one
     *
     * @param string $state
     * @throws InvalidAuthorizationStateException
     */
    protected function validateAuthorizationState($state) {
        if ($this->retrieveAuthorizationState() !== $state) {
            throw new InvalidAuthorizationStateException();
        }
    }

    /**
     * Generates a random string to be used as state
     *
     * @return string
     */
    protected function generateAuthorizationState() {
        return md5(microtime() . $this->config['name'] . uniqid());
    }

    /**
     * Retrieves the authorization state for the current service
     *
     * @return string
     */
    protected function retrieveAuthorizationState() {
        return $this->storage->retrieveAuthorizationState($this->config['name']);
    }

    /**
     * Stores a given authorization state into the storage
     *
     * @param string $state
     */
    protected function storeAuthorizationState($state) {
        $this->storage->storeAuthorizationState($this->config['name'], $state);
    }

    /**
     * Return any additional headers always needed for this service implementation's OAuth calls.
     *
     * @return array
     */
    protected function getExtraOAuthHeaders() {
        return array();
    }

    /**
     * Return any additional headers always needed for this service implementation's API calls.
     *
     * @return array
     */
    protected function getExtraApiHeaders() {
        return array();
    }

    public function requestRequestToken($success, $error = null) {
        $request = $this->client->createRequest($this->service->method, $this->service->request, [
            'auth' => 'oauth',
            'body' => [
                'oauth_callback' => $this->getCallback()
            ],
            'events' => [
                'complete' => function(CompleteEvent $e) use($success) {
                    $token = $this->parseRequestTokenResponse($e->getResponse()->getBody()->getContents());
                    $this->storage->storeAccessToken($this->config['name'], $token);
                    call_user_func($success, $token);
                },
                'error' => function(ErrorEvent $e)use($error) {
                    $exception = new \Exception(__METHOD__ . ' service: ' . $this->config['name']);
                    if ($error === null) {
                        throw $exception;
                    }
                    call_user_func($error, $exception, $e);
                }
            ]
        ]);
        $oauth = new Oauth1([
            'consumer_key' => $this->config['key'],
            'consumer_secret' => $this->config['secret'],
            'request_method' => $this->service['request_method']
        ]);
        $request->getEmitter()->attach($oauth);
        $this->client->send($request);
    }

    /**
     * Parses the request token response and returns a TokenInterface.
     * This is only needed to verify the `oauth_callback_confirmed` parameter. The actual
     * parsing logic is contained in the access token parser.
     *
     * @param string $responseBody
     *
     * @return TokenInterface
     *
     * @throws TokenResponseException
     */
    protected function parseRequestTokenResponse($responseBody) {
        return $this->service->parseRequestTokenResponse($responseBody);
    }

    /**
     * Parses the access token response and returns a TokenInterface.        

     *
     * @param string $responseBody
     *
     * @return TokenInterface
     *
     * @throws TokenResponseException
     */
    protected function parseAccessTokenResponse($responseBody) {
        return $this->service->parseAccessTokenResponse($responseBody);
    }

}
