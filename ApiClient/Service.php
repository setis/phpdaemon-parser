<?php

namespace ApiClient;

use \GuzzleHttp\Client,
    \Closure;

interface Service {

    public function method($method);

    public function one(Config $config, Client $client, $method, array $params, array $options = [], Closure $success, Closure $error = null);

    public function multi(Config $config, Client $client, $method, array $parameters, array $options = [], Closure $success, Closure $error = null);
}
