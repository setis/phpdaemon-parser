<?php

namespace ApiClient\Service;

use \GuzzleHttp\Client,
    \Closure,
    \ApiClient\Config;

class vk implements \ApiClient\Service {

    use \Traits\getInstance;

    const api = 'https://api.vk.com/method/';
    const version = '5.27';
    const post = 'POST';
    const get = 'GET';

    public $methods = [];

    /**
     *
     * @var Vk\Rules
     */
    public $rules;

    /**
     *
     * @var Vk\Subscriber
     */
    public $Subscriber;

    /**
     *
     * @var array
     */
    public $scopes;

    public function method($method) {
        return (in_array($method, $this->methods)) ? self::post : self::get;
    }

    public function __construct() {
        $this->Subscriber = new Vk\Subscriber();
        \PHPDaemon\FS\FileSystem::open(__DIR__ . '/Vk/rules.json', 'r', function($file) {
            if ($file) {
                $file->readAll(function($data) {
                    $this->rules = new \ApiClient\Service\Vk\Rules(json_decode($data, true));
                });
            } else {
                $this->rules = new \ApiClient\Service\Vk\Rules(json_decode(file_get_contents(__DIR__ . '/Vk/rules.json'), true));
            }
        });
        $this->scopes = $this->scopes();
    }

    protected function scopes() {
        $ref = new \ReflectionClass('\ApiClient\Service\Vk\Scopes');
        return array_values($ref->getConstants());
    }

    public function one(Config $config, Client $client, $method, array $params, array $options = [], Closure $success, Closure $error = null) {
        $request_method = self::method($method);
        $subscriber = clone $this->Subscriber;
        $subscriber->init($success, $error);
        $subscriber->config = $config;
        $requeri_options = [
            'timeout' => 5,
            'verify' => false,
        ];
        $params['v'] = self::version;
        $requeri_options[($request_method === self::get) ? 'query' : 'body'] = $params;
        $request = $client->createRequest($request_method, self::api . $method, (count($options) === 0) ? $requeri_options : array_merge($requeri_options, $options));
        $request->getEmitter()->attach($subscriber);
        $client->send($request);
    }

    public function multi(Config $config, Client $client, $method, array $parameters, array $options = [], Closure $success, Closure $error = null) {
        $url = self::api . $method;
        $subscriber = clone $this->Subscriber;
        $subscriber->init($success, $error);
        $subscriber->config = $config;
        $request_method = self::method($method);
        $requeri_options = [
            'timeout' => 5,
            'verify' => false,
        ];
        $params['v'] = self::version;
        $requeri_options[($request_method === self::get) ? 'query' : 'body'] = $params;
        foreach ($parameters as $params) {
            $request = $client->createRequest($request_method, $url, (count($options) === 0) ? $requeri_options : array_merge($requeri_options, $options));
            $request->getEmitter()->attach($subscriber);
            $requests[] = $request;
        }
        $client->sendAll($requests);
    }

}
