<?php

namespace ApiClient\Service\Vk;

class Rules {

    /**
     *
     * @var \SplFixedArray
     */
    public $methods;

    /**
     *
     * @var array
     */
    public $required;

    /**
     *
     * @var array
     */
    public $token;

    /**
     *
     * @var array
     */
    public $types;

    const validate_method = 1;
    const validate_required = 2;
    const validate_type = 3;

    public function __construct(array $rules = null,$level = 1) {
        if ($rules !== null) {
            $this->load($rules);
        }
    }

    public function load(array $rules) {
        $this->methods = \SplFixedArray::fromArray(array_keys($rules));
        $this->token = $rules['token'];
        unset($rules['token']);
        foreach ($rules as $method => $rule) {
            foreach ($rule as $field => $right) {
                if ($right['required'] === true) {
                    $this->required[$method][] = $field;
                }
                $this->types[$method][$field] = $this->typesValidate($right['type'], $right['value']);
                if (isset($right['token'])) {
                    $this->token[$method][$field] = (count($right['token']) === 1) ? $right['token'] : \SplFixedArray::fromArray($right['token']);
                }
            }
        }
    }

    public function typesValidate($type, $values) {
        switch ($type) {
            case 'listOptions':
                return function($data, &$diff)use($values) {
                    if (is_string($data)) {
                        $data = explode(',', $data);
                    }
                    $diff = array_diff($values, $data);
                    return (count($diff));
                };
            case 'enum':
                return function($data)use($values) {
                    return in_array($data, $values);
                };
            default:
                break;
        }
    }

    public function isMethod($method) {
        return (in_array($method, $this->methods));
    }

    public function isRequired($method, $params, &$diff = null) {
        $diff = array_diff($params, $this->required[$method]);
        return (count($diff) === 0);
    }

    public function isToken($method, $params,$break=1) {
        if (!isset($this->token[$method])) {
            return false;
        }
        $token = $this->token[$method];
        foreach($token as $field=>$value){
            if(isset($params[$field])){
                $params[$field]=$value;
            }
        }
    }

    public static function Type_ListOptions(array $data) {
        return implode(',', $data);
    }

    public function isTypes($method, $params, $break = 1, &$diff = null) {
        $types = $this->types[$method];
        $result = [];
        foreach ($types as $field => $type) {
            $f = call_user_func($type, $params[$field]);
            switch ($break) {
                case 2:
                    $result[$field] = $f;
                    break;
                case 1:
                    if ($f === false) {
                        return false;
                    }
                    break;
            }
        }
        $diff = array_filter($result, function($v) {
            return ($v === false);
        });
        return (count($diff) === 0);
    }

    public function isValidate($method, array $params, $level = self::validate_required) {
        if (!in_array($method, $this->methods)) {
            return true;
        }
        switch ($level) {
            case self::validate_required:


                break;

            default:
                break;
        }
    }

}
