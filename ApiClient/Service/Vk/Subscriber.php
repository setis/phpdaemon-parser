<?php

namespace ApiClient\Service\Vk;

use GuzzleHttp\Event\SubscriberInterface,
    GuzzleHttp\Event\RequestEvents,
    GuzzleHttp\Event\BeforeEvent,
    GuzzleHttp\Event\ErrorEvent,
    GuzzleHttp\Event\CompleteEvent;

class Subscriber implements SubscriberInterface {

    /**
     *
     * @var callable
     */
    public $success;

    /**
     *
     * @var callable
     */
    public $error;

    /**
     *
     * @var \ApiClient\Config
     */
    public $config;

    public function init($success, $error = null) {
        foreach (['success', 'error'] as $var) {
            if (is_callable(${$var})) {
                $this->{$var} = ${$var};
            }
        }
        return $this;
    }

    public function __clone() {
        $this->error = $this->success = null;
    }

    public function getEvents() {
        return [
            'complete' => ['onComplete'],
            'error' => ['onError'],
            'before' => ['onBefore', RequestEvents::SIGN_REQUEST]
        ];
    }

    public function onBefore(BeforeEvent $event) {
        if (!$this->config instanceof \ApiClient\Config || !in_array(Scopes::nohttps, $this->config->scopes)) {
            console(__METHOD__ . ' no sig');
            return;
        }
        $request = $event->getRequest();
        $params = $request->getQuery();
        $params['api_id'] = $this->config->app_id;
        $params['sig'] = md5(urldecode(str_replace('https://api.vk.com','',$request->getUrl())).$this->config->secret);
        $request->setQuery($params);
        $request->setScheme('http');
    }

    public function onComplete(CompleteEvent $event) {
        $content = json_decode($event->getResponse()->getBody()->getContents(), true);
        if (isset($content['error'])) {
            $e = new Exception($content['error']);
            if ($this->error === null) {
                throw $e;
            }
            call_user_func($this->error, $e, $event);
        }
//        console(__METHOD__, $content);
        if ($this->success !== null) {
            call_user_func($this->success, (isset($content['response'])) ? $content['response'] : $content, $event);
        }
    }

    public function onError(ErrorEvent $event) {
        $e = new \Exception('error service: vk');
        if ($this->error === null) {
            throw $e;
        }
        call_user_func($this->error, $e, $event);
    }

}
