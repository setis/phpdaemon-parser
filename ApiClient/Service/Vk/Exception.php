<?php

namespace ApiClient\Service\Vk;

class Exception extends \Exception {

    protected $params;

    public function __construct($content) {
        $this->message = $content['error_msg'];
        $this->code = $content['error_code'];
        $this->params = $content['request_params'];
    }

    public function getParams() {
        return $this->params;
    }

    public function getContent() {
        return [
            'error' => [
                'error_msg' => $this->message,
                'error_code' => $this->code,
                'request_params' => $this->params,
            ]
        ];
    }

}
