<?php

namespace ApiClient;

class Config {

    /**
     *
     * @var service
     */
    public $name;

    /**
     *
     * @var string key(APP_ID)
     */
    public $app_id;

    /**
     *
     * @var string secret
     */
    public $secret;

    /**
     *
     * @var string access_token
     */
    public $access;

    /**
     *
     * @var int USER_ID
     */
    public $user_id;

    /**
     *
     * @var int GROUP_ID
     */
    public $group_id;

    /**
     *
     * @var array|\SplFixedArray
     */
    public $scopes;

    /**
     *
     * @var array
     */
    protected static $list = ['name', 'app_id', 'secret', 'access', 'user_id', 'group_id', 'scopes'];

    public function __construct(array $array = null) {
        if ($array !== null) {
            $this->exchangeArray($array);
        }
    }

    public function exchangeArray($input) {
        foreach (self::$list as $var) {
            if (isset($input[$var])) {
                $this->{$var} = $input[$var];
            }
        }
    }

    public function toArray() {
        $array = [];
        foreach (self::$list as $var) {
            if (!empty($this->{$var})) {
                $array[$var] = $this->{$var};
            }
        }
        return $array;
    }

}
