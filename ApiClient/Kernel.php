<?php

namespace ApiClient;

class Kernel {

    use \Traits\getInstance;

    /**
     *
     * @var \GuzzleHttp\Client 
     */
    public $client;

    /**
     *
     * @var string service
     */
    public $service;

    const services = '\ApiClient\Service\\', one = 'one', multi = 'multi', mode_multi = true, mode_one = false;

    public function __construct() {
        $this->client = \BootStrap\GuzzleHttp::getInstance()->getClient();
    }

    /**
     * 
     * @param string $name
     * @return this|static|self
     */
    public function service($name) {
        $this->service = $name;
        return $this;
    }

    public function call(Config $config, $method, array $params, array $options = [], \Closure $success, \Closure $error = null, $multi = self::mode_one) {
        /* @var $object \ApiClient\Service */
        $object = call_user_func([self::services . ((empty($config->name)) ? $this->service : $config->name), 'getInstance']);
        call_user_func([$object, ($multi) ? self::multi : self::one], $config, $this->client, $method, $params, $options, $success, $error);
    }

    public function api(Config $config, $method, array $params, array $options = [], \Closure $success, \Closure $error = null, $multi = self::mode_one) {
        /* @var $object \ApiClient\Service */
        $object = call_user_func([self::services . ((empty($config->name)) ? $this->service : $config->name), 'getInstance']);
        call_user_func([$object, ($multi) ? self::multi : self::one], $config, $this->client, $method, $params, $options, $success, $error);
    }

}
